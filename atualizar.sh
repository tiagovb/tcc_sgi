#!/usr/bin/env bash

function info {
  echo " "
  echo "--> $1"
  echo " "
}

info "Atualizando codigo fonte...."
cd /www/app/
git fetch --depth=1 fetch_https
git reset --hard fetch_https/master
chmod +x atualizar.sh

info "Código atualizado com sucesso!"
