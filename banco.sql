SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS Membro;
CREATE TABLE Membro (
  id                 INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  conjuge_id         INT,
  igreja_id          INT          NOT NULL,
  nome               VARCHAR(256) NOT NULL,
  nomePai            VARCHAR(256),
  nomeMae            VARCHAR(256),
  sexo               TINYINT      NOT NULL,
  logradouro         VARCHAR(256) NOT NULL,
  numero             VARCHAR(256) NOT NULL,
  complemento        VARCHAR(256),
  cidade             VARCHAR(256) NOT NULL,
  estado             CHAR(2)      NOT NULL,
  cep                CHAR(9),
  telefone           VARCHAR(256),
  celular            VARCHAR(256),
  email              VARCHAR(256),
  nivelEscolar       TINYINT,
  dtNascimento       DATE         NOT NULL,
  naturalidade       VARCHAR(256),
  rg                 VARCHAR(256),
  cpf                VARCHAR(256),
  estadoCivil        TINYINT      NOT NULL,
  dtCasamento        DATE,
  necessidadeAuxilio TINYINT,
  observacoesAuxilio VARCHAR(500),
  observacoes        VARCHAR(500),
  dtCadastro         DATETIME     NOT NULL,
  status             TINYINT      NOT NULL,
  dtStatus           DATETIME     NOT NULL
);

DROP TABLE IF EXISTS Usuario;
CREATE TABLE Usuario (
  id             INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  igreja_id      INT          NOT NULL,
  login          VARCHAR(100) NOT NULL  UNIQUE,
  hashSenha      VARCHAR(150) NOT NULL,
  tipoUsuario    TINYINT      NOT NULL,
  dtUltimoAcesso DATETIME,
  tempoOciosoMax TINYINT
);

DROP TABLE IF EXISTS Atividade;
CREATE TABLE Atividade (
  id        INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  igreja_id INT          NOT NULL,
  dataHora  DATETIME     NOT NULL,
  descricao VARCHAR(256) NOT NULL
);

DROP TABLE IF EXISTS Despesa;
CREATE TABLE Despesa (
  id           INT            NOT NULL AUTO_INCREMENT PRIMARY KEY,
  igreja_id    INT            NOT NULL,
  dtReferencia DATE           NOT NULL,
  valor        DECIMAL(10, 2) NOT NULL,
  descricao    VARCHAR(256)   NOT NULL,
  notaFiscal   VARCHAR(100),
  quantidade   DECIMAL(10, 2) NOT NULL,
  tipo         TINYINT        NOT NULL
);

DROP TABLE IF EXISTS Compromisso;
CREATE TABLE Compromisso (
  id          INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  usuario_id  INT          NOT NULL,
  descricao   VARCHAR(256) NOT NULL,
  data        DATETIME     NOT NULL,
  localizacao VARCHAR(256)
);

DROP TABLE IF EXISTS Funcao;
CREATE TABLE Funcao (
  id        INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nome      VARCHAR(100) NOT NULL UNIQUE,
  descricao VARCHAR(256)
);

DROP TABLE IF EXISTS EscalaTrabalho;
CREATE TABLE EscalaTrabalho (
  membro_id INT     NOT NULL,
  funcao_id INT     NOT NULL,
  turno     TINYINT NOT NULL,
  data      DATE    NOT NULL,
  PRIMARY KEY (membro_id, funcao_id, data, turno)
);

CREATE TABLE Dependente (
  id           INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  membro_id    INT          NOT NULL,
  nome         VARCHAR(256) NOT NULL,
  dtNascimento DATE,
  religiao     TINYINT
);

DROP TABLE IF EXISTS Igreja;
CREATE TABLE Igreja (
  id          INT          NOT NULL PRIMARY KEY,
  razaoSocial VARCHAR(256) NOT NULL,
  logradouro  VARCHAR(256) NOT NULL,
  numero      VARCHAR(256) NOT NULL,
  complemento VARCHAR(256),
  bairro      VARCHAR(256),
  cep         INT,
  cidade      VARCHAR(256) NOT NULL,
  uf          CHAR(2)      NOT NULL
);

DROP TABLE IF EXISTS Patrimonio;
CREATE TABLE Patrimonio (
  id            INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  igreja_id     INT          NOT NULL,
  descricao     VARCHAR(256) NOT NULL,
  dtAquisicao   DATE         NOT NULL,
  valor         DECIMAL(10, 2),
  tipoAquisicao TINYINT      NOT NULL,
  observacoes   VARCHAR(256),
  notaFiscal    VARCHAR(256),
  status        TINYINT      NOT NULL
);

ALTER TABLE EscalaTrabalho
  ADD FOREIGN KEY (funcao_id) REFERENCES Funcao (id)
  ON UPDATE RESTRICT
  ON DELETE RESTRICT;

ALTER TABLE EscalaTrabalho
  ADD FOREIGN KEY (membro_id) REFERENCES Membro (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;

ALTER TABLE Membro
  ADD FOREIGN KEY (conjuge_id) REFERENCES Membro (id)
  ON UPDATE CASCADE
  ON DELETE RESTRICT;

ALTER TABLE Membro
  ADD FOREIGN KEY (igreja_id) REFERENCES Igreja (id)
  ON UPDATE CASCADE
  ON DELETE RESTRICT;

ALTER TABLE Usuario
  ADD FOREIGN KEY (igreja_id) REFERENCES Igreja (id)
  ON UPDATE CASCADE
  ON DELETE RESTRICT;

ALTER TABLE Atividade
  ADD FOREIGN KEY (igreja_id) REFERENCES Igreja (id)
  ON UPDATE CASCADE
  ON DELETE RESTRICT;

ALTER TABLE Despesa
  ADD FOREIGN KEY (igreja_id) REFERENCES Igreja (id)
  ON UPDATE CASCADE
  ON DELETE RESTRICT;

ALTER TABLE Compromisso
  ADD FOREIGN KEY (usuario_id) REFERENCES Usuario (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;

ALTER TABLE Dependente
  ADD FOREIGN KEY (membro_id) REFERENCES Membro (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;

ALTER TABLE Patrimonio
  ADD FOREIGN KEY (igreja_id) REFERENCES Igreja (id)
  ON UPDATE CASCADE
  ON DELETE RESTRICT;

SET FOREIGN_KEY_CHECKS = 1;