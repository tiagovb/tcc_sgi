#!/bin/bash

#set -e
set -e

FILES=$(git diff-tree --diff-filter=ACM --no-commit-id --name-only -r ..origin/develop -- | grep "\.php$" || echo "" )

if [ "$FILES" == "" ]; then
    echo "Nenhum arquivo PHP para testar"
    exit 0
else
    echo -e "\n--"
    echo "Arquivos PHP para testar:"
    echo ${FILES} | tr " ", "\n"
    echo -e "--\n"
fi

echo -e "--\nphpcs\n"
vendor/bin/phpcs -d memory_limit=512M -s -p --colors --extensions=php --standard=build/phpcs ${FILES} && echo "Ok!"
echo -e "--\n\n"

echo -e "--\nphp-cs-fixer\n"
vendor/bin/php-cs-fixer fix --config=build/phpcs_fixer.php --dry-run --no-ansi --no-interaction --verbose ${FILES} && echo "Ok!"
echo -e "--\n\n"

echo -e "--\nphpmd\n"
vendor/bin/phpmd $(echo ${FILES} |  tr ' ' ',') text ./build/phpmd.xml --suffixes php --exclude tests,vendor,.res && echo "Ok!"
echo -e "--\n\n"

echo -e "Sucesso!\n\n"
