<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class DateRangePickerAsset
 */
class DateRangePickerAsset extends AssetBundle
{
    /* @inheritdoc */
    public $js = [
        'js/dateRangePicker.js',
    ];

    /** {@inheritdoc} */
    public $css = [
        'css/dateRangePicker.css',
    ];

    /** {@inheritdoc} */
    public $depends = [
        \kartik\daterange\DateRangePickerAsset::class,
    ];

    /* @inheritdoc */
    public function init()
    {
        $this->sourcePath = __DIR__ . '/dateRangePicker';
        parent::init();
    }
}
