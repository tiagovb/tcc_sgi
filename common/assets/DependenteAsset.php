<?php

namespace common\assets;

use kartik\switchinput\SwitchInputAsset;
use yii\web\AssetBundle;
use yii\widgets\ActiveFormAsset;

/**
 * Class DependenteAsset
 */
class DependenteAsset extends AssetBundle
{
    /* @inheritdoc */
    public $js = [
        'js/dependente.js',
    ];

    /* @inheritdoc */
    public $depends = [
        SwitchInputAsset::class,
        ActiveFormAsset::class,
    ];

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->sourcePath = __DIR__ . '/dependente';
        parent::init();
    }
}
