<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class ValidationAsset
 */
class ValidationAsset extends AssetBundle
{
    /* @inheritdoc */
    public $js = [
        'js/validation.js',
    ];

    /* @inheritdoc */
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    /* @inheritdoc */
    public function init()
    {
        $this->sourcePath = __DIR__ . '/validation';
        parent::init();
    }
}
