/* exported DaterangepickerEvents */

var DaterangepickerEvents = {
    show: function(ev, picker) {
        picker.container.find('.ranges').off('mouseenter.daterangepicker', 'li');
        if ($(this).val() === '') {
            picker.container.find('.ranges .active').removeClass('active');
        }
    },
    apply: function(ev, picker) {
        if ($(this).val() === '') {
            $(this).val(picker.startDate.format(picker.locale.format) + picker.locale.separator + picker.endDate.format(picker.locale.format)).trigger('change');
        }
        $(this).submit();
    },
    cancel: function() {
        if ($(this).val() !== '') {
            $(this).val('').trigger('change');
        }
    },
    hideCalendar: function() {
    }
};
