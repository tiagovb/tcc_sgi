/* exported Dependente */
var Dependente = function() {
    return {
        init: function(membroId) {
            $('.btn-update-dependente').on('click', function() {
                var dependenteId = $(this).attr('data-dependente-id');

                if (typeof $('#modalDependenteForm') !== 'undefined') {
                    $('#modalDependenteForm').detach();
                }

                $.ajax({
                    url: '/dependente/update?dependenteId=' + dependenteId
                })
                    .done(function(data) {
                        $('body').append(data);
                        $('#modalDependenteForm').modal('show');
                    });
            });

            $('#btn-create-dependente').on('click', function() {
                if (typeof $('#modalDependenteForm') !== 'undefined') {
                    $('#modalDependenteForm').detach();
                }

                $.ajax({
                    url: '/dependente/create?membroId=' + membroId
                })
                    .done(function(data) {
                        $('body').append(data);
                        $('#modalDependenteForm').modal('show');
                    });
            });
        }
    };
}();
