validar = (function($) {
    var pub = {
            cpf: function(cpf) {
                if (!/^[\d]{3}\.?[\d]{3}\.?[\d]{3}-?[\d]{2}$/.test(cpf)) return false;

                cpf = cpf.replace(/[\D]/g, '');

                if (cpf.replace(eval('/' + cpf.charAt(1) + '/g'), '') == '') return false;

                for (var n = 9; n < 11; n++) {
                    for (var d = 0, c = 0; c < n; c++) d += cpf.charAt(c) * ((n + 1) - c);
                    d = ((10 * d) % 11) % 10;
                    if (cpf.charAt(c) != d) return false;
                }
                return true;
            },

            cnpj: function(cnpj) {
                var b = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];

                if (!/^[0-9]{2}\.?[0-9]{3}\.?[0-9]{3}\/?[0-9]{4}-?[0-9]{2}$/.test(cnpj)) return false;

                cnpj = cnpj.replace(/[\D]/g, '');

                if (/0{14}/.test(cnpj)) return false;

                for (var i = 0, n = 0; i < 12; n += cnpj[i] * b[++i]);
                if (cnpj[12] != (((n %= 11) < 2) ? 0 : 11 - n)) return false;

                for (var i = 0, n = 0; i <= 12; n += cnpj[i] * b[i++]);
                if (cnpj[13] != (((n %= 11) < 2) ? 0 : 11 - n)) return false;

                return true;
            }
        }
        ;

    return pub;
})(jQuery);
