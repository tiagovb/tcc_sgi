<?php

namespace common\components;

use common\components\enum\Enum;
use common\components\enum\EnumSexo;
use common\components\enum\EnumUf;
use common\models\Membro;
use common\models\Usuario;
use yii\helpers\ArrayHelper;

/**
 * @inheritdoc
 *
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Formatter extends \yii\i18n\Formatter
{
    /* @inheritdoc */
    public $dateFormat = 'medium';

    /* @inheritdoc */
    public $decimalSeparator = ',';

    /* @inheritdoc */
    public $thousandSeparator = '.';

    /**
     * @param $value
     *
     * @return mixed
     */
    public function asSexo($value)
    {
        return ArrayHelper::getValue(EnumSexo::$listEnumLabelsAbrev, $value, $this->nullDisplay);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function asUf($value)
    {
        return ArrayHelper::getValue(EnumUf::$listEnumLabels, $value, $this->nullDisplay);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function asNivelEscolar($value)
    {
        return ArrayHelper::getValue(Membro::$listEnumLabels, "nivelEscolar.$value", $this->nullDisplay);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function asEstadoCivil($value)
    {
        return ArrayHelper::getValue(Membro::$listEnumLabels, "estadoCivil.$value", $this->nullDisplay);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function asTipoUsuario($value)
    {
        return ArrayHelper::getValue(Usuario::$listEnumLabels, "tipoUsuario.$value", $this->nullDisplay);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function asStatus($value)
    {
        return ArrayHelper::getValue(Membro::$listEnumLabels, "status.$value", $this->nullDisplay);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function asSimNao($value)
    {
        return ArrayHelper::getValue(Enum::simNao(), $value, $this->nullDisplay);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function asBooleanSimNao($value)
    {
        return ArrayHelper::getValue(Enum::booleanSimNao(), $value, $this->nullDisplay);
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asCPF($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        if (preg_match('/^(\d{3})(\d{3})(\d{3})(\d{2})$/', $value, $matches)) {
            return "$matches[1].$matches[2].$matches[3]-$matches[4]";
        }

        return $value;
    }

    /**
     * @param null $value
     *
     * @return mixed|null
     */
    public function asSomenteNumeros($value = null)
    {
        if ($value) {
            return preg_replace('/[^0-9]+/', '', $value);
        }

        return $value;
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asTelefone($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }
        $value = preg_replace('/[^0-9]/', '', $value);

        if (preg_match('/^(\d{2})(\d{4})\-(\d{4})$/', $value, $matches)) {
            return "($matches[1]) $matches[2]-$matches[3]";
        }

        if (preg_match('/^(\d{2})(\d{4})(\d{4})$/', $value, $matches)) {
            return "($matches[1]) $matches[2]-$matches[3]";
        }

        if (preg_match('/^(\d{2})(\d{5})\-(\d{4})$/', $value, $matches)) {
            return "($matches[1]) $matches[2]-$matches[3]";
        }

        if (preg_match('/^(\d{2})(\d{5})(\d{4})$/', $value, $matches)) {
            return "($matches[1]) $matches[2]-$matches[3]";
        }

        return $value;
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asCEP($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        if (preg_match('/^(\d{5})(\d{3})$/', $value, $matches)) {
            return "$matches[1]-$matches[2]";
        }

        return $value;
    }


    /**
     * @param $str
     * @param $value
     *
     * @return string
     */
    public function getValue($str, $value)
    {
        return ($str === null) ? $this->nullDisplay : $value;
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asAppendMinutos($value)
    {
        return $this->getValue($value, $value . ($value == 1 ? ' ' . \Yii::t('app', 'Minuto') : ' ' . \Yii::t('app', 'Minutos')));
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asAppendDias($value)
    {
        return $this->getValue($value, $value . ($value == 1 ? ' ' . \Yii::t('app', 'Dia') : ' ' . \Yii::t('app', 'Dias')));
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function asCor($value)
    {
        return $this->getValue($value, '<div style="height: 20px; width: 20px; background-color: ' . $value . '"></div>');
    }

    /**
     * @inheritdoc
     */
    public function asDatetime($value, $format = null)
    {
        if ($format === null) {
            $format = 'dd/MM/Y - HH:mm';
        }

        return parent::asDate($value, $format);
    }

    /**
     * @inheritdoc
     */
    public function asDate($value, $format = null)
    {
        if ($format === null) {
            $format = 'dd/MM/YYYY';
        }

        return parent::asDate($value, $format);
    }
}
