<?php

namespace common\components\enum;

/**
 * Class Enum
 */
class Enum
{
    const SIM = 'S';
    const NAO = 'N';

    const BOOLEAN_TRUE = 1;
    const BOOLEAN_FALSE = 0;

    const SEXO_MASCULINO = 'M';
    const SEXO_FEMININO = 'F';

    const PESSOA_FISICA = 'PF';
    const PESSOA_JURIDICA = 'PJ';

    const INTERFACE_ANDROID = 'AN';
    const INTERFACE_EMAIL = 'EM';
    const INTERFACE_IOS = 'IO';
    const INTERFACE_INDEFINIDO = 'XX';
    const INTERFACE_OUTROS = 'OT';
    const INTERFACE_PESSOALMENTE = 'PE';
    const INTERFACE_SMS = 'SM';
    const INTERFACE_SITE = 'ST';
    const INTERFACE_TELEFONE = 'TF';

    const STATUS_ATIVO = 'AT';
    const STATUS_EXCLUIDO = 'EX';
    const STATUS_INATIVO = 'IN';
    const STATUS_PENDENTE = 'PN';

    /**
     * @param null $prompt
     *
     * @return array
     */
    public static function status($prompt = null)
    {
        return array_merge(
            ($prompt ? ['' => $prompt] : []),
            [
                self::STATUS_ATIVO => 'Ativo',
                self::STATUS_EXCLUIDO => 'Excluído',
                self::STATUS_INATIVO => 'Inativo',
                self::STATUS_PENDENTE => 'Pendente',
            ]
        );
    }

    /**
     * @param null $prompt
     *
     * @return array
     */
    public static function ativoInativo($prompt = null)
    {
        return array_merge(
            ($prompt ? ['' => $prompt] : []),
            [self::BOOLEAN_TRUE => 'Ativo', self::BOOLEAN_FALSE => 'Inativo']
        );
    }

    /**
     * @param null $prompt
     *
     * @return array
     */
    public static function simNao($prompt = null)
    {
        return array_merge(
            ($prompt ? ['' => $prompt] : []),
            [self::SIM => 'Sim', self::NAO => 'Não']
        );
    }

    /**
     * @param null $prompt
     *
     * @return mixed
     */
    public static function booleanSimNao($prompt = null)
    {
        if ($prompt) {
            $simNao[''] = $prompt;
        }

        $simNao[self::BOOLEAN_TRUE] = 'Sim';
        $simNao[self::BOOLEAN_FALSE] = 'Não';

        return $simNao;
    }

    /**
     * @param null $prompt
     *
     * @return array
     */
    public static function sexo($prompt = null)
    {
        return array_merge(
            ($prompt ? ['' => $prompt] : []),
            [self::SEXO_MASCULINO => 'Masculino', self::SEXO_FEMININO => 'Feminino']
        );
    }

    /**
     * @param null $prompt
     *
     * @return array
     */
    public static function tipoPessoa($prompt = null)
    {
        return array_merge(
            ($prompt ? ['' => $prompt] : []),
            [self::PESSOA_FISICA => 'Física', self::PESSOA_JURIDICA => 'Jurídica']
        );
    }

    /**
     * @param null $prompt
     *
     * @return array
     */
    public static function interfaceCliente($prompt = null)
    {
        return array_merge(
            ($prompt ? ['' => $prompt] : []),
            [
                self::INTERFACE_INDEFINIDO => 'Indefinido',
                self::INTERFACE_ANDROID => 'Android',
                self::INTERFACE_EMAIL => 'E-mail',
                self::INTERFACE_IOS => 'iOS',
                self::INTERFACE_PESSOALMENTE => 'Pessoalmente',
                self::INTERFACE_SITE => 'Site',
                self::INTERFACE_SMS => 'SMS',
                self::INTERFACE_TELEFONE => 'Telefone',
                self::INTERFACE_OUTROS => 'Outros',
            ]
        );
    }

    /**
     * @param null $prompt
     *
     * @return array
     */
    public static function interfaceClienteManual($prompt = null)
    {
        return array_merge(
            ($prompt ? ['' => $prompt] : []),
            [
                self::INTERFACE_TELEFONE => 'Telefone',
                self::INTERFACE_PESSOALMENTE => 'Pessoalmente',
                self::INTERFACE_EMAIL => 'E-mail',
                self::INTERFACE_OUTROS => 'Outros',
            ]
        );
    }
}
