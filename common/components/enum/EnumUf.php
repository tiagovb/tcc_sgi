<?php

namespace common\components\enum;

/**
 * Enumeration Uf
 */
class EnumUf extends AbstractEnum
{
    const UF_MG = 11;
    const UF_AC = 1;
    const UF_AL = 2;
    const UF_AM = 3;
    const UF_AP = 4;
    const UF_BA = 5;
    const UF_CE = 6;
    const UF_DF = 7;
    const UF_ES = 8;
    const UF_GO = 9;
    const UF_MA = 10;
    const UF_MS = 12;
    const UF_MT = 13;
    const UF_PA = 14;
    const UF_PB = 15;
    const UF_PE = 16;
    const UF_PI = 17;
    const UF_PR = 18;
    const UF_RJ = 19;
    const UF_RN = 20;
    const UF_RO = 21;
    const UF_RR = 22;
    const UF_RS = 23;
    const UF_SC = 24;
    const UF_SE = 25;
    const UF_SP = 26;
    const UF_TO = 27;

    /* @inheritdoc */
    public static $listEnumLabels = [
        self::UF_MG => 'Minas Gerais',
        self::UF_AC => 'Acre',
        self::UF_AL => 'Alagoas',
        self::UF_AM => 'Amazonas',
        self::UF_AP => 'Amapá',
        self::UF_BA => 'Bahia',
        self::UF_CE => 'Ceará',
        self::UF_DF => 'Distrito Federal',
        self::UF_ES => 'Espírito Santo',
        self::UF_GO => 'Goiás',
        self::UF_MA => 'Maranhão',
        self::UF_MS => 'Mato Grosso do Sul',
        self::UF_MT => 'Mato Grosso',
        self::UF_PA => 'Pará',
        self::UF_PB => 'Paraiba',
        self::UF_PE => 'Pernambuco',
        self::UF_PI => 'Piauí',
        self::UF_PR => 'Paraná',
        self::UF_RJ => 'Rio de Janeiro',
        self::UF_RN => 'Rio Grande do Norte',
        self::UF_RO => 'Rondônia',
        self::UF_RR => 'Roraima',
        self::UF_RS => 'Rio Grande do Sul',
        self::UF_SC => 'Santa Catarina',
        self::UF_SE => 'Sergipe',
        self::UF_SP => 'São Paulo',
        self::UF_TO => 'Tocantins',
    ];
}
