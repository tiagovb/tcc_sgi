<?php

namespace common\components\validators;

use common\assets\ValidationAsset;
use Yii;
use yii\validators\Validator;

/**
 * Class CPFValidator
 */
class CPFValidator extends Validator
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->message === null) {
            $this->message = Yii::t('yii', 'CPF inválido.');
        }

        parent::init();
    }

    /** {@inheritdoc} */
    public function clientValidateAttribute($model, $attribute, $view)
    {
        ValidationAsset::register($view);

        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return "if (value !== '' && !validar.cpf(value)) { messages.push($message); }";
    }

    /**
     * @inheritdoc
     */
    protected function validateValue($value)
    {
        $valid = true;

        $value = preg_replace('/[^0-9]/', '', $value);

        if (strlen($value) !== 11) {
            $valid = false;
        } else {
            $repetidos = [];
            for ($i = 0; $i <= 9; $i++) {
                $repetidos[] = str_pad('', 11, $i);
            }

            if (in_array($value, $repetidos)) {
                $valid = false;
            } else {
                for ($t = 9; $t < 11; $t++) {
                    for ($d = 0, $c = 0; $c < $t; $c++) {
                        $d += $value{$c} * (($t + 1) - $c);
                    }
                    $d = ((10 * $d) % 11) % 10;
                    if ($value{$c} != $d) {
                        $valid = false;
                        break;
                    }
                }
            }
        }

        return $valid ? null : [$this->message, []];
    }
}
