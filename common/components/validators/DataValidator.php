<?php

namespace common\components\validators;

use Yii;
use yii\helpers\Json;
use yii\validators\DateValidator;
use yii\validators\ValidationAsset;
use yii\web\JsExpression;

/**
 * Class DataValidator
 */
class DataValidator extends DateValidator
{
    /**
     * @inheritdoc
     */
    public $format = 'd/m/Y';

    /* @var string */
    public $pattern = '/^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[012])\/(18|19|20)\d{2}$/';

    /**
     * @inheritdoc
     */
    public function validateAttribute($object, $attribute)
    {
        /** @noinspection PhpVariableVariableInspection */
        $value = $object->$attribute;
        if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $value)) {
            $this->format = 'Y-m-d';
        }

        $result = $this->validateValue($value);
        if (null !== $result) {
            $this->addError($object, $attribute, $result[0], $result[1]);
        } else {
            $date = \DateTime::createFromFormat($this->format, $value);
            $object->{$attribute} = $date->format('Y-m-d');
        }
    }

    /**
     * @inheritdoc
     */
    public function clientValidateAttribute($object, $attribute, $view)
    {
        $options = [
            'pattern' => new JsExpression($this->pattern),
            'message' => Yii::$app->getI18n()->format($this->message, [
                'attribute' => $object->getAttributeLabel($attribute),
            ], Yii::$app->language),
        ];

        if ($this->skipOnEmpty) {
            $options['skipOnEmpty'] = 1;
        }

        ValidationAsset::register($view);

        return 'yii.validation.regularExpression(value, messages, ' . Json::encode($options) . ');';
    }
}
