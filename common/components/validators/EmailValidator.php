<?php

namespace common\components\validators;

use GuzzleHttp\Client;
use Yii;
use yii\helpers\Json;

/**
 * Class EmailValidator
 */
class EmailValidator extends \yii\validators\EmailValidator
{
    /**
     * @inheridoc
     */
    public $message = 'Digite um endereço de e-mail válido.';

    /**
     * @inheritDoc
     */
    public function validateAttribute($model, $attribute)
    {
        parent::validateAttribute($model, $attribute);

        if (getenv('BRITEVERIFY_ENABLE') == '1') {
            $isEmailValido = EmailValidator::validarEmailBriteverify($model->$attribute);
            if (!$isEmailValido) {
                $model->addError($attribute, Yii::t('app', $this->message));
            }
        }
    }

    /**
     * @param $value
     *
     * @return bool
     */
    public static function validarEmailBriteverify($value)
    {
        $apiKey = getenv('BRITEVERIFY_API_KEY');
        $client = new Client();
        $response = false;
        try {
            $response = $client->request('GET', 'https://bpi.briteverify.com/emails.json', [
                'query' => [
                    'address' => $value,
                    'apikey' => $apiKey,
                ],
                'connect_timeout' => 5,
            ]);
        } catch (\RuntimeException $e) {
            Yii::warning('Falha na requisição para https://bpi.briteverify.com/emails.json (validação de email)');
        }

        if ($response && $response->getStatusCode() == 200) {
            $response = Json::decode($response->getBody());
            if ($response['status'] === 'invalid') {
                return false;
            }
        }

        return true;
    }
}
