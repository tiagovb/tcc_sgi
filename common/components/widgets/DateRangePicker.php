<?php

namespace common\components\widgets;

use common\assets\DateRangePickerAsset;
use Yii;

/**
 * Class DateRangePicker
 */
class DateRangePicker extends \kartik\daterange\DateRangePicker
{
    /**
     * @var array
     */
    public $pluginEvents = [
        'apply.daterangepicker' => 'DaterangepickerEvents.apply',
        'show.daterangepicker' => 'DaterangepickerEvents.show',
        'cancel.daterangepicker' => 'DaterangepickerEvents.cancel',
        'hide.daterangepicker' => 'DaterangepickerEvents.hideCalendar',
    ];

    /**
     * @var array
     */
    public $pluginOptions = [
        'locale' => [
            'format' => 'd/m/Y',
        ],
    ];

    /**
     * @var bool
     */
    public $convertFormat = true;

    /**
     * @var array
     */
    public $options = [
        'readonly' => 'readonly',
        'class' => 'form-control',
    ];

    /**
     * @var bool
     */
    public $presetDropdown = false;

    /**
     * @inheritDoc
     */
    public function registerAssets()
    {
        DateRangePickerAsset::register($this->getView());
        parent::registerAssets();

        return;
    }

    /**
     * @inheritDoc
     */
    protected function setLanguage($prefix, $assetPath = null, $filePath = null, $suffix = '.js')
    {
        parent::setLanguage($prefix, Yii::getAlias('@vendor/kartik-v/yii2-date-range/assets/'), $filePath, $suffix);
    }
}
