<?php

namespace common\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView as YiiDetailView;

/** {@inheritdoc} */
class DetailView extends YiiDetailView
{
    /** {@inheritdoc} */
    public $template = '<tr{rowOptions}><th{captionOptions}>{label}</th><td{contentOptions}>{value}</td></tr>';

    /** {@inheritdoc} */
    protected function renderAttribute($attribute, $index)
    {
        if (is_string($this->template)) {
            $captionOptions = Html::renderTagAttributes(ArrayHelper::getValue($attribute, 'captionOptions', []));
            $contentOptions = Html::renderTagAttributes(ArrayHelper::getValue($attribute, 'contentOptions', []));
            $rowOptions = Html::renderTagAttributes(ArrayHelper::getValue($attribute, 'rowOptions', []));

            return strtr($this->template, [
                '{label}' => $attribute['label'],
                '{value}' => $this->formatter->format($attribute['value'], $attribute['format']),
                '{captionOptions}' => $captionOptions,
                '{contentOptions}' => $contentOptions,
                '{rowOptions}' => $rowOptions,
            ]);
        } else {
            return call_user_func($this->template, $attribute, $index, $this);
        }
    }
}
