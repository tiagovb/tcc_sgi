<?php

use common\components\Formatter;

return [
    'name' => 'SGI',
    'language' => 'pt-BR',
    'sourceLanguage' => 'pt-BR',
    'timeZone' => 'America/Sao_Paulo',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'formatter' => [
            'class' => Formatter::class,
            'defaultTimeZone' => 'America/Sao_Paulo',
            'timeZone' => 'America/Sao_Paulo',
            'nullDisplay' => ' - ',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
    'modules' => [
        'datecontrol' => [
            'class' => \kartik\datecontrol\Module::class,
            'ajaxConversion' => false,
            'displaySettings' => [
                \kartik\datecontrol\Module::FORMAT_DATE => 'dd/MM/yyyy',
                \kartik\datecontrol\Module::FORMAT_TIME => 'HH:mm',
                \kartik\datecontrol\Module::FORMAT_DATETIME => 'dd/MM/yyyy HH:mm',
            ],
            'saveSettings' => [
                \kartik\datecontrol\Module::FORMAT_DATE => 'php:Y-m-d',
                \kartik\datecontrol\Module::FORMAT_TIME => 'php:H:i:s',
                \kartik\datecontrol\Module::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
            ],
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ]
    ],
];
