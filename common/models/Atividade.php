<?php

namespace common\models;


/**
 * This is the model class for table "Atividade".
 *
 * @property int $id
 * @property int $igreja_id
 * @property string $dataHora
 * @property string $descricao
 *
 * @property Igreja $igreja
 */
class Atividade extends MainModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Atividade';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['igreja_id', 'dataHora', 'descricao'], 'required'],
            [['igreja_id'], 'integer'],
            [['dataHora'], 'safe'],
            [['descricao'], 'string', 'max' => 256],
            [['igreja_id'], 'exist', 'skipOnError' => true, 'targetClass' => Igreja::className(), 'targetAttribute' => ['igreja_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'igreja_id' => 'Igreja ID',
            'dataHora' => 'Data e Hora',
            'descricao' => 'Descrição',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIgreja()
    {
        return $this->hasOne(Igreja::className(), ['id' => 'igreja_id']);
    }
}
