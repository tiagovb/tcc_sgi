<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Compromisso".
 *
 * @property int $id
 * @property int $usuario_id
 * @property string $descricao
 * @property string $data
 * @property string $localizacao
 *
 * @property Usuario $usuario
 */
class Compromisso extends MainModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Compromisso';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usuario_id', 'descricao', 'data'], 'required'],
            [['usuario_id'], 'integer'],
            [['data'], 'safe'],
            [['descricao', 'localizacao'], 'string', 'max' => 256],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_id' => 'Usuario ID',
            'descricao' => 'Descrição',
            'data' => 'Data',
            'localizacao' => 'Localização',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }

    public function beforeValidate()
    {
        $this->setUsuario();

        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        $this->setUsuario();

        return parent::beforeSave($insert);
    }


    protected function setUsuario()
    {
        $this->setAttribute('usuario_id', Yii::$app->user->identity->getId());
    }
}
