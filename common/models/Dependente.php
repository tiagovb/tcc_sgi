<?php

namespace common\models;

use common\components\interfaces\EnumInterface;
use common\components\traits\EnumTrait;

/**
 * This is the model class for table "Dependente".
 *
 * @property int $id
 * @property int $membro_id
 * @property string $nome
 * @property string $dtNascimento
 * @property int $religiao
 *
 * @property Membro $membro
 */
class Dependente extends MainModel implements EnumInterface
{
    use EnumTrait;

    const RELIGIAO_EVANGELICA = 1;
    const RELIGIAO_CATOLICA = 2;
    const RELIGIAO_ESPIRITA = 3;
    const RELIGIAO_SEM_RELIGIAO = 4;
    const RELIGIAO_OUTROS = 5;

    /**
     * @var array
     */
    public static $listEnumLabels = [
        'religiao' => [
            self::RELIGIAO_EVANGELICA => 'Evangélica',
            self::RELIGIAO_CATOLICA => 'Católica',
            self::RELIGIAO_ESPIRITA => 'Espírita',
            self::RELIGIAO_SEM_RELIGIAO => 'Sem religião',
            self::RELIGIAO_OUTROS => 'Outros',
        ],
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Dependente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['membro_id', 'nome'], 'required'],
            [['membro_id', 'religiao'], 'integer'],
            [['dtNascimento'], 'safe'],
            [['nome'], 'string', 'max' => 255],
            [['membro_id'], 'exist', 'skipOnError' => true, 'targetClass' => Membro::className(), 'targetAttribute' => ['membro_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'membro_id' => 'Membro',
            'nome' => 'Nome',
            'dtNascimento' => 'Data de Nascimento',
            'religiao' => 'Religião',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMembro()
    {
        return $this->hasOne(Membro::className(), ['id' => 'membro_id']);
    }
}
