<?php

namespace common\models;

use common\components\interfaces\EnumInterface;
use common\components\traits\EnumTrait;

/**
 * This is the model class for table "Despesa".
 *
 * @property int $id
 * @property int $igreja_id
 * @property string $dtReferencia
 * @property string $valor
 * @property string $descricao
 * @property string $notaFiscal
 * @property string $quantidade
 * @property int $tipo
 *
 * @property Igreja $igreja
 */
class Despesa extends MainModel implements EnumInterface
{
    use EnumTrait;

    const TIPO_MATERIAL_LIMPEZA = 1;
    const TIPO_ESCRITORIO = 2;
    const TIPO_ALIMENTACAO = 3;
    const TIPO_OUTROS = 4;

    /**
     * @var array
     */
    public static $listEnumLabels = [
        'tipo' => [
            self::TIPO_MATERIAL_LIMPEZA => 'Material de Limpeza',
            self::TIPO_ESCRITORIO => 'Escritório',
            self::TIPO_ALIMENTACAO => 'Alimentação',
            self::TIPO_OUTROS => 'Outros',
        ],
    ];


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Despesa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['igreja_id', 'dtReferencia', 'valor', 'descricao', 'quantidade', 'tipo'], 'required'],
            [['igreja_id', 'tipo'], 'integer'],
            [['dtReferencia'], 'safe'],
            [['valor', 'quantidade'], 'number', 'min' => 0.01,'tooSmall' => '{attribute} deve ser maior que 0'],
            [['descricao'], 'string', 'max' => 256],
            [['notaFiscal'], 'string', 'max' => 100],
            [['igreja_id'], 'exist', 'skipOnError' => true, 'targetClass' => Igreja::className(), 'targetAttribute' => ['igreja_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'igreja_id' => 'Igreja ID',
            'dtReferencia' => 'Referência',
            'valor' => 'Valor',
            'descricao' => 'Descrição',
            'notaFiscal' => 'Nota Fiscal',
            'quantidade' => 'Quantidade',
            'tipo' => 'Tipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIgreja()
    {
        return $this->hasOne(Igreja::className(), ['id' => 'igreja_id']);
    }
}
