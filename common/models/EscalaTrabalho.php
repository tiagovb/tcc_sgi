<?php

namespace common\models;

use common\components\interfaces\EnumInterface;
use common\components\traits\EnumTrait;

/**
 * This is the model class for table "EscalaTrabalho".
 *
 * @property int $membro_id
 * @property int $funcao_id
 * @property int $turno
 * @property string $data
 *
 * @property Membro $membro
 * @property Funcao $funcao
 */
class EscalaTrabalho extends MainModel implements EnumInterface
{
    use EnumTrait;

    const TURNO_MANHA = 1;
    const TURNO_TARDE = 2;
    const TURNO_NOITE = 3;

    /**
     * @var array
     */
    public static $listEnumLabels = [
        'turno' => [
            self::TURNO_MANHA => 'Manhã',
            self::TURNO_TARDE => 'Tarde',
            self::TURNO_NOITE => 'Noite',
        ],
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EscalaTrabalho';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['membro_id', 'funcao_id', 'data', 'turno'], 'required'],
            [['membro_id', 'funcao_id', 'turno'], 'integer'],
            [['membro_id'], 'exist', 'skipOnError' => true, 'targetClass' => Membro::className(), 'targetAttribute' => ['membro_id' => 'id']],
            [['funcao_id'], 'exist', 'skipOnError' => true, 'targetClass' => Funcao::className(), 'targetAttribute' => ['funcao_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'membro_id' => 'Membro',
            'funcao_id' => 'Função',
            'turno' => 'Turno',
            'data' => 'Data',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMembro()
    {
        return $this->hasOne(Membro::className(), ['id' => 'membro_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFuncao()
    {
        return $this->hasOne(Funcao::className(), ['id' => 'funcao_id']);
    }
}
