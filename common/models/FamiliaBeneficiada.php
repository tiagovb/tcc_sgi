<?php

namespace common\models;

use common\components\interfaces\EnumInterface;
use common\components\traits\EnumTrait;
use common\components\validators\CPFValidator;

/**
 * Class FamiliaBeneficiada
 *
 * @property int $necessidadeAuxilio
 * @property string $observacoesAuxilio
 */
class FamiliaBeneficiada extends Membro implements EnumInterface
{
    use /** @noinspection TraitsPropertiesConflictsInspection */
        EnumTrait;

    const NECESSIDADE_BAIXA = 1;
    const NECESSIDADE_MEDIA = 2;
    const NECESSIDADE_ALTA = 3;

    /**
     * @var array
     */
    public static $listEnumLabels = [
        'necessidadeAuxilio' => [
            self::NECESSIDADE_BAIXA => 'Baixa',
            self::NECESSIDADE_MEDIA => 'Média',
            self::NECESSIDADE_ALTA => 'Alta',
        ],
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['conjuge_id', 'igreja_id', 'sexo', 'nivelEscolar', 'estadoCivil', 'necessidadeAuxilio', 'status'], 'integer'],
            [
                ['igreja_id', 'nome', 'sexo', 'logradouro', 'numero', 'cidade', 'estado', 'dtNascimento', 'estadoCivil', 'status', 'celular', 'necessidadeAuxilio'],
                'required'
            ],
            [['dtNascimento', 'dtCasamento', 'dtCadastro', 'dtStatus'], 'safe'],
            [['nome', 'nomePai', 'nomeMae', 'logradouro', 'numero', 'complemento', 'cidade', 'telefone', 'celular', 'email', 'naturalidade', 'rg', 'cpf'], 'string', 'max' => 256],
            [['cpf'], CPFValidator::class],
            [['cpf'], 'unique', 'skipOnEmpty' => true],
            [['email'], 'email'],
            [['estado'], 'string', 'max' => 2],
            [['cep'], 'string', 'max' => 9],
            [['observacoesAuxilio'], 'string', 'max' => 500],
            [['observacoesAuxilio'], 'string', 'max' => 500],
            [['conjuge_id'], 'exist', 'skipOnError' => true, 'targetClass' => Membro::className(), 'targetAttribute' => ['conjuge_id' => 'id']],
            [['igreja_id'], 'exist', 'skipOnError' => true, 'targetClass' => Igreja::className(), 'targetAttribute' => ['igreja_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(),
            [
                'nome' => 'Responsável',
                'necessidadeAuxilio' => 'Necessidade de Auxílio',
                'observacoesAuxilio' => 'Observações de Auxílio',
            ]);
    }

    public function beforeSave($insert)
    {
        $this->setStatus();

        return parent::beforeSave($insert);
    }

    private function setStatus()
    {
        $this->status = $this->isNewRecord ? Membro::STATUS_INATIVO : $this->status;
    }

    public static function find()
    {
        return parent::find()->andWhere(['IS NOT', 'necessidadeAuxilio', null]);
    }
}
