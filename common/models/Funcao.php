<?php

namespace common\models;

/**
 * This is the model class for table "Funcao".
 *
 * @property int $id
 * @property string $nome
 * @property string $descricao
 */
class Funcao extends MainModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Funcao';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome'], 'required'],
            [['descricao'], 'string'],
            [['nome'], 'string', 'max' => 256],
            [['nome'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'descricao' => 'Descrição',
        ];
    }

    /**
     * @return bool
     */
    public function canDelete()
    {
        return !EscalaTrabalho::find()->andWhere(['funcao_id' => $this->id])->exists();
    }
}
