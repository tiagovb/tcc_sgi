<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "Igreja".
 *
 * @property int $id
 * @property string $razaoSocial
 * @property string $logradouro
 * @property string $numero
 * @property string $complemento
 * @property string $bairro
 * @property int $cep
 * @property string $cidade
 * @property int $uf
 *
 * @property Atividade[] $atividades
 * @property Despesa[] $despesas
 * @property Membro[] $membros
 * @property Patrimonio[] $patrimonios
 */
class Igreja extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Igreja';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cep', 'uf'], 'integer'],
            [['razaoSocial', 'logradouro', 'numero', 'complemento', 'bairro', 'cidade'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'razaoSocial' => 'Razao Social',
            'logradouro' => 'Logradouro',
            'numero' => 'Numero',
            'complemento' => 'Complemento',
            'bairro' => 'Bairro',
            'cep' => 'Cep',
            'cidade' => 'Cidade',
            'uf' => 'Uf',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAtividades()
    {
        return $this->hasMany(Atividade::className(), ['igreja_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDespesas()
    {
        return $this->hasMany(Despesa::className(), ['igreja_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMembros()
    {
        return $this->hasMany(Membro::className(), ['igreja_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatrimonios()
    {
        return $this->hasMany(Patrimonio::className(), ['igreja_id' => 'id']);
    }
}
