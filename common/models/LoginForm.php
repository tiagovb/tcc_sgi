<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\db\Expression;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Login',
            'password' => 'Senha',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Login ou senha incorretos.');
            }
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return Usuario
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = Usuario::findByUsername($this->username);
        }

        return $this->_user;
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $login = Yii::$app->user->login($this->getUser());

            if ($login) {
                $this->getUser()->updateAttributes([
                    'dtUltimoAcesso' => new Expression('NOW()')
                ]);
            }

            return $login;
        }

        return false;
    }
}
