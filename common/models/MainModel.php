<?php

namespace common\models;

use Yii;

class MainModel extends \yii\db\ActiveRecord
{
    public function beforeValidate()
    {
        $this->setIgreja();

        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        if ($this->hasIgreja() && !$this->igreja_id) {
            $this->setIgreja();
        }

        return parent::beforeSave($insert);
    }


    protected function setIgreja()
    {
        if ($this->hasIgreja()) {
            $this->setAttribute('igreja_id', Yii::$app->user->identity->igreja_id);
        }
    }

    /**
     * @return bool
     */
    protected function hasIgreja()
    {
        return in_array('igreja_id', $this->attributes(), true);
    }

    /**
     * @return string
     */
    public function colorTypeBox()
    {
        return $this->isNewRecord ? 'success' : 'primary';
    }
}
