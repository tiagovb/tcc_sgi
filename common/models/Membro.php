<?php

namespace common\models;

use common\components\interfaces\EnumInterface;
use common\components\traits\EnumTrait;
use common\components\validators\CPFValidator;
use yii\db\Expression;
use yii\db\Query;

/**
 * This is the model class for table "Membro".
 *
 * @property int $id
 * @property int $conjuge_id
 * @property int $igreja_id
 * @property string $nome
 * @property string $nomePai
 * @property string $nomeMae
 * @property int $sexo
 * @property string $logradouro
 * @property string $numero
 * @property string $complemento
 * @property string $cidade
 * @property string $estado
 * @property string $cep
 * @property string $telefone
 * @property string $celular
 * @property string $email
 * @property int $nivelEscolar
 * @property string $dtNascimento
 * @property string $naturalidade
 * @property string $rg
 * @property string $cpf
 * @property int $estadoCivil
 * @property string $dtCasamento
 * @property string $dtCadastro
 * @property int $status
 * @property string $dtStatus
 * @property string $observacoes
 *
 * @property Dependente[] $dependentes
 * @property EscalaTrabalho[] $escalaTrabalhos
 * @property Funcao[] $funcaos
 * @property Membro $conjuge
 * @property Igreja $igreja
 */
class Membro extends MainModel implements EnumInterface
{
    use EnumTrait;

    const NIVEL_ESCOLAR_FUNDAMENTAL = 1;
    const NIVEL_ESCOLAR_MEDIO = 2;
    const NIVEL_ESCOLAR_SUPERIOR_INCOMPLETO = 3;
    const NIVEL_ESCOLAR_SUPERIOR_COMPLETO = 4;

    const ESTADO_CIVIL_SOLTEIRO = 1;
    const ESTADO_CIVIL_CASADO = 2;
    const ESTADO_CIVIL_DIVORCIADO = 3;
    const ESTADO_CIVIL_SEPARADO = 4;
    const ESTADO_CIVIL_VIUVO = 5;

    const STATUS_MEMBRO = 1;
    const STATUS_INATIVO = 2;
    const STATUS_PASTOR = 3;
    const STATUS_LIDER_MINISTERIO = 4;

    /**
     * @var array
     */
    public static $listEnumLabels = [
        'nivelEscolar' => [
            self::NIVEL_ESCOLAR_FUNDAMENTAL => 'Fundamental',
            self::NIVEL_ESCOLAR_MEDIO => 'Médio',
            self::NIVEL_ESCOLAR_SUPERIOR_INCOMPLETO => 'Superior Incompleto',
            self::NIVEL_ESCOLAR_SUPERIOR_COMPLETO => 'Superior Completo',
        ],
        'nivelEscolarAbrev' => [
            self::NIVEL_ESCOLAR_FUNDAMENTAL => 'Fund.',
            self::NIVEL_ESCOLAR_MEDIO => 'Méd.',
            self::NIVEL_ESCOLAR_SUPERIOR_INCOMPLETO => 'Sup. Incomp.',
            self::NIVEL_ESCOLAR_SUPERIOR_COMPLETO => 'Sup. Comp.',
        ],
        'estadoCivil' => [
            self::ESTADO_CIVIL_SOLTEIRO => 'Solteiro(a)',
            self::ESTADO_CIVIL_CASADO => 'Casado(a)',
            self::ESTADO_CIVIL_DIVORCIADO => 'Divorciado(a)',
            self::ESTADO_CIVIL_SEPARADO => 'Separado(a)',
            self::ESTADO_CIVIL_VIUVO => 'Viúvo(a)',
        ],
        'status' => [
            self::STATUS_MEMBRO => 'Membro',
            self::STATUS_PASTOR => 'Pastor',
            self::STATUS_LIDER_MINISTERIO => 'Líder Ministério',
            self::STATUS_INATIVO => 'Inativo',
        ],
        'statusAbrev' => [
            self::STATUS_INATIVO => 'Inat.',
            self::STATUS_MEMBRO => 'Membro',
            self::STATUS_PASTOR => 'Pastor',
            self::STATUS_LIDER_MINISTERIO => 'Líd. Minist.',
        ],
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Membro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['conjuge_id', 'igreja_id', 'sexo', 'nivelEscolar', 'estadoCivil', 'necessidadeAuxilio', 'status'], 'integer'],
            [
                ['igreja_id', 'nome', 'sexo', 'logradouro', 'numero', 'cidade', 'estado', 'nivelEscolar', 'dtNascimento', 'naturalidade', 'estadoCivil', 'status'],
                'required'
            ],
            [['dtNascimento', 'dtCasamento', 'dtCadastro', 'dtStatus'], 'safe'],
            [['nome', 'nomePai', 'nomeMae', 'logradouro', 'numero', 'complemento', 'cidade', 'telefone', 'celular', 'email', 'naturalidade', 'rg', 'cpf'], 'string', 'max' => 256],
            [['cpf'], CPFValidator::class],
            [['cpf'], 'verificarCpf'],
            [['email'], 'email'],
            [['estado'], 'string', 'max' => 2],
            [['cep'], 'string', 'max' => 9],
            [['observacoesAuxilio', 'observacoes'], 'string', 'max' => 500],
            [['conjuge_id'], 'exist', 'skipOnError' => true, 'targetClass' => Membro::className(), 'targetAttribute' => ['conjuge_id' => 'id']],
            [['igreja_id'], 'exist', 'skipOnError' => true, 'targetClass' => Igreja::className(), 'targetAttribute' => ['igreja_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'conjuge_id' => 'Cônjuge',
            'nome' => 'Nome',
            'nomePai' => 'Nome do Pai',
            'nomeMae' => 'Nome da Mãe',
            'sexo' => 'Sexo',
            'logradouro' => 'Logradouro',
            'numero' => 'Numero',
            'complemento' => 'Complemento',
            'cidade' => 'Cidade',
            'estado' => 'Estado',
            'cep' => 'Cep',
            'telefone' => 'Telefone',
            'celular' => 'Celular',
            'email' => 'Email',
            'nivelEscolar' => 'Nivel Escolar',
            'dtNascimento' => 'Data de Nascimento',
            'naturalidade' => 'Naturalidade',
            'rg' => 'Rg',
            'cpf' => 'Cpf',
            'estadoCivil' => 'Estado Civil',
            'dtCasamento' => 'Data de Casamento',
            'status' => 'Status',
            'dtStatus' => 'Data do Status',
            'dtCadastro' => 'Data de Cadastro',
            'observacoes' => 'Observações',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDependentes()
    {
        return $this->hasMany(Dependente::className(), ['membro_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEscalaTrabalhos()
    {
        return $this->hasMany(EscalaTrabalho::className(), ['membro_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFuncaos()
    {
        return $this->hasMany(Funcao::className(), ['id' => 'funcao_id'])->viaTable('EscalaTrabalho', ['membro_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConjuge()
    {
        return $this->hasOne(Membro::className(), ['id' => 'conjuge_id'])->inverseOf('conjuge');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIgreja()
    {
        return $this->hasOne(Igreja::className(), ['id' => 'igreja_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $conjuge = Membro::findOne($this->conjuge_id);

        if ($conjuge) {
            $conjuge->updateAttributes([
                'conjuge_id' => $this->conjuge_id,
            ]);
        }

        if (array_key_exists('status', $changedAttributes)) {
            $this->updateAttributes([
                'dtStatus' => new Expression('NOW()'),
            ]);
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->setAttributes([
                'dtStatus' => new Expression('NOW()'),
                'dtCadastro' => new Expression('NOW()'),
            ]);
        }

        return parent::beforeSave($insert);
    }

    public function verificarCpf()
    {
        $existsCpf = (new Query())->from('Membro')
            ->andWhere([
                'Membro.cpf' => $this->cpf,
                'Membro.necessidadeAuxilio' => null,
            ])
            ->andWhere(['!=', 'Membro.id', $this->id])
            ->exists();

        if ($this->cpf && $existsCpf) {
            $this->addError('cpf', 'CPF já utilizado');
        }
    }
}
