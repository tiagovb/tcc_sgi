<?php

namespace common\models;

use common\components\interfaces\EnumInterface;
use common\components\traits\EnumTrait;

/**
 * This is the model class for table "Patrimonio".
 *
 * @property int $id
 * @property int $igreja_id
 * @property string $descricao
 * @property string $dtAquisicao
 * @property string $valor
 * @property int $tipoAquisicao
 * @property string $observacoes
 * @property string $notaFiscal
 * @property int $status
 *
 * @property Igreja $igreja
 */
class Patrimonio extends MainModel implements EnumInterface
{
    use EnumTrait;

    const AQUISICAO_DOADO = 1;
    const AQUISICAO_COMPRADO = 2;

    const STATUS_ATIVO = 1;
    const STATUS_BAIXADO = 2;

    /**
     * @var array
     */
    public static $listEnumLabels = [
        'aquisicao' => [
            self::AQUISICAO_DOADO => 'Doado',
            self::AQUISICAO_COMPRADO => 'Comprado',
        ],
        'status' => [
            self::STATUS_ATIVO => 'Ativo',
            self::STATUS_BAIXADO => 'Baixado',
        ],
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Patrimonio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['igreja_id', 'descricao', 'dtAquisicao', 'tipoAquisicao', 'status'], 'required'],
            [['dtAquisicao'], 'safe'],
            [
                ['valor'],
                'required',
                'when' => function ($model) {
                    return $model->tipoAquisicao == self::AQUISICAO_COMPRADO;
                },
                'whenClient' => "function (attribute, value) {
                    return $('#patrimonio-tipoaquisicao').val() == " . self::AQUISICAO_COMPRADO . ';
                }',
                'message' => 'Valor deve ser informado quando patrimônio for comprado.'
            ],
            [['descricao', 'observacoes', 'notaFiscal'], 'string', 'max' => 256],
            [['igreja_id'], 'exist', 'skipOnError' => true, 'targetClass' => Igreja::className(), 'targetAttribute' => ['igreja_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'igreja_id' => 'Igreja ID',
            'descricao' => 'Descrição',
            'dtAquisicao' => 'Data de Aquisição',
            'valor' => 'Valor',
            'tipoAquisicao' => 'Tipo de Aquisição',
            'observacoes' => 'Observações',
            'notaFiscal' => 'Nota Fiscal',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIgreja()
    {
        return $this->hasOne(Igreja::className(), ['id' => 'igreja_id']);
    }
}
