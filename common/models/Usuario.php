<?php

namespace common\models;

use common\components\interfaces\EnumInterface;
use common\components\traits\EnumTrait;
use Yii;
use yii\base\NotSupportedException;
use yii\db\Expression;
use yii\web\IdentityInterface;

/**
 * @property int $id
 * @property int $igreja_id
 * @property string $login
 * @property string $hashSenha
 * @property int $tipoUsuario
 * @property string $dtUltimoAcesso
 * @property int $tempoOciosoMax
 *
 * @property Compromisso[] $compromissos
 * @property Igreja $igreja
 */
class Usuario extends MainModel implements IdentityInterface, EnumInterface
{
    use EnumTrait;

    /** Tempo em minutos */
    const TEMPO_OCIOSO_MAX_PADRAO = 20;

    const TIPO_GERENTE = 1;
    const TIPO_AUXILIAR = 2;

    const SCENARIO_ALTERAR_SENHA = 'scenarioAlterarSenha';
    const SCENARIO_CADASTRO = 'scenarioCadastro';

    /** @var string */
    public $password;

    /** @var string */
    public $passwordConfirm;

    /**
     * @var array
     */
    public static $listEnumLabels = [
        'tipoUsuario' => [
            self::TIPO_GERENTE => 'Gerente',
            self::TIPO_AUXILIAR => 'Auxiliar Administrativo',
        ],
    ];

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_ALTERAR_SENHA => ['password', 'passwordConfirm']
        ]);
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Usuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['login', 'trim'],
            [['igreja_id', 'login', 'hashSenha', 'tipoUsuario'], 'required'],
            [['igreja_id', 'login', 'hashSenha', 'tipoUsuario', 'password', 'passwordConfirm'], 'required', 'on' => self::SCENARIO_CADASTRO],
            [['password', 'passwordConfirm'], 'required', 'on' => self::SCENARIO_ALTERAR_SENHA],
            ['password', 'string', 'length' => [4, 24]],
            ['passwordConfirm', 'compare', 'compareAttribute' => 'password'],
            [['igreja_id', 'tipoUsuario', 'tempoOciosoMax'], 'integer'],
            [['dtUltimoAcesso'], 'safe'],
            [['login'], 'string', 'max' => 100],
            [['hashSenha'], 'string', 'max' => 150],
            [['tempoOciosoMax'], 'integer', 'max' => 60, 'min' => 10],
            [['login'], 'unique'],
            [['hashSenha'], 'unique'],
            [['igreja_id'], 'exist', 'skipOnError' => true, 'targetClass' => Igreja::className(), 'targetAttribute' => ['igreja_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'igreja_id' => 'Igreja ID',
            'login' => 'Login',
            'tipoUsuario' => 'Tipo de Usuário',
            'dtUltimoAcesso' => 'Último Acesso',
            'tempoOciosoMax' => 'Tempo Ocioso Máximo',
            'password' => 'Senha',
            'passwordConfirm' => 'Confirmação da senha',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompromissos()
    {
        return $this->hasMany(Compromisso::className(), ['usuario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIgreja()
    {
        return $this->hasOne(Igreja::className(), ['id' => 'igreja_id']);
    }

    /**
     * Removes password reset token
     */
    public function isGerente()
    {
        return (int)$this->tipoUsuario === self::TIPO_GERENTE;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->hashSenha);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->hashSenha = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return false;
    }

    public static function setTempoOcioso()
    {
        $user = Yii::$app->user;
        if (!$user->isGuest && $user->identity->tempoOciosoMax) {
            $user->authTimeout = $user->identity->tempoOciosoMax * 60;
            Yii::$app->session->set($user->authTimeoutParam, time() + $user->authTimeout);
        }
    }

    public function saveUltimoLogin()
    {
        $this->updateAttributes(['dtUltimoAcesso' => new Expression('NOW()')]);
    }
}
