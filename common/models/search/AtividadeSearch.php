<?php

namespace common\models\search;

use common\models\Atividade;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AtividadeSearch represents the model behind the search form of `common\models\Atividade`.
 */
class AtividadeSearch extends Atividade
{
    const BUSCA_MES = 1;
    const BUSCA_ANO = 2;

    /** @var integer */
    public $tipoBusca;

    /** @var string */
    public $mes;

    /** @var string */
    public $ano;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['dataHora', 'descricao', 'mes', 'ano', 'tipoBusca'], 'safe'],
            [
                ['mes'],
                'required',
                'when' => function ($model) {
                    return $model->tipoBusca === self::BUSCA_MES;
                },
                'whenClient' => "function (attribute, value) {
                    return $('input:checked[name=\"AtividadeSearch[tipoBusca]\"][type=\"radio\"]').val() == " . self::BUSCA_MES . ';
                }',
                'message' => 'Informe o mês'
            ],
            [
                ['ano'],
                'required',
                'when' => function ($model) {
                    return $model->tipoBusca === self::BUSCA_ANO;
                },
                'whenClient' => "function (attribute, value) {
                    return $('input:checked[name=\"AtividadeSearch[tipoBusca]\"][type=\"radio\"]').val() == " . self::BUSCA_ANO . ';
                }',
                'message' => 'Informe o ano'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Atividade::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dataHora' => $this->dataHora,
        ]);

        $query->andFilterWhere(['like', 'descricao', $this->descricao]);

        if ((int)$this->tipoBusca === self::BUSCA_MES) {
            $query
                ->andFilterWhere([
                    'AND',
                    ['MONTH(dataHora)' => $this->getMes()],
                    ['YEAR(dataHora)' => $this->getAno()],
                ]);
        } elseif ((int)$this->tipoBusca === self::BUSCA_ANO) {
            $query->andFilterWhere(['YEAR(dataHora)' => $this->getAno()]);
        }

        return $dataProvider;
    }

    /**
     * @return false|int|string
     */
    public function getMes()
    {
        return $this->mes ? date('m', strtotime($this->mes)) : null;
    }

    /**
     * @return false|int|string
     */
    public function getAno()
    {
        $ano = null;

        if ((int)$this->tipoBusca === self::BUSCA_MES) {
            $ano = date('Y', strtotime($this->mes));
        } elseif ((int)$this->tipoBusca === self::BUSCA_ANO) {
            $ano = $this->ano;
        }

        return $ano;
    }
}
