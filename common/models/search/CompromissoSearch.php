<?php

namespace common\models\search;

use common\helpers\DateTimeHelper;
use common\models\Compromisso;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CompromissoSearch represents the model behind the search form of `common\models\Compromisso`.
 */
class CompromissoSearch extends Compromisso
{
    const SCENARIO_RELATORIO = 'scenarioRelatorio';

    /** @var string */
    public $mes;

    /** @var string */
    public $ano;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'usuario_id'], 'integer'],
            [['descricao', 'localizacao', 'data', 'mes', 'ano'], 'safe'],
            [['mes'], 'required', 'on' => self::SCENARIO_RELATORIO],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Compromisso::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ($params && !$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'usuario_id' => Yii::$app->user->identity->getId(),
        ]);

        $query->andFilterWhere(['like', 'descricao', $this->descricao])
            ->andFilterWhere(['like', 'localizacao', $this->localizacao]);

        $condition = DateTimeHelper::buscarIntervalo('data', $this->data);
        $query->andFilterWhere($condition);
        if ($this->mes) {
            $query
                ->andFilterWhere([
                    'AND',
                    ['MONTH(data)' => $this->getMes()],
                    ['YEAR(data)' => $this->getAno()],
                ]);
        }

        return $dataProvider;
    }

    /**
     * @return false|int|string
     */
    public function getMes()
    {
        return $this->mes ? date('m', strtotime($this->mes)) : null;
    }

    /**
     * @return false|int|string
     */
    public function getAno()
    {
        $ano = null;

        if ($this->mes) {
            $ano = date('Y', strtotime($this->mes));
        } elseif ($this->ano) {
            $ano = $this->ano;
        }

        return $ano;
    }
}
