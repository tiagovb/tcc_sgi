<?php

namespace common\models\search;

use common\models\Despesa;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DespesaSearch represents the model behind the search form of `common\models\Despesa`.
 *
 * @property string $ano
 * @property string $mes
 */
class DespesaSearch extends Despesa
{
    const SCENARIO_RELATORIO = 'scenarioRelatorio';

    /** @var string */
    public $competencia;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'igreja_id', 'tipo'], 'integer'],
            [['dtReferencia', 'descricao', 'notaFiscal', 'competencia'], 'safe'],
            [['valor', 'quantidade'], 'number'],

            [['competencia'], 'required', 'on' => self::SCENARIO_RELATORIO, 'message' => 'O Mês deve ser informado'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Despesa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'igreja_id' => $this->igreja_id,
            'dtReferencia' => $this->dtReferencia,
            'valor' => $this->valor,
            'quantidade' => $this->quantidade,
            'tipo' => $this->tipo,
        ]);

        $query->andFilterWhere(['like', 'descricao', $this->descricao])
            ->andFilterWhere(['like', 'notaFiscal', $this->notaFiscal]);

        if ($this->competencia) {
            $query->andFilterWhere([
                'AND',
                ['MONTH(dtReferencia)' => $this->mes],
                ['YEAR(dtReferencia)' => $this->ano],
            ]);
        }

        return $dataProvider;
    }

    /**
     * @return false|int|string
     */
    public function getMes()
    {
        return $this->competencia ? date('m', strtotime($this->competencia)) : $this->mesCompetencia;
    }

    /**
     * @return false|int|string
     */
    public function getAno()
    {
        return $this->competencia ? date('Y', strtotime($this->competencia)) : $this->anoCompetencia;
    }
}
