<?php

namespace common\models\search;

use common\helpers\DateTimeHelper;
use common\models\EscalaTrabalho;
use DateInterval;
use DatePeriod;
use DateTime;
use frontend\models\EscalaTrabalhoForm;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * EscalaTrabalhoSearch represents the model behind the search form of `common\models\EscalaTrabalhoForm`.
 */
class EscalaTrabalhoSearch extends EscalaTrabalhoForm
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ArrayDataProvider
     */
    public function search($params)
    {
        $this->data = ArrayHelper::getValue($params, 'EscalaTrabalhoSearch.data');

        $arrayDataProvider = new ArrayDataProvider([
            'allModels' => [],
        ]);

//        if (!$this->data) {
//            return $arrayDataProvider;
//        }

        list($dtInicio, $dtFim) = DateTimeHelper::definirIntervalo($this->data);

        if (!$this->data) {
            $dataMin = EscalaTrabalho::find()->select('MIN(data)')->scalar();
            $dataMax = EscalaTrabalho::find()->select('MAX(data)')->scalar();

            if ($dataMin && $dataMax) {
                $dtInicio = date('Y-m-d', strtotime($dataMin . '-1 day'));
                $dtFim = date('Y-m-d', strtotime($dataMax . '+1 day'));
            }
        }

        $period = new DatePeriod(
            new DateTime($dtInicio),
            new DateInterval('P1D'),
            new DateTime($dtFim)
        );

        foreach ($period as $date) {
            $escala = self::findOne($date->format('Y-m-d'));

            if ($escala) {
                $arrayDataProvider->allModels[] = $escala;
            }
        }

        return $arrayDataProvider;
    }
}
