<?php

namespace common\models\search;

use common\models\FamiliaBeneficiada;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * FamiliaBeneficiadaSearch represents the model behind the search form of `common\models\FamiliaBeneficiada`.
 */
class FamiliaBeneficiadaSearch extends FamiliaBeneficiada
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'conjuge_id', 'igreja_id', 'sexo', 'nivelEscolar', 'estadoCivil', 'necessidadeAuxilio', 'status'], 'integer'],
            [['nome', 'nomePai', 'nomeMae', 'logradouro', 'numero', 'complemento', 'cidade', 'estado', 'cep', 'telefone', 'celular', 'email', 'dtNascimento', 'naturalidade', 'rg', 'cpf', 'dtCasamento', 'necessidadeAuxilio', 'observacoesAuxilio', 'dtCadastro', 'dtStatus'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FamiliaBeneficiada::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sexo' => $this->sexo,
            'dtNascimento' => $this->dtNascimento,
            'estadoCivil' => $this->estadoCivil,
            'dtCasamento' => $this->dtCasamento,
            'necessidadeAuxilio' => $this->necessidadeAuxilio,
            'observacoesAuxilio' => $this->observacoesAuxilio,
            'dtCadastro' => $this->dtCadastro,
            'status' => $this->status,
            'dtStatus' => $this->dtStatus,
        ]);

        $query
            ->andFilterWhere(['like', 'nome', $this->nome])
            ->andFilterWhere(['like', 'nomePai', $this->nomePai])
            ->andFilterWhere(['like', 'nomeMae', $this->nomeMae])
            ->andFilterWhere(['like', 'logradouro', $this->logradouro])
            ->andFilterWhere(['like', 'numero', $this->numero])
            ->andFilterWhere(['like', 'complemento', $this->complemento])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'naturalidade', $this->naturalidade])
            ->andFilterWhere(['like', 'rg', $this->rg])
            ->andFilterWhere(['like', 'cpf', $this->cpf]);

        return $dataProvider;
    }
}
