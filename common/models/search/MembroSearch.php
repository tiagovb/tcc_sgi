<?php

namespace common\models\search;

use common\models\Membro;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MembroSearch represents the model behind the search form of `common\models\Membro`.
 */
class MembroSearch extends Membro
{
    /** @var int */
    public $status = self::STATUS_MEMBRO;

    /** @var string */
    public $mesNascimento;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'conjuge_id', 'igreja_id', 'sexo', 'nivelEscolar', 'estadoCivil', 'necessidadeAuxilio', 'status'], 'integer'],
            [
                [
                    'nome',
                    'nomePai',
                    'nomeMae',
                    'logradouro',
                    'numero',
                    'complemento',
                    'cidade',
                    'estado',
                    'cep',
                    'telefone',
                    'celular',
                    'email',
                    'dtNascimento',
                    'naturalidade',
                    'rg',
                    'cpf',
                    'dtCasamento',
                    'observacoesAuxilio',
                    'dtCadastro',
                    'dtStatus',
                    'mesNascimento',
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Membro::find()
            ->andWhere([
                'NOT IN',
                ['id'],
                self::find()->select('id')->where(['and', ['status' => self::STATUS_INATIVO], ['IS NOT', 'necessidadeAuxilio', null]])
            ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'conjuge_id' => $this->conjuge_id,
            'igreja_id' => $this->igreja_id,
            'sexo' => $this->sexo,
            'nivelEscolar' => $this->nivelEscolar,
            'dtNascimento' => $this->dtNascimento,
            'estadoCivil' => $this->estadoCivil,
            'dtCasamento' => $this->dtCasamento,
            'necessidadeAuxilio' => $this->necessidadeAuxilio,
            'dtCadastro' => $this->dtCadastro,
            'status' => $this->status,
            'dtStatus' => $this->dtStatus,
        ]);
        $query->andFilterWhere(['like', 'nome', $this->nome])
            ->andFilterWhere(['like', 'nomePai', $this->nomePai])
            ->andFilterWhere(['like', 'nomeMae', $this->nomeMae])
            ->andFilterWhere(['like', 'logradouro', $this->logradouro])
            ->andFilterWhere(['like', 'numero', $this->numero])
            ->andFilterWhere(['like', 'complemento', $this->complemento])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'naturalidade', $this->naturalidade])
            ->andFilterWhere(['like', 'rg', $this->rg])
            ->andFilterWhere(['like', 'cpf', $this->cpf]);

        if ($this->mesNascimento) {
            $query->andFilterWhere([
                'AND',
                ['MONTH(dtNascimento)' => $this->mesNascimento],
            ]);
        }

        return $dataProvider;
    }
}
