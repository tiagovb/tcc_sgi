<?php

namespace common\models\search;

use common\helpers\DateTimeHelper;
use common\models\Patrimonio;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PatrimonioSearch represents the model behind the search form of `\common\models\Patrimonio`.
 */
class PatrimonioSearch extends Patrimonio
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'igreja_id', 'tipoAquisicao', 'status'], 'integer'],
            [['descricao', 'dtAquisicao', 'observacoes', 'notaFiscal'], 'safe'],
            [['valor'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Patrimonio::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'igreja_id' => $this->igreja_id,
            'valor' => $this->valor,
            'tipoAquisicao' => $this->tipoAquisicao,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'descricao', $this->descricao])
            ->andFilterWhere(['like', 'observacoes', $this->observacoes])
            ->andFilterWhere(['like', 'notaFiscal', $this->notaFiscal]);

        $query->andFilterWhere(DateTimeHelper::buscarIntervalo('dtAquisicao', $this->dtAquisicao));

        return $dataProvider;
    }
}
