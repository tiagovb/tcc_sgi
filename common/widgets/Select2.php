<?php

namespace common\widgets;

use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

/**
 * Class Select2
 */
class Select2 extends \kartik\select2\Select2
{
    /* @var array */
    public $options = ['placeholder' => 'Selecione...'];

    /* @var string */
    public $ajaxData;

    /* @var string */
    public $ajaxError;

    /* @var string */
    public $ajaxProcessResults;

    /* @var string */
    public $templateResult;

    /* @var string */
    public $templateSelection;

    /* @var string */
    public $objeto;

    /* @var string */
    public $url;

    /* @var bool */
    public $allowClear;

    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function init()
    {
        parent::init();

        // Definir parâmetros obrigatórios

        if (!$this->objeto) {
            throw new InvalidParamException('Objeto para busca não informado');
        }

        if (!$this->url) {
            $this->url = Url::to(['/dados/paginados', 'o' => $this->objeto]);
        }

        if (!$this->ajaxError) {
            $this->ajaxError = new JsExpression("function (error) { if (error.responseText) { var msg = error.responseText || 'Um erro ocorreu'; alert(msg); }}");
        }

        if (!$this->ajaxProcessResults) {
            $this->ajaxProcessResults = new JsExpression('function (data, params) { params.page = params.page || 1; return data; }');
        }

        if (!$this->ajaxData) {
            $this->ajaxData = new JsExpression('function(params) { return {q:params.term, page: params.page}; }');
        }

        // Incluir parâmetros definidos na classe

        if ($this->templateResult) {
            $this->pluginOptions['templateResult'] = $this->templateResult;
        }

        if ($this->templateSelection) {
            $this->pluginOptions['templateSelection'] = $this->templateSelection;
        }

        if ($this->allowClear === null && $this->hasModel() && $this->model->isAttributeRequired($this->attribute)) {
            $this->allowClear = false;
        }

        $this->pluginOptions = ArrayHelper::merge(
            [
                'allowClear' => $this->allowClear === null ? true : $this->allowClear,
                'minimumInputLength' => 0,
                'ajax' => [
                    'url' => $this->url,
                    'dataType' => 'json',
                    'data' => $this->ajaxData,
                    'delay' => 200,
                    'processResults' => $this->ajaxProcessResults,
                    'cache' => true,
                    'error' => $this->ajaxError,
                ],
            ],
            $this->pluginOptions
        );
    }
}
