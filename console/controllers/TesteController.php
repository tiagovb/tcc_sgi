<?php

namespace console\controllers;

use common\models\search\EscalaTrabalhoSearch;
use yii\console\Controller;

/**
 * Class TesteController
 */
class TesteController extends Controller
{
    public function actionIndex()
    {
        $model = new EscalaTrabalhoSearch();

        $array = $model->search([
            'EscalaTrabalhoSearch' => ['data' => '12/10/2017 - 12/10/2017']
        ]);
    }
}
