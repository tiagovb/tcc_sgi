<?php

namespace console\controllers;

use common\models\Igreja;
use common\models\Usuario;
use Yii;
use yii\console\Controller;
use yii\db\Expression;

/**
 * Class UtilController
 */
class UtilController extends Controller
{
    public function actionInit()
    {
        $scriptBanco = file_get_contents(Yii::$app->basePath . '/../' . 'banco.sql');
        Yii::$app->db->createCommand($scriptBanco)->execute();

        $igreja = new Igreja([
            'id' => 1,
            'razaoSocial' => 'ICB Manoel Honório',
            'logradouro' => 'NA',
            'numero' => 'NA',
            'complemento' => 'NA',
            'bairro' => 'NA',
            'cep' => 00000,
            'cidade' => 'NA',
            'uf' => 'NA',
        ]);

        $igreja->save(false);

        $user = new Usuario();
        $user->igreja_id = 1;
        $user->login = 'admin';
        $user->setPassword('admin');
        $user->tipoUsuario = Usuario::TIPO_GERENTE;

        $user->save(false);
    }

    public function actionTeste()
    {
        $data = \common\models\EscalaTrabalho::find()->select('MAX(data)')->scalar();
        echo $data;
    }

    public function actionCarga()
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            //Carga: Função
            Yii::$app->db->createCommand()->batchInsert('Funcao', ['nome'], [
                ['Manutenção da igreja'],
                ['Intercessão'],
                ['Ministrador de Dízimos e Ofertas'],
                ['Ministrador do Culto'],
                ['Unção com Óleo (Dízimos e Ofertas)'],
                ['Operador Multimídia'],
                ['Portaria 1'],
                ['Recepção 1'],
                ['Portaria 2'],
                ['Recepção 2'],
                ['Água para o Ministrador'],
                ['Limpeza Banheiro Masc.'],
                ['Limpeza Banheiro Fem.'],
                ['Orador Inicial'],
                ['Ministrador do Louvor'],
                ['Ministrador do Dança'],
                ['ICB Kids Crianças menores'],
                ['Adolescentes'],
            ])->execute();

            $ano = 2017;
            //Carga: Atividade
            $security = Yii::$app->getSecurity();
            for ($i = 1; $i <= 12; $i++) {
                Yii::$app->db->createCommand()->batchInsert('Atividade', ['igreja_id', 'dataHora', 'descricao'], [
                    [1, date('Y-m-d H:i:s', mktime(random_int(8, 18), random_int(1, 59), 0, $i, random_int(1, 30), $ano)), 'Atividade ' . $security->generateRandomString(5)],
                    [1, date('Y-m-d H:i:s', mktime(random_int(8, 18), random_int(1, 59), 0, $i, random_int(1, 30), $ano)), 'Atividade ' . $security->generateRandomString(5)],
                    [1, date('Y-m-d H:i:s', mktime(random_int(8, 18), random_int(1, 59), 0, $i, random_int(1, 30), $ano)), 'Atividade ' . $security->generateRandomString(5)],
                    [1, date('Y-m-d H:i:s', mktime(random_int(8, 18), random_int(1, 59), 0, $i, random_int(1, 30), $ano)), 'Atividade ' . $security->generateRandomString(5)],
                    [1, date('Y-m-d H:i:s', mktime(random_int(8, 18), random_int(1, 59), 0, $i, random_int(1, 30), $ano)), 'Atividade ' . $security->generateRandomString(5)],
                    [1, date('Y-m-d H:i:s', mktime(random_int(8, 18), random_int(1, 59), 0, $i, random_int(1, 30), $ano)), 'Atividade ' . $security->generateRandomString(5)],
                ])->execute();
            }

            //Carga: Compromisso
            for ($i = 1; $i <= 12; $i++) {
                Yii::$app->db->createCommand()->batchInsert('Compromisso', ['usuario_id', 'data', 'descricao'], [
                    [1, date('Y-m-d', mktime(random_int(8, 18), random_int(1, 59), 0, $i, random_int(1, 30), $ano)), 'Compromisso ' . $security->generateRandomString(6)],
                    [1, date('Y-m-d', mktime(random_int(8, 18), random_int(1, 59), 0, $i, random_int(1, 30), $ano)), 'Compromisso ' . $security->generateRandomString(6)],
                    [1, date('Y-m-d', mktime(random_int(8, 18), random_int(1, 59), 0, $i, random_int(1, 30), $ano)), 'Compromisso ' . $security->generateRandomString(6)],
                    [1, date('Y-m-d', mktime(random_int(8, 18), random_int(1, 59), 0, $i, random_int(1, 30), $ano)), 'Compromisso ' . $security->generateRandomString(6)],
                    [1, date('Y-m-d', mktime(random_int(8, 18), random_int(1, 59), 0, $i, random_int(1, 30), $ano)), 'Compromisso ' . $security->generateRandomString(6)],
                ])->execute();
            }

            //Carga: Despesa
            Yii::$app->db->createCommand()->batchInsert('Despesa', ['igreja_id', 'dtReferencia', 'descricao', 'quantidade', 'tipo', 'valor'], [
                [1, date('Y-m-d', strtotime('-15 day')), 'Despesa -15 dias', '15', 1, 115],
                [1, date('Y-m-d', strtotime('-14 day')), 'Despesa -14 dias', '14', 2, 214],
                [1, date('Y-m-d', strtotime('-13 day')), 'Despesa -13 dias', '13', 3, 313],
                [1, date('Y-m-d', strtotime('-12 day')), 'Despesa -12 dias', '12', 4, 412],
                [1, date('Y-m-d', strtotime('-11 day')), 'Despesa -11 dias', '11', 1, 111],
                [1, date('Y-m-d', strtotime('-10 day')), 'Despesa -10 dias', '10', 2, 210],
                [1, date('Y-m-d', strtotime('-9 day')), 'Despesa -9 dias', '9', 3, 39],
                [1, date('Y-m-d', strtotime('-8 day')), 'Despesa -8 dias', '8', 4, 48],
                [1, date('Y-m-d', strtotime('-7 day')), 'Despesa -7 dias', '7', 1, 17],
                [1, date('Y-m-d', strtotime('-6 day')), 'Despesa -6 dias', '6', 2, 26],
                [1, date('Y-m-d', strtotime('-5 day')), 'Despesa -5 dias', '5', 3, 35],
                [1, date('Y-m-d', strtotime('-4 day')), 'Despesa -4 dias', '4', 4, 44],
                [1, date('Y-m-d', strtotime('-3 day')), 'Despesa -3 dias', '3', 1, 13],
                [1, date('Y-m-d', strtotime('-2 day')), 'Despesa -2 dias', '2', 2, 22],
                [1, date('Y-m-d', strtotime('-1 day')), 'Despesa -1 dias', '1', 3, 31],
                [1, date('Y-m-d', strtotime('-0 day')), 'Despesa -0 dias', '0', 4, 40],
                [1, date('Y-m-d', strtotime('+1 day')), 'Despesa +1 dias', '1', 1, 11],
                [1, date('Y-m-d', strtotime('+2 day')), 'Despesa +2 dias', '2', 2, 22],
                [1, date('Y-m-d', strtotime('+3 day')), 'Despesa +3 dias', '3', 3, 33],
                [1, date('Y-m-d', strtotime('+4 day')), 'Despesa +4 dias', '4', 4, 44],
                [1, date('Y-m-d', strtotime('+5 day')), 'Despesa +5 dias', '5', 1, 15],
                [1, date('Y-m-d', strtotime('+6 day')), 'Despesa +6 dias', '6', 2, 26],
                [1, date('Y-m-d', strtotime('+7 day')), 'Despesa +7 dias', '7', 3, 37],
                [1, date('Y-m-d', strtotime('+8 day')), 'Despesa +8 dias', '8', 4, 48],
                [1, date('Y-m-d', strtotime('+9 day')), 'Despesa +9 dias', '9', 1, 19],
                [1, date('Y-m-d', strtotime('+10 day')), 'Despesa +10 dias', '10', 2, 210],
                [1, date('Y-m-d', strtotime('+11 day')), 'Despesa +11 dias', '11', 3, 311],
                [1, date('Y-m-d', strtotime('+12 day')), 'Despesa +12 dias', '12', 4, 412],
                [1, date('Y-m-d', strtotime('+13 day')), 'Despesa +13 dias', '13', 1, 113],
                [1, date('Y-m-d', strtotime('+14 day')), 'Despesa +14 dias', '14', 2, 214],
            ])->execute();

            //Carga: Patrimonio
            Yii::$app->db->createCommand()->batchInsert('Patrimonio', ['igreja_id', 'dtAquisicao', 'descricao', 'valor', 'tipoAquisicao', 'status'], [
                [1, date('Y-m-d', strtotime('-15 day')), 'Patrimonio a1', 115, 2, 1],
                [1, date('Y-m-d', strtotime('-14 day')), 'Patrimonio b2', 214, 2, 1],
                [1, date('Y-m-d', strtotime('-13 day')), 'Patrimonio c3', 313, 2, 1],
                [1, date('Y-m-d', strtotime('-12 day')), 'Patrimonio d4', 412, 2, 1],
                [1, date('Y-m-d', strtotime('-11 day')), 'Patrimonio e5', 111, 2, 1],
                [1, date('Y-m-d', strtotime('-10 day')), 'Patrimonio f6', 210, 2, 1],
                [1, date('Y-m-d', strtotime('-9 day')), 'Patrimonio g7', 39, 2, 1],
                [1, date('Y-m-d', strtotime('-8 day')), 'Patrimonio a8', 48, 2, 1],
                [1, date('Y-m-d', strtotime('-7 day')), 'Patrimonio b9', 17, 2, 1],
                [1, date('Y-m-d', strtotime('-6 day')), 'Patrimonio c10', 26, 2, 1],
                [1, date('Y-m-d', strtotime('-5 day')), 'Patrimonio d11', 35, 2, 1],
                [1, date('Y-m-d', strtotime('-4 day')), 'Patrimonio e12', 44, 2, 1],
                [1, date('Y-m-d', strtotime('-3 day')), 'Patrimonio f13', 13, 2, 1],
                [1, date('Y-m-d', strtotime('-2 day')), 'Patrimonio g14', 22, 2, 1],
                [1, date('Y-m-d', strtotime('-1 day')), 'Patrimonio a15', 31, 2, 1],
                [1, date('Y-m-d', strtotime('-0 day')), 'Patrimonio b16', 40, 2, 1],
                [1, date('Y-m-d', strtotime('+1 day')), 'Patrimonio c17', 11, 2, 1],
                [1, date('Y-m-d', strtotime('+2 day')), 'Patrimonio d18', 22, 2, 1],
                [1, date('Y-m-d', strtotime('+3 day')), 'Patrimonio e19', 33, 2, 1],
                [1, date('Y-m-d', strtotime('+4 day')), 'Patrimonio f20', 44, 2, 1],
                [1, date('Y-m-d', strtotime('+5 day')), 'Patrimonio g21', 15, 2, 1],
                [1, date('Y-m-d', strtotime('+6 day')), 'Patrimonio a22', 26, 2, 1],
                [1, date('Y-m-d', strtotime('+7 day')), 'Patrimonio b23', 37, 2, 1],
                [1, date('Y-m-d', strtotime('+8 day')), 'Patrimonio c24', 48, 2, 1],
                [1, date('Y-m-d', strtotime('+9 day')), 'Patrimonio d25', 19, 2, 1],
                [1, date('Y-m-d', strtotime('+10 day')), 'Patrimonio e26', 210, 2, 1],
                [1, date('Y-m-d', strtotime('+11 day')), 'Patrimonio f27', 311, 2, 1],
                [1, date('Y-m-d', strtotime('+12 day')), 'Patrimonio g28', 412, 2, 1],
                [1, date('Y-m-d', strtotime('+13 day')), 'Patrimonio a29', 113, 2, 1],
                [1, date('Y-m-d', strtotime('+14 day')), 'Patrimonio b30', 214, 2, 1],
            ])->execute();

            //Carga: Membro
            $nowExpression = new Expression('NOW()');
            Yii::$app->db->createCommand()->batchInsert(
                'Membro',
                [
                    'igreja_id',
                    'nome',
                    'sexo',
                    'logradouro',
                    'numero',
                    'cidade',
                    'estado',
                    'nivelEscolar',
                    'dtNascimento',
                    'naturalidade',
                    'estadoCivil',
                    'status',
                    'cpf',
                    'dtCadastro',
                    'dtStatus',
                ],
                [
                    [
                        1,
                        'Romildo',
                        1,
                        'Rua a1',
                        11,
                        'Juiz de Fora',
                        'MG',
                        1,
                        date('Y-m-d', strtotime('-18 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '24668173061',
                        date('Y-m-d', strtotime('-1 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Paulo',
                        1,
                        'Rua b2',
                        12,
                        'Juiz de Fora',
                        'MG',
                        1,
                        date('Y-m-d', strtotime('-19 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '82329904495',
                        date('Y-m-d', strtotime('-2 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Lívia',
                        2,
                        'Rua c3',
                        23,
                        'Juiz de Fora',
                        'MG',
                        2,
                        date('Y-m-d', strtotime('-20 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '02283215650',
                        date('Y-m-d', strtotime('-3 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Alexsandro',
                        1,
                        'Rua d4',
                        14,
                        'Juiz de Fora',
                        'MG',
                        1,
                        date('Y-m-d', strtotime('-21 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '37919300363',
                        date('Y-m-d', strtotime('-4 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Tatiane',
                        2,
                        'Rua e5',
                        25,
                        'Juiz de Fora',
                        'MG',
                        2,
                        date('Y-m-d', strtotime('-22 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '55835234333',
                        date('Y-m-d', strtotime('-5 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Jorge',
                        1,
                        'Rua f6',
                        16,
                        'Juiz de Fora',
                        'MG',
                        1,
                        date('Y-m-d', strtotime('-23 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '35693299848',
                        date('Y-m-d', strtotime('-6 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Felipe',
                        1,
                        'Rua g7',
                        17,
                        'Juiz de Fora',
                        'MG',
                        1,
                        date('Y-m-d', strtotime('-24 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '68297173615',
                        date('Y-m-d', strtotime('-7 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'André',
                        1,
                        'Rua a8',
                        18,
                        'Juiz de Fora',
                        'MG',
                        1,
                        date('Y-m-d', strtotime('-25 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '27598985715',
                        date('Y-m-d', strtotime('-8 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Rodrigo',
                        1,
                        'Rua b9',
                        19,
                        'Juiz de Fora',
                        'MG',
                        1,
                        date('Y-m-d', strtotime('-26 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '60386007870',
                        date('Y-m-d', strtotime('-9 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Leila',
                        2,
                        'Rua c10',
                        210,
                        'Juiz de Fora',
                        'MG',
                        2,
                        date('Y-m-d', strtotime('-27 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '73649788527',
                        date('Y-m-d', strtotime('-10 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'João',
                        1,
                        'Rua d11',
                        111,
                        'Juiz de Fora',
                        'MG',
                        1,
                        date('Y-m-d', strtotime('-28 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '87119658514',
                        date('Y-m-d', strtotime('-11 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Reinaldo',
                        1,
                        'Rua e12',
                        112,
                        'Juiz de Fora',
                        'MG',
                        1,
                        date('Y-m-d', strtotime('-29 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '07762630139',
                        date('Y-m-d', strtotime('-12 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Santina',
                        2,
                        'Rua f13',
                        213,
                        'Juiz de Fora',
                        'MG',
                        2,
                        date('Y-m-d', strtotime('-30 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '11471268446',
                        date('Y-m-d', strtotime('-13 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Marcela',
                        2,
                        'Rua g14',
                        214,
                        'Juiz de Fora',
                        'MG',
                        2,
                        date('Y-m-d', strtotime('-14 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '14678242830',
                        date('Y-m-d', strtotime('-14 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Daniela',
                        2,
                        'Rua a15',
                        215,
                        'Juiz de Fora',
                        'MG',
                        2,
                        date('Y-m-d', strtotime('-15 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '67085040149',
                        date('Y-m-d', strtotime('-15 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Denise',
                        2,
                        'Rua b16',
                        216,
                        'Juiz de Fora',
                        'MG',
                        2,
                        date('Y-m-d', strtotime('-16 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '68453347816',
                        date('Y-m-d', strtotime('-16 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Priscilla',
                        2,
                        'Rua c17',
                        217,
                        'Juiz de Fora',
                        'MG',
                        2,
                        date('Y-m-d', strtotime('-17 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '93745875303',
                        date('Y-m-d', strtotime('-17 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Arnaldo',
                        1,
                        'Rua d18',
                        118,
                        'Juiz de Fora',
                        'MG',
                        1,
                        date('Y-m-d', strtotime('-18 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '11945182148',
                        date('Y-m-d', strtotime('-18 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Viviane',
                        2,
                        'Rua e19',
                        219,
                        'Juiz de Fora',
                        'MG',
                        2,
                        date('Y-m-d', strtotime('-19 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '44232644393',
                        date('Y-m-d', strtotime('-19 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Marcos',
                        1,
                        'Rua f20',
                        120,
                        'Juiz de Fora',
                        'MG',
                        1,
                        date('Y-m-d', strtotime('-20 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '57631086125',
                        date('Y-m-d', strtotime('-20 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Aline',
                        2,
                        'Rua g21',
                        221,
                        'Juiz de Fora',
                        'MG',
                        2,
                        date('Y-m-d', strtotime('-21 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '56331378685',
                        date('Y-m-d', strtotime('-21 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Silvio',
                        1,
                        'Rua a22',
                        122,
                        'Juiz de Fora',
                        'MG',
                        1,
                        date('Y-m-d', strtotime('-22 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '51367464323',
                        date('Y-m-d', strtotime('-22 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Regina',
                        2,
                        'Rua b23',
                        223,
                        'Juiz de Fora',
                        'MG',
                        2,
                        date('Y-m-d', strtotime('-23 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '38277197802',
                        date('Y-m-d', strtotime('-23 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Silvana',
                        2,
                        'Rua c24',
                        224,
                        'Juiz de Fora',
                        'MG',
                        2,
                        date('Y-m-d', strtotime('-24 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '87961653597',
                        date('Y-m-d', strtotime('-24 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Vicente',
                        1,
                        'Rua d25',
                        125,
                        'Juiz de Fora',
                        'MG',
                        1,
                        date('Y-m-d', strtotime('-25 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '74468754370',
                        date('Y-m-d', strtotime('-25 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Tayna',
                        2,
                        'Rua e26',
                        226,
                        'Juiz de Fora',
                        'MG',
                        2,
                        date('Y-m-d', strtotime('-26 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '71878761307',
                        date('Y-m-d', strtotime('-26 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Jose',
                        1,
                        'Rua f27',
                        127,
                        'Juiz de Fora',
                        'MG',
                        1,
                        date('Y-m-d', strtotime('-27 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '93451875705',
                        date('Y-m-d', strtotime('-27 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Lucas',
                        1,
                        'Rua g28',
                        128,
                        'Juiz de Fora',
                        'MG',
                        1,
                        date('Y-m-d', strtotime('-28 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '41143210204',
                        date('Y-m-d', strtotime('-28 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Waldinei',
                        1,
                        'Rua a29',
                        129,
                        'Juiz de Fora',
                        'MG',
                        1,
                        date('Y-m-d', strtotime('-29 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '28153767844',
                        date('Y-m-d', strtotime('-29 day')),
                        $nowExpression
                    ],
                    [
                        1,
                        'Pedro',
                        1,
                        'Rua b30',
                        130,
                        'Juiz de Fora',
                        'MG',
                        1,
                        date('Y-m-d', strtotime('-30 year')),
                        'Juiz de Fora',
                        1,
                        1,
                        '32786131715',
                        date('Y-m-d', strtotime('-30 day')),
                        $nowExpression
                    ],
                ])->execute();
            $transaction->commit();

            //Carga: Dependentes
            Yii::$app->db->createCommand()->batchInsert(
                'Dependente',
                [
                    'membro_id',
                    'nome',
                    'dtNascimento',
                    'religiao',
                ],
                [
                    [new Expression('(SELECT MIN(id) from Membro) + 1 '), 'Fabio', date('Y-m-d', strtotime('-1 year')), 1],
                    [new Expression('(SELECT MIN(id) from Membro) + 1 '), 'Eufrasio', date('Y-m-d', strtotime('-2 year')), 2],
                    [new Expression('(SELECT MIN(id) from Membro) + 2 '), 'Myrthes', date('Y-m-d', strtotime('-3 year')), 3],
                    [new Expression('(SELECT MIN(id) from Membro) + 2 '), 'Moises', date('Y-m-d', strtotime('-4 year')), 4],
                    [new Expression('(SELECT MIN(id) from Membro) + 3 '), 'Marco', date('Y-m-d', strtotime('-5 year')), 5],
                    [new Expression('(SELECT MIN(id) from Membro) + 3 '), 'Jaqueline', date('Y-m-d', strtotime('-6 year')), 1],
                    [new Expression('(SELECT MIN(id) from Membro) + 4 '), 'Edson', date('Y-m-d', strtotime('-7 year')), 2],
                    [new Expression('(SELECT MIN(id) from Membro) + 4 '), 'Raphael', date('Y-m-d', strtotime('-8 year')), 3],
                ])->execute();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}
