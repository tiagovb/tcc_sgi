<?php

use common\components\Formatter;
use yii\db\Connection;
use yii\swiftmailer\Mailer;

return [
    'language' => 'pt-BR',
    'sourceLanguage' => 'pt-BR',
    'timeZone' => 'America/Sao_Paulo',

    'components' => [
        'db' => [
            'class' => Connection::class,
            'dsn' => 'mysql:host=localhost;dbname=sgi',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'formatter' => [
            'class' => Formatter::class,
            'defaultTimeZone' => 'America/Sao_Paulo',
            'timeZone' => 'America/Sao_Paulo',
        ],
        'mailer' => [
            'class' => Mailer::class,
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
];
