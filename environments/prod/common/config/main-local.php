<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=sgi',
            'username' => 'ubuntu',
            'password' => '',
            'charset' => 'utf8',
            'enableSchemaCache' => true,
            'schemaCache' => 'cacheDb'
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],
    ],
];
