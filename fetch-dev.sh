#!/usr/bin/env bash

function info {
  echo " "
  echo "--> $1"
  echo " "
}

info "Removendo app.conf dev"
rm -rf /etc/nginx/sites-enabled/app.conf

info "Copiando app.conf local"
ln -s /app/vagrant/nginx/app.conf /etc/nginx/sites-enabled/app.conf

info "Restart nginx"
service nginx restart