#!/usr/bin/env bash

function info {
  echo " "
  echo "--> $1"
  echo " "
}

info "Atualizando /www/app"
/www/app/atualizar.sh

info "Removendo app.conf dev"
rm -rf /etc/nginx/sites-enabled/app.conf

info "Copiando app.conf local"
ln -s /www/app/vagrant/nginx/app-local.conf /etc/nginx/sites-enabled/app.conf

info "Restart nginx"
service nginx restart