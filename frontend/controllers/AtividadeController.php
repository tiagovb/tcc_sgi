<?php

namespace frontend\controllers;

use common\components\Flash;
use common\models\Atividade;
use common\models\search\AtividadeSearch;
use kartik\mpdf\Pdf;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * AtividadeController implements the CRUD actions for Atividade model.
 */
class AtividadeController extends MainController
{
    /**
     * Lists all Atividade models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AtividadeSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Atividade model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Atividade model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Atividade the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Atividade::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new Atividade model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Atividade();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Flash::success('Atividade cadastrada com sucesso.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Atividade model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Flash::success('Atividade alterada com sucesso.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Atividade model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Flash::success('Atividade excluída com sucesso.');

        return $this->redirect(['index']);
    }

    /**
     * @return mixed
     */
    public function actionImprimirRelatorio()
    {
        $this->layout = false;
        $searchModel = new AtividadeSearch();
        $queryParams = Yii::$app->request->queryParams;
        $queryParams = reset($queryParams);

        $dataProvider = $searchModel->search($queryParams);

        $header = '';
        $content = '';
        $fontSize = '';
        if ($searchModel->tipoBusca == AtividadeSearch::BUSCA_ANO) {
            $header = ["Relatório de Atividades - {$searchModel->ano}||Gerado em: " . date('d/m/y H:i')];
            $content = $this->renderPartial('imprimir-ano', [
                'searchModel' => $searchModel,
            ]);
            $fontSize = '10';
        } elseif ($searchModel->tipoBusca == AtividadeSearch::BUSCA_MES) {
            $mes = ucwords(Yii::$app->formatter->asDate($searchModel->mes, 'php:F/Y'));
            $header = ["Relatório de Atividades - {$mes}||Gerado em: " . date('d/m/y H:i')];
            $content = $this->renderPartial('imprimir-mes', [
                'dataProvider' => $dataProvider,
                'mes' => $searchModel->mes,
            ]);
            $fontSize = '12';
        }

        $css = <<<CSS
.table-mes{font-size:{$fontSize}px;} 
.table-vazia{font-size:10px;}
.title-mes,tr,td{align:center;}
CSS;

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'marginBottom' => 1,
            'content' => $content,
            'cssInline' => $css,
            'methods' => [
                'SetHeader' => $header,
            ]
        ]);

        return $pdf->render();
    }

    /**
     * Lists all Atividade models.
     *
     * @return mixed
     */
    public function actionRelatorio()
    {
        $searchModel = new AtividadeSearch();
        $queryParams = Yii::$app->request->queryParams;

        $dataProvider = $searchModel->search($queryParams);

        return $this->render('relatorio', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'queryParams' => $queryParams,
        ]);
    }

}
