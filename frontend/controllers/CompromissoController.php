<?php

namespace frontend\controllers;

use common\components\Flash;
use common\models\Compromisso;
use common\models\search\CompromissoSearch;
use common\models\Usuario;
use kartik\mpdf\Pdf;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * CompromissoController implements the CRUD actions for Compromisso model.
 */
class CompromissoController extends MainController
{
    /**
     * Lists all Compromisso models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompromissoSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Compromisso model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Compromisso model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Compromisso the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Compromisso::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new Compromisso model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Compromisso();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Flash::success('Compromisso cadastrado com sucesso.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        $erros = $model->getErrors();

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Compromisso model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Flash::success('Compromisso alterado com sucesso.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Patrimonio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $refereer = Yii::$app->request->referrer;
        $this->findModel($id)->delete();

        Flash::success('Compromisso excluído com sucesso.');

        return $this->redirect(strpos($refereer, 'index') !== false ? $refereer : ['index']);
    }

    /**
     * @return mixed
     */
    public function actionImprimirRelatorio()
    {
        $this->layout = false;
        $searchModel = new CompromissoSearch();
        $queryParams = Yii::$app->request->queryParams;
        $queryParams = reset($queryParams);

        $dataProvider = $searchModel->search($queryParams);

        $mes = ucwords(Yii::$app->formatter->asDate($searchModel->mes, 'php:F/Y'));
        /** @var Usuario $usuario */
        $usuario = Yii::$app->user->identity;
        $header = ["Relatório de Compromissos - {$mes} - {$usuario->login}||Gerado em: " . date('d/m/y H:i')];
        $content = $this->renderPartial('imprimir-mes', [
            'dataProvider' => $dataProvider,
            'mes' => $searchModel->mes,
        ]);
        $fontSize = '12';

        $css = <<<CSS
.table-mes{font-size:{$fontSize}px;} 
.table-vazia{font-size:10px;}
.title-mes,tr,td{align:center;}
CSS;

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'marginBottom' => 1,
            'content' => $content,
            'cssInline' => $css,
            'methods' => [
                'SetHeader' => $header,
            ]
        ]);

        return $pdf->render();
    }

    /**
     * Lists all Atividade models.
     *
     * @return mixed
     */
    public function actionRelatorio()
    {
        $searchModel = new CompromissoSearch();
        $searchModel->setScenario(CompromissoSearch::SCENARIO_RELATORIO);
        $queryParams = Yii::$app->request->queryParams;

        $dataProvider = $searchModel->search($queryParams);

        return $this->render('relatorio', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'queryParams' => $queryParams,
        ]);
    }

}
