<?php

namespace frontend\controllers;

use common\components\Flash;
use common\models\Dependente;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * DependenteController implements the CRUD actions for Membro model.
 */
class DependenteController extends MainController
{

    /**
     * @param $membroId
     *
     * @return mixed
     */
    public function actionCreate($membroId)
    {
        $dependente = new Dependente();
        $dependente->setAttributes([
            'membro_id' => $membroId,
        ]);

        if (Yii::$app->request->isAjax) {
            $renderAjax = $this->renderAjax('_dependenteForm', ['dependente' => $dependente, 'action' => ['create', 'membroId' => $membroId]]);

            return $renderAjax;
        }

        if ($dependente->load(Yii::$app->request->post()) && $dependente->save()) {
            Flash::success('Dependente cadastrado com sucesso.');
        } else {
            Flash::error('Erro ao cadastrar Dependente');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $dependenteId
     *
     * @return string
     */
    public function actionUpdate($dependenteId)
    {
        $dependente = Dependente::findOne($dependenteId);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_dependenteForm', ['dependente' => $dependente, 'action' => ['update', 'dependenteId' => $dependente->id]]);
        }

        if ($dependente->load(Yii::$app->request->post()) && $dependente->save()) {
            Flash::success('Dependente alterado com sucesso.');
        } else {
            Flash::error('Erro ao salvar alterações.');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $dependenteId
     *
     * @throws NotFoundHttpException
     * @return mixed
     */
    public function actionDelete($dependenteId)
    {
        $dependente = $this->findModel($dependenteId);

        $dependente->delete();

        Flash::success('Dependente excluído com sucesso.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Membro model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dependente the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dependente::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
