<?php

namespace frontend\controllers;

use common\components\Flash;
use common\models\Despesa;
use common\models\search\DespesaSearch;
use kartik\mpdf\Pdf;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;

/**
 * DespesaController implements the CRUD actions for Despesa model.
 */
class DespesaController extends MainController
{

    /**
     * Lists all Despesa models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DespesaSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Despesa model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Despesa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Despesa();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Flash::success('Despesa cadastrada com sucesso.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Despesa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Flash::success('Despesa alterada com sucesso.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Patrimonio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $refereer = Yii::$app->request->referrer;
        $this->findModel($id)->delete();

        Flash::success('Despesa excluida com sucesso.');

        return $this->redirect(strpos($refereer, 'index') !== false ? $refereer : ['index']);
    }

    /**
     * Finds the Despesa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Despesa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Despesa::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @return mixed
     */
    public function actionImprimirRelatorio()
    {
        $searchModel = new DespesaSearch();
        $searchModel->competencia = date('Y-m');

        $queryParams = Yii::$app->request->queryParams;
        $queryParams = reset($queryParams);
        $activeDataProvider = $searchModel->search($queryParams);
        $activeDataProvider->setPagination(false);

        $models = $activeDataProvider->getModels();
        $dataProvider = new ArrayDataProvider([
            'allModels' => $models,
        ]);

        $totalDespesas = $activeDataProvider->query->select('SUM(valor)')->scalar();

        $content = $this->renderPartial('imprimir-relatorio', [
            'dataProvider' => $dataProvider,
            'totalDespesas' => $totalDespesas,
        ]);

        $competenciaTxt = ucfirst(Yii::$app->formatter->asDate(strtotime($searchModel->competencia), 'php:F-Y'));

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'content' => $content,
            'methods' => [
                'SetHeader' => ["Relatório de Despesas - {$competenciaTxt}||Gerado em: " . date('d/m/y H:i')],
                'SetFooter' => ['|Página {PAGENO}|'],
            ]
        ]);

        return $pdf->render();
    }

    /**
     * @return mixed
     */
    public function actionRelatorio()
    {
        $searchModel = new DespesaSearch(['competencia' => date('Y-m', strtotime('first day of this month'))]);
        $searchModel->setScenario(DespesaSearch::SCENARIO_RELATORIO);

        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('relatorio', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'queryParams' => $queryParams,
        ]);
    }

}
