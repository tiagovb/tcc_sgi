<?php

namespace frontend\controllers;

use common\helpers\ActiveForm;
use common\models\search\EscalaTrabalhoSearch;
use frontend\models\EscalaTrabalhoForm;
use kartik\mpdf\Pdf;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * EscalaTrabalhoController implements the CRUD actions for EscalaTrabalho model.
 */
class EscalaTrabalhoController extends MainController
{
    /**
     * Lists all EscalaTrabalhoForm models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EscalaTrabalhoSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);

        if (!empty($queryParams)) {
            $searchModel->validate('data');
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EscalaTrabalhoForm model.
     *
     * @param string $data
     *
     * @return mixed
     */
    public function actionView($data)
    {
        $escalaTrabalhoForm = $this->findModel($data);

        return $this->render('view', [
            'model' => $escalaTrabalhoForm,
        ]);
    }

    /**
     * Creates a new EscalaTrabalho model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EscalaTrabalhoForm(['data' => date('Y-m-d', strtotime('next sunday'))]);
        $model->setScenario(EscalaTrabalhoForm::SCENARIO_CREATE);

        $request = Yii::$app->request;
        $post = $request->post();

        if (!$post) {
            $model->preencherComAnterior();
        }

        if ($request->isAjax) {
            $model->load($post);
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->validate();

            return ActiveForm::validate($model);
        }

        if ($model->load($post) && $model->validate() && $model->save()) {
            return $this->redirect(['view', 'data' => $model->data]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing EscalaTrabalho model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param string $data
     *
     * @return mixed
     */
    public function actionUpdate($data)
    {
        $model = $this->findModel($data);
        $request = Yii::$app->request;
        $post = $request->post();

        if ($request->isAjax) {
            $model->load($post);
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->validate();

            return ActiveForm::validate($model);
        }

        if ($model->load($post) && $model->update()) {
            return $this->redirect(['view', 'data' => $data]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing EscalaTrabalho model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $data
     *
     * @return mixed
     */
    public function actionDelete($data)
    {
        $refereer = Yii::$app->request->referrer;

        $this->findModel($data)->delete();

        return $this->redirect(strpos($refereer, 'index') !== false ? $refereer : ['index']);
    }

    /**
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $data
     *
     * @return EscalaTrabalhoForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($data)
    {
        $escalaTrabalhoForm = EscalaTrabalhoForm::findOne($data);

        if ($escalaTrabalhoForm) {
            return $escalaTrabalhoForm;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $data
     *
     * @return mixed
     */
    public function actionImprimir($data)
    {
        $escalaTrabalhoForm = $this->findModel($data);

        return $this->render('imprimir-relatorio', [
            'model' => $escalaTrabalhoForm,
        ]);
    }


    /**
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionImprimirRelatorio($data)
    {
        $escalaTrabalhoForm = $this->findModel($data);

        $content = $this->renderPartial('imprimir-relatorio', [
            'model' => $escalaTrabalhoForm,
        ]);

        $css = <<<CSS
@media print
{    
    .no-print, .no-print *
    {
        display: none !important;
    }
}
hr{
margin-top: 1px;
margin-bottom: 1px;
}
.table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #979494;
}
.table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > th, .table > caption + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > td, .table > thead:first-child > tr:first-child > td {
    border: 1px solid #979494;
}
CSS;

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'cssInline' => $css,
            'content' => $content,
            'methods' => [
                'SetHeader' => ['Escala de Trabalho||Gerado em: ' . date('d/m/y H:i')],
                'SetFooter' => ['|Página {PAGENO}|'],
            ]
        ]);

        return $pdf->render();
    }
}
