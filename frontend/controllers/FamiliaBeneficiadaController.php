<?php

namespace frontend\controllers;

use common\components\Flash;
use common\models\FamiliaBeneficiada;
use common\models\Membro;
use common\models\search\FamiliaBeneficiadaSearch;
use kartik\mpdf\Pdf;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;

/**
 * FamiliaBeneficiadaController implements the CRUD actions for FamiliaBeneficiada model.
 */
class FamiliaBeneficiadaController extends MainController
{
    /**
     * Lists all FamiliaBeneficiada models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FamiliaBeneficiadaSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FamiliaBeneficiada model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $familia = $this->findModel($id);
        $depentendesProvider = new ArrayDataProvider(['allModels' => $familia->dependentes]);

        return $this->render('view', [
            'model' => $familia,
            'depentendesProvider' => $depentendesProvider,
        ]);
    }

    /**
     * Finds the FamiliaBeneficiada model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return FamiliaBeneficiada the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FamiliaBeneficiada::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new FamiliaBeneficiada model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FamiliaBeneficiada(['status' => Membro::STATUS_INATIVO]);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Flash::success('Família Beneficiada cadastrada com sucesso.');

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FamiliaBeneficiada model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Flash::success('Família Beneficiada alterada com sucesso.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FamiliaBeneficiada model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        Flash::success('Família Beneficiada excluída com sucesso.');

        return $this->redirect(['index']);
    }

    /**
     * @return mixed
     */
    public function actionRelatorio()
    {
        $searchModel = new FamiliaBeneficiadaSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('relatorio', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'queryParams' => $queryParams,
        ]);
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionImprimirRelatorio()
    {
        $searchModel = new FamiliaBeneficiadaSearch();
        $queryParams = Yii::$app->request->queryParams;
        $queryParams = reset($queryParams);
        $activeDataProvider = $searchModel->search($queryParams);
        $activeDataProvider->setPagination(false);

        $models = $activeDataProvider->getModels();
        $dataProvider = new ArrayDataProvider([
            'allModels' => $models,
        ]);

        $content = $this->renderPartial('imprimir-relatorio', [
            'dataProvider' => $dataProvider,
        ]);

        $pdf = new Pdf([
            'marginLeft' => 5,
            'marginRight' => 5,
            'marginBottom' => 5,
            'mode' => Pdf::MODE_UTF8,
            'content' => $content,
            'methods' => [
                'SetHeader' => ['Relatório de Famílias Beneficiadas||Gerado em: ' . date('d/m/y H:i')],
                'SetFooter' => ['|Página {PAGENO}|'],
            ]
        ]);

        return $pdf->render();
    }
}
