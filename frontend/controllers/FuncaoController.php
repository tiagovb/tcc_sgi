<?php

namespace frontend\controllers;

use common\components\Flash;
use common\models\Funcao;
use common\models\search\FuncaoSearch;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * FuncaoController implements the CRUD actions for Funcao model.
 */
class FuncaoController extends MainController
{
    /**
     * Lists all Funcao models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FuncaoSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Funcao model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Funcao model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Funcao the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Funcao::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new Funcao model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Funcao();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Flash::success('Função cadastrada com sucesso.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Funcao model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Flash::success('Função alterada com sucesso.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Funcao model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $funcao = $this->findModel($id);

        if (!$funcao->canDelete()) {
            Flash::error('Não é possível excluir está função.');

            return $this->render('view', $funcao);
        }

        $funcao->delete();

        Flash::success('Função excluída com sucesso.');

        return $this->redirect(['index']);
    }
}
