<?php

namespace frontend\controllers;

use common\components\Flash;
use common\models\Dependente;
use common\models\Membro;
use common\models\search\MembroSearch;
use kartik\mpdf\Pdf;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;

/**
 * MembroController implements the CRUD actions for Membro model.
 */
class MembroController extends MainController
{
    /**
     * Lists all Membro models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MembroSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Membro model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $membro = $this->findModel($id);
        $depentendesProvider = new ArrayDataProvider(['allModels' => $membro->dependentes]);

        return $this->render('view', [
            'model' => $membro,
            'depentendesProvider' => $depentendesProvider,
        ]);
    }

    /**
     * Finds the Membro model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Membro the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Membro::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new Membro model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Membro();
        $dataProvider = new ActiveDataProvider([
            'query' => Dependente::find()
        ]);

        $post = Yii::$app->request->post();
        if ($model->load($post) && $model->save()) {
            Flash::success('Membro cadastrado com sucesso.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing Membro model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Flash::success('Membro alterado com sucesso.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionRelatorio()
    {
        $searchModel = new MembroSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('relatorio', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'queryParams' => $queryParams,
        ]);
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionImprimirRelatorio()
    {
        $searchModel = new MembroSearch();

        $queryParams = Yii::$app->request->queryParams;
        $queryParams = reset($queryParams);

        $activeDataProvider = $searchModel->search($queryParams);
        $activeDataProvider->setPagination(false);

        $models = $activeDataProvider->getModels();
        $dataProvider = new ArrayDataProvider([
            'allModels' => $models,
        ]);

        $content = $this->renderPartial('imprimir-relatorio', [
            'dataProvider' => $dataProvider,
        ]);

        $pdf = new Pdf([
            'marginLeft' => 5,
            'marginRight' => 5,
//            'marginTop' => 5,
            'marginBottom' => 5,
            'mode' => Pdf::MODE_UTF8,
            'content' => $content,
            'methods' => [
                'SetHeader' => ['Relatório de Membro||Gerado em: ' . date('d/m/y H:i')],
                'SetFooter' => ['|Página {PAGENO}|'],
            ]
        ]);

        return $pdf->render();
    }
}
