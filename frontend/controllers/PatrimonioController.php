<?php

namespace frontend\controllers;

use common\components\Flash;
use common\models\Patrimonio;
use common\models\search\PatrimonioSearch;
use kartik\mpdf\Pdf;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\NotFoundHttpException;

/**
 * PatrimonioController implements the CRUD actions for Patrimonio model.
 */
class PatrimonioController extends MainController
{
    /**
     * Lists all Patrimonio models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PatrimonioSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Patrimonio model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Patrimonio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Patrimonio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Patrimonio::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new Patrimonio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Patrimonio();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Flash::success('Patrimônio cadastrado com sucesso.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Patrimonio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Flash::success('Patrimônio alterado com sucesso.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Patrimonio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $refereer = Yii::$app->request->referrer;
        $this->findModel($id)->delete();

        Flash::success('Patrimônio excluido com sucesso.');

        return $this->redirect(strpos($refereer, 'index') !== false ? $refereer : ['index']);
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionRelatorio()
    {
        $searchModel = new PatrimonioSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('relatorio', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'queryParams' => $queryParams,
        ]);
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionImprimirRelatorio()
    {
        $searchModel = new PatrimonioSearch();

        $queryParams = Yii::$app->request->queryParams;
        $queryParams = reset($queryParams);

        $activeDataProvider = $searchModel->search($queryParams);
        $activeDataProvider->setPagination(false);

        $models = $activeDataProvider->getModels();
        $dataProvider = new ArrayDataProvider([
            'allModels' => $models,
        ]);

        $content = $this->renderPartial('imprimir-relatorio', [
            'dataProvider' => $dataProvider,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'content' => $content,
            'methods' => [
                'SetHeader' => ['Relatório de Patrimonio||Gerado em: ' . date('d/m/y H:i')],
                'SetFooter' => ['|Página {PAGENO}|'],
            ]
        ]);

        return $pdf->render();
    }
}
