<?php

namespace frontend\controllers;

use common\components\Flash;
use common\models\search\UsuarioSearch;
use common\models\Usuario;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UsuarioController extends MainController
{
    public function behaviors()
    {
        /** @var Usuario $usuario */
        $usuario = Yii::$app->user->identity;

        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'create', 'update', 'view'],
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => $usuario && $usuario->isGerente(),
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsuarioSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Usuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Usuario();

        $model->setScenario(Usuario::SCENARIO_CADASTRO);

        if ($model->load(Yii::$app->request->post())) {
            $model->setPassword($model->password);

            if ($model->save()) {
                Flash::success('Usuário cadastrado com sucesso.');

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelAlterarSenha = $this->findModel($id);
        $modelAlterarSenha->setScenario(Usuario::SCENARIO_ALTERAR_SENHA);

        $post = Yii::$app->request->post();

        if (ArrayHelper::getValue($post, 'Usuario.password')) {
            if ($modelAlterarSenha->load($post)) {
                $modelAlterarSenha->setPassword($modelAlterarSenha->password);

                if ($modelAlterarSenha->save()) {
                    Flash::success('Senha alteraa com sucesso.');
                }
            }

            return $this->render('update', [
                'model' => $model,
                'modelAlterarSenha' => $modelAlterarSenha,
            ]);
        }

        if ($model->load($post) && $model->save()) {
            Flash::success('Usuário alterado com sucesso.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelAlterarSenha' => $modelAlterarSenha,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($id == Yii::$app->user->identity->getId()) {
            Flash::error('Não é possível excluir seu próprio usuário.');
        } else {
            $this->findModel($id)->delete();

            Flash::success('Usuário excluído com sucesso.');
        }

        return $this->redirect(['index']);
    }
}
