<?php

namespace frontend\models;

use common\components\Flash;
use common\models\EscalaTrabalho;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class EscalaTrabalhoForm extends Model
{
    const SCENARIO_CREATE = 'scenarioCreate';

    /** @var  string */
    public $data;

    /** @var  EscalaTrabalho[] */
    public $escalasManha;

    /** @var  EscalaTrabalho[] */
    public $escalasTarde;

    /** @var  EscalaTrabalho[] */
    public $escalasNoite;

    /**
     * @param $data
     *
     * @return EscalaTrabalhoForm|null
     */
    public static function findOne($data)
    {
        if (!$data) {
            return null;
        }

        $escala = new EscalaTrabalhoForm(['data' => $data]);

        $escala->fetchFromData($data);

        if (empty($escala->escalasManha) && empty($escala->escalasTarde) && empty($escala->escalasNoite)) {
            return null;
        }

        return $escala;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data'], 'required'],
            [['data'], 'verificaData', 'on' => self::SCENARIO_CREATE],
            [['escalasManha', 'escalasTarde', 'escalasNoite'], 'verificarRepetidos'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'data' => 'Data',
            'escalasManha' => 'Manhã',
            'escalasTarde' => 'Tarde',
            'escalasNoite' => 'Noite'
        ];
    }

    public function load($data, $formName = null)
    {
        if (!isset($data['EscalaTrabalhoForm'])) {
            return false;
        }

        $dataEscala = ArrayHelper::getValue($data, 'EscalaTrabalhoForm.data');
        $escalasManha = ArrayHelper::getValue($data, 'EscalaTrabalhoForm.escalasManha') ?: [];
        $escalasTarde = ArrayHelper::getValue($data, 'EscalaTrabalhoForm.escalasTarde') ?: [];
        $escalasNoite = ArrayHelper::getValue($data, 'EscalaTrabalhoForm.escalasNoite') ?: [];

        if (empty($escalasManha) && empty($escalasTarde) && empty($escalasNoite)) {
            Flash::error('Favor alocar pelo menos um membro para uma função.');

            return false;
        }

        $this->escalasManha = [];
        $this->escalasTarde = [];
        $this->escalasNoite = [];

        foreach ($escalasManha as $escalaTrabalho) {
            $this->escalasManha[] = new EscalaTrabalho($escalaTrabalho + ['data' => $dataEscala]);
        }

        foreach ($escalasTarde as $escalaTrabalho) {
            $this->escalasTarde[] = new EscalaTrabalho($escalaTrabalho + ['data' => $dataEscala]);
        }

        foreach ($escalasNoite as $escalaTrabalho) {
            $this->escalasNoite[] = new EscalaTrabalho($escalaTrabalho + ['data' => $dataEscala]);
        }

        $this->data = $dataEscala;

        return true;
    }

    /**
     * @return bool
     */
    public function save()
    {
        foreach ($this->escalasManha as $escalaTrabalho) {
            $escalaTrabalho->save();
        }

        foreach ($this->escalasTarde as $escalaTrabalho) {
            $escalaTrabalho->save();
        }

        foreach ($this->escalasNoite as $escalaTrabalho) {
            $escalaTrabalho->save();
        }

        return true;
    }

    public function preencherComAnterior()
    {
        $data = EscalaTrabalho::find()->select('MAX(data)')->scalar();

        if ($data) {
            $this->fetchFromData($data);
        }
    }

    /**
     * @param $data
     */
    public function fetchFromData($data)
    {
        $this->escalasManha = EscalaTrabalho::find()->andWhere(['data' => $data, 'turno' => EscalaTrabalho::TURNO_MANHA])->all();

        $this->escalasTarde = EscalaTrabalho::find()->andWhere(['data' => $data, 'turno' => EscalaTrabalho::TURNO_TARDE])->all();

        $this->escalasNoite = EscalaTrabalho::find()->andWhere(['data' => $data, 'turno' => EscalaTrabalho::TURNO_NOITE])->all();
    }

    /**
     * @return bool
     */
    public function delete()
    {
        foreach ($this->escalasManha as $escalaTrabalho) {
            EscalaTrabalho::deleteAll([
                'funcao_id' => $escalaTrabalho->funcao_id,
                'membro_id' => $escalaTrabalho->membro_id,
            ]);
        }

        foreach ($this->escalasTarde as $escalaTrabalho) {
            EscalaTrabalho::deleteAll([
                'funcao_id' => $escalaTrabalho->funcao_id,
                'membro_id' => $escalaTrabalho->membro_id,
            ]);
        }

        foreach ($this->escalasNoite as $escalaTrabalho) {
            EscalaTrabalho::deleteAll([
                'funcao_id' => $escalaTrabalho->funcao_id,
                'membro_id' => $escalaTrabalho->membro_id,
            ]);
        }

        return true;
    }

    /**
     * @param $attribute
     * @param $params
     * @param $validator
     */
    public function verificarRepetidos($attribute, $params, $validator)
    {
        $escalas = $this->$attribute;
        /**
         * @var int $index
         * @var EscalaTrabalho[] $escalas
         */
        foreach ($escalas as $index => $escala) {
            foreach ($escalas as $index2 => $escalaExistente) {
                if ($index2 != $index && $this->equals($escala, $escalaExistente)) {
                    $key = $attribute . '[' . $index . '][funcao_id]';
                    $this->addError($key, 'Membro já possui está função para este turno.');
                }
            }

            /** @var EscalaTrabalho $escala */
            $escala->validate();

            $firstErrorFuncao = $escala->getFirstError('funcao_id');
            if ($firstErrorFuncao) {
                $key = $attribute . '[' . $index . '][funcao_id]';
                $this->addError($key, $firstErrorFuncao);
            }

            $firstErrorMembro = $escala->getFirstError('membro_id');
            if ($firstErrorMembro) {
                $key = $attribute . '[' . $index . '][membro_id]';
                $this->addError($key, $firstErrorMembro);
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @param $validator
     */
    public function verificaData($attribute, $params, $validator)
    {
        $data = $this->$attribute;
        if (EscalaTrabalho::find()->andWhere(['data' => $data])->exists()) {
            $this->addError($attribute, 'Já existe Escala de Trabalho cadastrada para esta data');
        }
    }

    /**
     * @param EscalaTrabalho $escala
     * @param EscalaTrabalho $escalaExistente
     *
     * @return bool
     */
    private function equals(EscalaTrabalho $escala, EscalaTrabalho $escalaExistente)
    {
        if ($escala->funcao_id == $escalaExistente->funcao_id
            && $escala->membro_id == $escalaExistente->membro_id
            && $escala->data == $escalaExistente->data
            && $escala->turno == $escalaExistente->turno) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function update()
    {
        EscalaTrabalho::deleteAll(['data' => $this->data]);

        return $this->save();
    }
}
