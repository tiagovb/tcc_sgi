<?php


namespace frontend\models;


use common\models\Usuario;
use Yii;

class MenuItems
{
    public static function create()
    {
        /** @var Usuario $usuario */
        $usuario = Yii::$app->user->identity;

        if (!$usuario) {
            return [];
        }

        $menuItems = [
            ['icon' => 'home', 'label' => 'Página Principal', 'url' => ['/site']],

            ['icon' => 'users', 'label' => 'Membros', 'items' => [
                [
                    'label' => 'Consultar',
                    'url' => ['/membro/index'],
                ],
                [
                    'label' => 'Cadastrar',
                    'url' => ['/membro/create'],
                ],
                [
                    'label' => 'Relatório',
                    'url' => ['/membro/relatorio'],
                ],
            ]],
            ['icon' => 'child', 'label' => 'Familias Beneficiadas', 'items' => [
                [
                    'label' => 'Consultar',
                    'url' => ['/familia-beneficiada/index'],
                ],
                [
                    'label' => 'Cadastrar',
                    'url' => ['/familia-beneficiada/create'],
                ],
                [
                    'label' => 'Relatório',
                    'url' => ['/familia-beneficiada/relatorio'],
                ],
            ]],
            ['icon' => 'refresh', 'label' => 'Escala de Trabalho', 'items' => [
                [
                    'label' => 'Consultar',
                    'url' => ['/escala-trabalho/index'],
                ],
                [
                    'label' => 'Cadastrar',
                    'url' => ['/escala-trabalho/create'],
                ],
//                [
//                    'label' => 'Relatório',
//                    'url' => ['/escala-trabalho/relatorio'],
//                ],
            ]],
            ['icon' => 'th-list', 'label' => 'Compromissos', 'items' => [
                [
                    'label' => 'Consultar',
                    'url' => ['/compromisso/index'],
                ],
                [
                    'label' => 'Cadastrar',
                    'url' => ['/compromisso/create'],
                ],
                [
                    'label' => 'Relatório',
                    'url' => ['/compromisso/relatorio'],
                ],
            ]],
            ['icon' => 'calendar-check-o', 'label' => 'Atividades', 'items' => [
                [
                    'label' => 'Consultar',
                    'url' => ['/atividade/index'],
                ],
                [
                    'label' => 'Cadastrar',
                    'url' => ['/atividade/create'],
                ],
                [
                    'label' => 'Relatório',
                    'url' => ['/atividade/relatorio'],
                ],
            ]],
            ['icon' => 'cogs', 'label' => 'Funções de Trabalho', 'items' => [
                [
                    'label' => 'Consultar',
                    'url' => ['/funcao/index'],
                ],
                [
                    'label' => 'Cadastrar',
                    'url' => ['/funcao/create'],
                ],
            ]],

            //Despesas
            ['icon' => 'money', 'label' => 'Despesas', 'items' => [
                [
                    'label' => 'Consultar',
                    'url' => ['/despesa/index'],
                ],
                [
                    'label' => 'Cadastrar',
                    'url' => ['/despesa/create'],
                ],
                [
                    'label' => 'Relatório',
                    'url' => ['/despesa/relatorio'],
                ],
            ]],

            //Patrimônio
            ['icon' => 'building-o', 'label' => 'Patrimônio', 'items' => [
                [
                    'label' => 'Consultar',
                    'url' => ['/patrimonio/index'],
                ],
                [
                    'label' => 'Cadastrar',
                    'url' => ['/patrimonio/create'],
                ],
                [
                    'label' => 'Relatório',
                    'url' => ['/patrimonio/relatorio'],
                ],
            ]],

            //Usuário
            [
                'icon' => 'user',
                'label' => 'Usuários',
                'visible' => $usuario && $usuario->isGerente(),
                'items' => [
                    [
                        'label' => 'Consultar',
                        'url' => ['/usuario/index'],
                    ],
                    [
                        'label' => 'Cadastrar',
                        'url' => ['/usuario/create'],
                    ],
                ],
            ],
        ];

        return $menuItems;


    }
}
