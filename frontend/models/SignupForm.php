<?php

namespace frontend\models;

use common\models\Usuario;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $tipoUsuario;
    public $password;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => Usuario::class,'targetAttribute' => 'login', 'message' => 'Já existe usuário com username informado.'],
            ['username', 'string', 'min' => 2, 'max' => 100],

            ['tipoUsuario', 'required'],

            ['password', 'required'],
            ['password', 'string', 'min' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Login',
            'password' => 'Senha',
            'tipoUsuario' => 'Tipo',
        ];
    }

    /**
     * Signs user up.
     *
     * @return Usuario|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new Usuario();
        $user->login = $this->username;
        $user->setPassword($this->password);
//        $user->generateAuthKey();

        $save = $user->save();
        return $save ? $user : null;
    }
}
