<?php

use common\helpers\ActiveForm;
use kartik\datecontrol\DateControl;

/** @var $this yii\web\View */
/** @var $model common\models\Atividade */

?>

<div class="box box-<?= $model->colorTypeBox() ?>">
    <div class="box-body">
        <div class="atividade-form">
            <div class="row">
                <div class="col-sm-6">

                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'dataHora')->widget(DateControl::className(), [
                        'type' => DateControl::FORMAT_DATETIME,
                        'widgetOptions' => [
                            'options' => ['placeholder' => 'Informe data e hora da atividade...'],
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'startDate' => date('d-m-Y H:i'),
                                'minuteStep' => 5,
                                'todayHighlight' => true,
                            ],
                        ],
                    ]); ?>

                    <?= $form->field($model, 'descricao')->textarea(['rows' => 2, 'maxlenght' => true]) ?>

                    <?= $this->render('../layouts/_buttonsForm', ['model' => $model]) ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
