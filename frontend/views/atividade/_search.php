<?php

use common\helpers\ActiveForm;
use common\models\search\AtividadeSearch;
use kartik\helpers\Html;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model AtividadeSearch */
/* @var $form ActiveForm */

$buscaMes = AtividadeSearch::BUSCA_MES;
$buscaAno = AtividadeSearch::BUSCA_ANO;

$js = <<<JS
function setTipoBusca(radioValue, speed = 300) {
    if(radioValue){ 
    
    var selectMes = $('#busca-mes'),
        selectAno = $('#busca-ano');

    $('#busca-picker').show();
    
    if (parseInt(radioValue) === {$buscaAno}) {
        selectMes.slideUp(speed);
        selectAno.slideDown(speed);
    }

    if (parseInt(radioValue) === {$buscaMes}) {
        selectAno.slideUp(speed);
        selectMes.slideDown(speed);
    }}
}

$('input[name="AtividadeSearch[tipoBusca]"][type="radio"]').on('click', function() {
    setTipoBusca($(this).val());
});
var tipoBusca = "{$model->tipoBusca}";
if (tipoBusca) {setTipoBusca(tipoBusca, 0);}
JS;

$this->registerJs($js);
?>
<div class="row atividade-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'enableClientValidation' => true
    ]); ?>

    <div class="col-sm-3">
        <?= $form->field($model, 'descricao') ?>
    </div>

    <div class="col-sm-2">
        <?= $form->field($model, 'tipoBusca')
            ->radioList([
                $buscaMes => 'Mês',
                $buscaAno => 'Ano',
            ], ['id' => 'atividadesearch-tipobusca', 'inline' => true])
            ->label('Buscar por') ?>
    </div>

    <div id="busca-picker" class="col-sm-2">
        <div id="busca-mes" style="display: none;">
            <?= $form->field($model, 'mes')->widget(DatePicker::className(), [
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose' => true,
                    'startView' => 'months',
                    'minViewMode' => 'months',
                    'format' => 'yyyy-mm'
                ],
            ])->label('Mês') ?>
        </div>

        <div id="busca-ano" style="display: none;">
            <?= $form->field($model, 'ano')->widget(DatePicker::className(), [
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose' => true,
                    'startView' => 'year',
                    'minViewMode' => 'years',
                    'format' => 'yyyy'
                ],
            ])->label('Ano') ?>
        </div>

    </div>

    <div class="col-sm-3">
        <?= $this->render('../layouts/_buttonsSearch') ?>
    </div>

    <div class="col-md-2" style="padding-left: 0;padding-right: 0;">
        <label class="control-label" for="add-button-container">&nbsp;</label>
        <div id="add-button-container">
            <?= Html::a(Html::icon('plus') . ' Cadastrar Atividade', ['create'], ['class' => 'btn btn-success', 'style' => 'padding-left: 5px;']); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
