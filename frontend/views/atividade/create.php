<?php

/* @var $this yii\web\View */
/* @var $model common\models\Atividade */

$this->title = 'Cadastrar Atividade';
$this->params['breadcrumbs'][] = ['label' => 'Atividades', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="atividade-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
