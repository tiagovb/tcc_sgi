<?php

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\models\search\AtividadeSearch;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\AtividadeSearch */

$ano = $searchModel->ano;
?>
<div class="row relatorio">
    <div class="col-xs-12 text-center">
        <h3>Atividades  - <?= $ano ?></h3>
    </div>
    <div class="col-xs-12">
        <table id="calendario" class="table" style="border: 1px solid #999; border-collapse: collapse; vertical-align:top;">
            <tr style="border: 1px solid #999; border-collapse: collapse;">
                <?php for ($i = 1; $i <= 3; $i++) :
                    $date = date('Y-m', mktime(0, 0, 0, $i, 1, $ano));
                    $query = [
                        'AtividadeSearch' => [
                            'tipoBusca' => 1,
                            'mes' => $date,
                            'ano' => $ano,
                        ]
                    ];
                    $atividadeSearch = new AtividadeSearch();
                    $dataProviderMes = $atividadeSearch->search($query);
                    ?>

                    <td style="border: 1px solid #999; border-collapse: collapse;" align="center">
                        <?= $this->render('imprimir-meses', ['mes' => $atividadeSearch->mes, 'dataProvider' => $dataProviderMes])?>
                    </td>
                <?php endfor; ?>
            </tr>

            <tr style="border: 1px solid #999; border-collapse: collapse;">
                <?php for ($i = 4; $i <= 6; $i++) :
                    $date = date('Y-m', mktime(0, 0, 0, $i, 1, $ano));
                    $query = [
                        'AtividadeSearch' => [
                            'tipoBusca' => 1,
                            'mes' => $date,
                            'ano' => $ano,
                        ]
                    ];
                    $atividadeSearch = new AtividadeSearch();
                    $dataProviderMes = $atividadeSearch->search($query);
                    ?>

                    <td style="border: 1px solid #999; border-collapse: collapse;" align="center">
                        <?= $this->render('imprimir-meses', ['mes' => $atividadeSearch->mes, 'dataProvider' => $dataProviderMes])?>
                    </td>
                <?php endfor; ?>
            </tr>

            <tr>
                <?php for ($i = 7; $i <= 9; $i++) :
                    $date = date('Y-m', mktime(0, 0, 0, $i, 1, $ano));
                    $query = [
                        'AtividadeSearch' => [
                            'tipoBusca' => 1,
                            'mes' => $date,
                            'ano' => $ano,
                        ]
                    ];
                    $atividadeSearch = new AtividadeSearch();
                    $dataProviderMes = $atividadeSearch->search($query);
                    ?>

                    <td style="border: 1px solid #999; border-collapse: collapse;" align="center">
                        <?= $this->render('imprimir-meses', ['mes' => $atividadeSearch->mes, 'dataProvider' => $dataProviderMes])?>
                    </td>
                <?php endfor; ?>
            </tr>

            <tr>
                <?php for ($i = 10; $i <= 12; $i++) :
                    $date = date('Y-m', mktime(0, 0, 0, $i, 1, $ano));
                    $query = [
                        'AtividadeSearch' => [
                            'tipoBusca' => 1,
                            'mes' => $date,
                            'ano' => $ano,
                        ]
                    ];
                    $atividadeSearch = new AtividadeSearch();
                    $dataProviderMes = $atividadeSearch->search($query);
                    ?>

                    <td style="border: 1px solid #999; border-collapse: collapse;" align="center">
                        <?= $this->render('imprimir-meses', ['mes' => $atividadeSearch->mes, 'dataProvider' => $dataProviderMes])?>
                    </td>
                <?php endfor; ?>
            </tr>
        </table>
    </div>
</div>
