<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $mes string */
/* @var $dataProvider yii\data\ActiveDataProvider */

$dataProvider->sort = false;
?>
<h4><?= ucwords(Yii::$app->formatter->asDate($mes, 'php:F')) ?></h4>
<?php if (!$dataProvider->models) : ?>
    <br>
    <table class="table table-vazia">
        <tr>
            <td>
                Sem atividades neste mês
            </td>
        </tr>
    </table>
<?php else : ?>
    <?= GridView::widget([
        'showHeader' => false,
        'tableOptions' => ['class' => 'table table-mes'],
        'layout' => '{items}',
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'dataHora',
                'format' => 'dateTime',
                'contentOptions' => ['class' => 'text-center small'],
            ],
            [
                'attribute' => 'descricao',
                'contentOptions' => ['class' => 'text-center small'],
            ],
        ],
    ]); ?>
<?php endif; ?>
