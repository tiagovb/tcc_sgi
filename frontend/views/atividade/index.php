<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\AtividadeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consultar Atividade';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="funcao-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">

                <div class="box-header with-border">
                    <?= $this->render('_search', ['model' => $searchModel]); ?>
                </div>


                    <div class="box-body">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,

                            'columns' => [
                                'descricao',
                                'dataHora:dateTime',

                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </div>
            </div>
        </div>
    </div>
</div>

