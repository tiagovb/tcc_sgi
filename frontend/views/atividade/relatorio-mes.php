<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $mes string */
/* @var $calendarioFull boolean */
/* @var $dataProvider yii\data\ActiveDataProvider */

$dataProvider->sort = false;
if ($calendarioFull) {
    $formato = 'php:F';
    $classTabela = 'table';
} else {
    $formato = 'php:F/Y';
    $classTabela = 'table table-bordered';
}
?>
<div class="row">
    <div class="col-xs-12 text-center">
        <h3><?= ucwords(Yii::$app->formatter->asDate($mes, $formato)) ?></h3>
    </div>

    <div class="col-xs-12 text-center">
        <?= GridView::widget([
            'showHeader' => false,
            'tableOptions' => ['class' => $classTabela],
            'layout' => '{items}',
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'dataHora',
                    'format' => 'dateTime',
                    'contentOptions' => ['class' => 'text-center small'],
                ],
                [
                    'attribute' => 'descricao',
                    'contentOptions' => ['class' => 'text-center small'],
                ],
            ],
        ]); ?>

    </div>
</div>
