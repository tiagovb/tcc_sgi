<?php

use common\helpers\ActiveForm;
use common\models\search\AtividadeSearch;
use kartik\helpers\Html;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\AtividadeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $queryParams array */

$this->title = 'Relatório de Atividades';
$this->params['breadcrumbs'][] = $this->title;

$buscaMes = AtividadeSearch::BUSCA_MES;
$buscaAno = AtividadeSearch::BUSCA_ANO;

$js = <<<JS
function setTipoBusca(radioValue, speed = 300) {
    if(radioValue){ 
    
    var selectMes = $('#busca-mes'),
        selectAno = $('#busca-ano');

    $('#busca-picker').show();
    
    if (parseInt(radioValue) === {$buscaAno}) {
        selectMes.slideUp(speed);
        selectAno.slideDown(speed);
    }

    if (parseInt(radioValue) === {$buscaMes}) {
        selectAno.slideUp(speed);
        selectMes.slideDown(speed);
    }}
}

$('input[name="AtividadeSearch[tipoBusca]"][type="radio"]').on('click', function() {
    setTipoBusca($(this).val());
});
var tipoBusca = "{$searchModel->tipoBusca}";
if (tipoBusca) {setTipoBusca(tipoBusca, 0);}
JS;

$this->registerJs($js);
?>
<div class="funcao-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">

                <div class="box-header with-border">
                    <div class="row atividade-search">

                        <?php $form = ActiveForm::begin([
                            'method' => 'get',
                            'enableClientValidation' => true
                        ]); ?>

                        <div class="col-sm-2" style="padding-left: 20px;">
                            <?= $form->field($searchModel, 'tipoBusca')
                                ->radioList([
                                    $buscaMes => 'Mês',
                                    $buscaAno => 'Ano',
                                ], ['id' => 'atividadesearch-tipobusca', 'inline' => true])
                                ->label('Buscar por') ?>
                        </div>

                        <div id="busca-picker" class="col-sm-2" style="display: none;">
                            <div id="busca-mes" style="display: none;">
                                <?= $form->field($searchModel, 'mes')->widget(DatePicker::className(), [
                                    'layout' => '{input}{picker}',
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'startView' => 'months',
                                        'minViewMode' => 'months',
                                        'format' => 'yyyy-mm'
                                    ],
                                ])->label('Mês') ?>
                            </div>

                            <div id="busca-ano" style="display: none;">
                                <?= $form->field($searchModel, 'ano')->widget(DatePicker::className(), [
                                    'layout' => '{input}{picker}',
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'startView' => 'year',
                                        'minViewMode' => 'years',
                                        'format' => 'yyyy'
                                    ],
                                ])->label('Ano') ?>
                            </div>

                        </div>

                        <div class="col-sm-5">
                            <?= $this->render('../layouts/_buttonsSearch') ?>
                        </div>

                        <div class="col-sm-3 text-right">
                            <?php if (!empty($dataProvider->models)) : ?>

                                <label class="control-label" for="buttons-container">&nbsp;</label>
                                <div id="buttons-container">
                                    <?=
                                    Html::a('Imprimir', ['imprimir-relatorio', $queryParams], ['target' => '_blank', 'class' => 'btn bg-navy btn-md']) ?>
                                </div>

                            <?php endif; ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>

                <?php if (Yii::$app->request->queryParams) : ?>
                    <div class="box-body">
                        <?php if ($searchModel->tipoBusca == AtividadeSearch::BUSCA_MES) : ?>
                            <div class="row">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <?= $this->render('relatorio-mes', [
                                        'dataProvider' => $dataProvider,
                                        'mes' => $searchModel->mes,
                                        'calendarioFull' => $searchModel->tipoBusca == AtividadeSearch::BUSCA_ANO,
                                    ]); ?>
                                </div>
                            </div>
                        <?php elseif ($searchModel->tipoBusca == AtividadeSearch::BUSCA_ANO) : ?>
                            <?= $this->render('relatorio-ano', [
                                'searchModel' => $searchModel,
                            ]); ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

