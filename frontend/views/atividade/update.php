<?php

/* @var $this yii\web\View */
/* @var $model common\models\Atividade */

$this->title = 'Editar Atividade: ' . $model->descricao;
$this->params['breadcrumbs'][] = ['label' => 'Atividades', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->descricao, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="atividade-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
