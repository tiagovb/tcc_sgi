<?php

/* @var $this yii\web\View */
/* @var $model common\models\Compromisso */

$this->title = 'Cadastrar Compromisso';
$this->params['breadcrumbs'][] = ['label' => 'Compromissos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compromisso-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
