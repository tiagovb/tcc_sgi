<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CompromissoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consultar Compromisso';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compromisso-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                </div>


                    <div class="box-body">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                [
                                    'attribute' => 'id',
                                    'headerOptions' => ['class' => 'col-xs-1'],
                                    'contentOptions' => ['class' => 'col-xs-1'],
                                ],
                                [
                                    'attribute' => 'descricao',
//                                    'headerOptions' => ['class' => 'col-xs-4'],
//                                    'contentOptions' => ['class' => 'col-xs-4'],
                                ],
                                [
                                    'attribute' => 'data',
                                    'format' => 'dateTime',
                                ],
                                [
                                    'attribute' => 'localizacao',
//                                    'headerOptions' => ['class' => 'col-xs-4'],
//                                    'contentOptions' => ['class' => 'col-xs-4'],
                                ],

                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </div>

            </div>
        </div>
    </div>
</div>