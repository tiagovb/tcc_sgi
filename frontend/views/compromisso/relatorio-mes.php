<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $mes string */
/* @var $dataProvider yii\data\ActiveDataProvider */

$dataProvider->sort = false;
$formato = 'php:F/Y';
$classTabela = 'table table-bordered';
?>
<div class="row">
    <div class="col-xs-12 text-center">
        <h3><?= ucwords(Yii::$app->formatter->asDate($mes, $formato)) ?></h3>
    </div>

    <div class="col-xs-12 text-center">
        <?= GridView::widget([
            'tableOptions' => ['class' => $classTabela],
            'layout' => '{items}',
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'data',
                    'format' => 'dateTime',
                    'contentOptions' => ['class' => 'text-center small'],
                    'headerOptions' => ['class' => 'text-center small'],
                ],
                [
                    'headerOptions' => ['class' => 'text-center small'],
                    'attribute' => 'descricao',
                    'contentOptions' => ['class' => 'text-center small'],
                ],
                [
                    'headerOptions' => ['class' => 'text-center small'],
                    'attribute' => 'localizacao',
                    'contentOptions' => ['class' => 'text-center small'],
                ],
            ],
        ]); ?>

    </div>
</div>
