<?php

use common\helpers\ActiveForm;
use kartik\helpers\Html;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CompromissoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $queryParams array */

$this->title = 'Relatório de Compromissos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="funcao-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">

                <div class="box-header with-border">
                    <div class="row compromisso-search">

                        <?php $form = ActiveForm::begin([
                            'method' => 'get',
                            'enableClientValidation' => true
                        ]); ?>

                        <div class="col-sm-2">
                            <div>
                                <?= $form->field($searchModel, 'mes')->widget(DatePicker::className(), [
                                    'layout' => '{input}{picker}',
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'startView' => 'months',
                                        'minViewMode' => 'months',
                                        'format' => 'yyyy-mm'
                                    ],
                                ])->label('Mês') ?>
                            </div>

                        </div>

                        <div class="col-sm-5">
                            <?= $this->render('../layouts/_buttonsSearch') ?>
                        </div>

                        <div class="col-sm-3 text-right">
                            <?php if (!empty($dataProvider->models)) : ?>

                                <label class="control-label" for="buttons-container">&nbsp;</label>
                                <div id="buttons-container">
                                    <?= Html::a('Imprimir', ['imprimir-relatorio', $queryParams], ['target' => '_blank', 'class' => 'btn bg-navy btn-md']) ?>
                                </div>

                            <?php endif; ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>

                <?php if (Yii::$app->request->queryParams) : ?>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-offset-3 col-sm-6">
                                <?= $this->render('relatorio-mes', [
                                    'dataProvider' => $dataProvider,
                                    'mes' => $searchModel->mes,
                                ]); ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

