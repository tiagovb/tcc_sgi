<?php

/* @var $this yii\web\View */
/* @var $model common\models\Compromisso */

$this->title = 'Editar Compromisso: ' . $model->descricao;
$this->params['breadcrumbs'][] = ['label' => 'Compromissos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->descricao, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="compromisso-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
