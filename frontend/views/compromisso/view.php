<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Compromisso */

$this->title = 'Compromisso: ' . $model->descricao;
$this->params['breadcrumbs'][] = ['label' => 'Compromissos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compromisso-view">
    <div class="row" style="margin-bottom: 5px;">
        <div class="col-md-6">
            <?= Html::a('Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>&nbsp;&nbsp;
            <?= Html::a('Excluir', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Tem certeza que deseja excluir este item?',
                    'method' => 'post',
                ],
            ]) ?>&nbsp;&nbsp;

            <?= Html::a(Html::icon('plus') . ' Cadastrar Novo Compromisso', ['create'], ['class' => 'btn btn-success']); ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'descricao:ntext',
                                    'data:dateTime',
                                    'localizacao:ntext',
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>