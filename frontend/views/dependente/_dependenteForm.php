<?php

use common\helpers\ActiveForm;
use common\models\Dependente;
use kartik\datecontrol\DateControl;
use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $dependente Dependente */
/* @var $action array */

?>
<div class="modal fade" id="modalDependenteForm" tabindex="-1" role="dialog" aria-labelledby="modalDependenteFormLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalDependenteFormLabel">Dependente</h4>
            </div>

            <?php $form = ActiveForm::begin([
                'action' => $action,
                'id' => 'form-dependente' . $dependente->id,
                'type' => ActiveForm::TYPE_HORIZONTAL,
                'formConfig' => ['labelSpan' => 4, 'deviceSize' => ActiveForm::SIZE_SMALL],
                'enableClientValidation' => true,
            ]); ?>

            <div class="modal-body">
                <?= $form->field($dependente, 'nome')->textInput(); ?>

                <?= $form->field($dependente, 'dtNascimento')->widget(DateControl::className(), [
                    'type' => DateControl::FORMAT_DATE,
                    'widgetOptions' => [
                        'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayHighlight' => true,
                        ],
                    ],
                ]); ?>

                <?= $form->field($dependente, 'religiao')->drop(Dependente::getEnumListLabels('religiao')) ?>

                <?= Html::activeHiddenInput($dependente, 'membro_id') ?>
            </div>

            <div class="modal-footer">
                <?= Html::submitButton('Salvar', ['class' => "btn btn-{$dependente->colorTypeBox()}"]) ?>
                <?= Html::button(Yii::t('app', 'Fechar'), ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
