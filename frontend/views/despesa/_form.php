<?php

use common\helpers\ActiveForm;
use common\models\Despesa;
use kartik\datecontrol\DateControl;
use kartik\money\MaskMoney;

/* @var $this yii\web\View */
/* @var $model common\models\Despesa */
/* @var $form ActiveForm */
?>
<div class="box box-<?= $model->colorTypeBox() ?>">
    <div class="box-body">
        <div class="despesa-form">
            <div class="row">
                <div class="col-sm-6">
                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'dtReferencia')->widget(DateControl::className(), [
                        'type' => DateControl::FORMAT_DATE,
                        'widgetOptions' => [
                            'options' => ['placeholder' => 'Informe a data de referência...'],
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'todayHighlight' => true,
                            ],
                        ],
                    ]); ?>

                    <?= $form->field($model, 'valor')->widget(MaskMoney::className(), [
                        'pluginOptions' => [
                            'prefix' => 'R$',
                            'allowNegative' => false,
                            'thousands' => '.',
                            'decimal' => ',',
                            'precision' => 2,
                        ],
                    ]) ?>


                    <?= $form->field($model, 'descricao')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'notaFiscal')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'quantidade')->widget(MaskMoney::className(), [
                        'pluginOptions' => [
                            'prefix' => '# ',
                            'allowNegative' => false,
                            'thousands' => '.',
                            'decimal' => ',',
                            'precision' => 2,
                        ],
                    ]) ?>

                    <?= $form->field($model, 'tipo')->drop(Despesa::getEnumListLabels('tipo')) ?>

                    <?= $this->render('../layouts/_buttonsForm', ['model' => $model]) ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
