<?php


/* @var $this yii\web\View */

use common\helpers\ActiveForm;
use kartik\helpers\Html;

/* @var $model common\models\search\DespesaSearch */
/* @var $form ActiveForm */
?>

<div class="row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-sm-3">
        <?= $form->field($model, 'descricao') ?>
    </div>

    <div class="col-sm-3">
        <?= $form->field($model, 'tipo')->drop(\common\models\Despesa::getEnumListLabels('tipo')) ?>
    </div>

    <div class="col-sm-3">
        <?= $this->render('../layouts/_buttonsSearch') ?>
    </div>

    <div class="col-md-3 text-right">
        <label class="control-label" for="add-button-container">&nbsp;</label>
        <div id="add-button-container">
            <?= Html::a(Html::icon('plus') . ' Cadastrar Despesa', ['create'], ['class' => 'btn btn-success']); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
