<?php

/* @var $this yii\web\View */
/* @var $model common\models\Despesa */

$this->title = 'Cadastrar Despesa';
$this->params['breadcrumbs'][] = ['label' => 'Despesas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="despesa-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
