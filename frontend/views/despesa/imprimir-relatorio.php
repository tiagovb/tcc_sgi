<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PatrimonioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $totalDespesas string */

$this->title = 'Relatório de Despesas';
$this->params['breadcrumbs'][] = $this->title;
$dataProvider->setPagination(false);
?>

<?php if ($totalDespesas) : ?>
    <h5 class="page-header">
        Total de despesas: <?= Yii::$app->formatter->asCurrency($totalDespesas) ?>
    </h5>
<?php endif; ?>

<?= GridView::widget([
    'layout' => '{items}',
    'dataProvider' => $dataProvider,
    'columns' => [
        'dtReferencia:date',
        'valor:currency',
        'descricao',
        'quantidade:decimal',
        'notaFiscal',
        [
            'attribute' => 'tipo',
            'value' => function ($model) {
                return \common\models\Despesa::getEnumLabel('tipo', $model->tipo);
            }
        ],
    ],
]); ?>
