<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\DespesaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consultar Despesa';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="despesa-index">

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                </div>
                    <div class="box-body">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                'id',
                                'dtReferencia:date',
                                'valor:currency',
                                'descricao',
                                'quantidade:decimal',
                                'notaFiscal',
                                [
                                    'attribute' => 'tipo',
                                    'value' => function ($model) {
                                        return \common\models\Despesa::getEnumLabel('tipo', $model->tipo);
                                    }
                                ],
                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </div>
            </div>
        </div>
    </div>
</div>
