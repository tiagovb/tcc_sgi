<?php

use common\helpers\ActiveForm;
use common\models\Despesa;
use kartik\helpers\Html;
use kartik\widgets\DatePicker;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\DespesaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $queryParams array */

$this->title = 'Relatório de Despesas';
$this->params['breadcrumbs'][] = $this->title;

$querySoma = $dataProvider ? clone($dataProvider->query) : null;
$totalDespesas = $querySoma ? $querySoma->select('SUM(valor)')->scalar() : null;
?>
<div class="patrimonio-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="row">

                        <?php $form = ActiveForm::begin([
                            'method' => 'get',
                        ]); ?>

                        <div class="col-sm-3">
                            <?= $form->field($searchModel, 'competencia')->widget(DatePicker::className(), [
                                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'startView' => 'year',
                                    'minViewMode' => 'months',
                                    'format' => 'yyyy-mm'
                                ],
                            ])->label('Mês') ?>
                        </div>

                        <div class="col-sm-3">
                            <?= $form->field($searchModel, 'tipo')->drop(Despesa::getEnumListLabels('tipo')) ?>
                        </div>


                        <div class="col-sm-3">
                            <?= $this->render('../layouts/_buttonsSearch') ?>
                        </div>

                        <div class="col-sm-3 text-right">
                            <?php if (!empty($dataProvider->models)) : ?>

                                <label class="control-label" for="buttons-container">&nbsp;</label>
                                <div id="buttons-container">
                                    <?=
                                    Html::a('Imprimir', ['imprimir-relatorio', $queryParams], ['target' => '_blank', 'class' => 'btn bg-navy btn-md']) ?>
                                </div>

                            <?php endif; ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>


                    <div class="box-body">

                        <?php if ($totalDespesas) : ?>
                            <h2 class="page-header">
                                Total de despesas: <?= Yii::$app->formatter->asCurrency($totalDespesas) ?>
                            </h2>
                        <?php endif; ?>

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [

                                'id',
                                'dtReferencia:date',
                                'valor:currency',
                                'descricao',
                                'quantidade:decimal',
                                'notaFiscal',
                                [
                                    'attribute' => 'tipo',
                                    'value' => function ($model) {
                                        return \common\models\Despesa::getEnumLabel('tipo', $model->tipo);
                                    }
                                ],

                                ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
                            ],
                        ]); ?>
                    </div>

            </div>
        </div>
    </div>

</div>
