<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Despesa */

$this->title = 'Despesa: ' . $model->descricao;
$this->params['breadcrumbs'][] = ['label' => 'Despesas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row" style="margin-bottom: 5px;">
    <div class="col-md-6">
        <?= Html::a('Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>&nbsp;&nbsp;
        <?= Html::a('Excluir', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Tem certeza que deseja excluir este item?',
                'method' => 'post',
            ],
        ]) ?>&nbsp;&nbsp;

        <?= Html::a(Html::icon('plus') . ' Cadastrar Despesa', ['create'], ['class' => 'btn btn-success']); ?>
    </div>
</div>

<div class="box">
    <div class="box-body">
        <div class="funcao-form">
            <div class="row">
                <div class="col-md-6">
                    <div class="despesa-view">

                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                'dtReferencia:date',
                                'valor:currency',
                                'descricao',
                                'quantidade:decimal',
                                [
                                    'attribute' => 'tipo',
                                    'value' => \common\models\Despesa::getEnumLabel('tipo', $model->tipo),
                                ],
                                'notaFiscal',
                            ],
                        ]) ?>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
