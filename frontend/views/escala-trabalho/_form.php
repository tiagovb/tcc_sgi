<?php

use common\helpers\ActiveForm;
use common\models\EscalaTrabalho;
use common\models\Funcao;
use common\models\Membro;
use frontend\models\EscalaTrabalhoForm;
use kartik\datecontrol\DateControl;
use kartik\helpers\Html;
use kartik\widgets\Select2;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model EscalaTrabalhoForm */
/* @var $form ActiveForm */

$membros = Membro::find()->select(['id', 'nome'])->asArray()->all();
$funcoes = Funcao::find()->select(['id', 'nome'])->orderBy(['id' => SORT_ASC])->asArray()->all();

$css = <<<CSS
hr{
margin-top: 1px;
margin-bottom: 1px;
}
.table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #474444;
}
CSS;

$this->registerCss($css);
?>
<div class="row">
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-body">
                <div class="escala-trabalho-form">
                    <div class="row">
                        <div class="col-sm-12">

                            <?php $form = ActiveForm::begin([
                                'enableAjaxValidation' => true,
                                'enableClientValidation' => false,
                                'validateOnChange' => true,
                                'validateOnBlur' => true,
                            ]); ?>

                            <?php if ($model->getScenario() === EscalaTrabalhoForm::SCENARIO_CREATE) : ?>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <?= $form->field($model, 'data')->widget(DateControl::className(), [
                                            'type' => DateControl::FORMAT_DATE,
                                            'widgetOptions' => [
                                                'options' => ['placeholder' => 'Informe a data'],
                                                'removeButton' => false,
                                                'pluginOptions' => [
                                                    'autoclose' => true,
                                                    'todayHighlight' => true,
                                                ],
                                            ],
                                        ])->label('<h4>Data</h4>'); ?>
                                    </div>
                                </div>
                            <?php else : ?>
                                <?= Html::activeHiddenInput($model, 'data'); ?>
                            <?php endif; ?>

                            <div class="row">
                                <div class="col-sm-12">
                                    <?= $form->field($model, 'escalasTarde')->widget(MultipleInput::className(), [
                                        'addButtonPosition' => MultipleInput::POS_HEADER,
                                        'allowEmptyList' => true,
                                        'columns' => [
                                            [
                                                'headerOptions' => ['class' => 'col-xs-6'],
                                                'enableError' => true,
                                                'name' => 'funcao_id',
                                                'title' => 'Função',
                                                'type' => Select2::class,
                                                'options' => [
                                                    'data' => ArrayHelper::map($funcoes, 'id', 'nome'),
                                                    'options' => ['placeholder' => 'Selecione uma função...'],
                                                ],
                                            ],
                                            [
                                                'headerOptions' => ['class' => 'col-xs-6'],
                                                'enableError' => true,
                                                'name' => 'membro_id',
                                                'title' => 'Membro',
                                                'type' => Select2::class,
                                                'options' => [
                                                    'data' => ArrayHelper::map($membros, 'id', 'nome'),
                                                    'options' => ['placeholder' => 'Selecione um membro...'],
                                                ],
                                            ],
                                            [
                                                'name' => 'turno',
                                                'type' => \unclead\multipleinput\MultipleInputColumn::TYPE_HIDDEN_INPUT,
                                                'defaultValue' => EscalaTrabalho::TURNO_TARDE,
                                            ],
                                        ],
                                    ])->label(false);
                                    ?>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-sm-12">
                                    <h4>Turno Manhã</h4>
                                </div>

                                <div class="col-sm-12">
                                    <?= $form->field($model, 'escalasManha')->widget(MultipleInput::className(), [
                                        'addButtonPosition' => MultipleInput::POS_HEADER,
                                        'allowEmptyList' => true,
                                        'columns' => [
                                            [
                                                'headerOptions' => ['class' => 'col-xs-6'],
                                                'enableError' => true,
                                                'name' => 'funcao_id',
                                                'title' => 'Função',
                                                'type' => Select2::class,
                                                'options' => [
                                                    'data' => ArrayHelper::map($funcoes, 'id', 'nome'),
                                                    'options' => ['placeholder' => 'Selecione uma função...'],
                                                ],
                                            ],
                                            [
                                                'headerOptions' => ['class' => 'col-xs-6'],
                                                'enableError' => true,
                                                'name' => 'membro_id',
                                                'title' => 'Membro',
                                                'type' => Select2::class,
                                                'options' => [
                                                    'data' => ArrayHelper::map($membros, 'id', 'nome'),
                                                    'options' => ['placeholder' => 'Selecione um membro...'],
                                                ],
                                            ],
                                            [
                                                'name' => 'turno',
                                                'type' => \unclead\multipleinput\MultipleInputColumn::TYPE_HIDDEN_INPUT,
                                                'defaultValue' => EscalaTrabalho::TURNO_MANHA,
                                            ],
                                        ],
                                    ])->label(false);
                                    ?>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-sm-12">
                                    <h4>Turno Noite</h4>
                                </div>

                                <div class="col-sm-12">
                                    <?= $form->field($model, 'escalasNoite')->widget(MultipleInput::className(), [
                                        'addButtonPosition' => MultipleInput::POS_HEADER,
                                        'allowEmptyList' => true,
                                        'columns' => [
                                            [
                                                'headerOptions' => ['class' => 'col-xs-6'],
                                                'enableError' => true,
                                                'name' => 'funcao_id',
                                                'title' => 'Função',
                                                'type' => Select2::class,
                                                'options' => [
                                                    'data' => ArrayHelper::map($funcoes, 'id', 'nome'),
                                                    'options' => ['placeholder' => 'Selecione uma função...'],
                                                ],
                                            ],
                                            [
                                                'headerOptions' => ['class' => 'col-xs-6'],
                                                'enableError' => true,
                                                'name' => 'membro_id',
                                                'title' => 'Membro',
                                                'type' => Select2::class,
                                                'options' => [
                                                    'data' => ArrayHelper::map($membros, 'id', 'nome'),
                                                    'options' => ['placeholder' => 'Selecione um membro...'],
                                                ],
                                            ],
                                            [
                                                'name' => 'turno',
                                                'type' => \unclead\multipleinput\MultipleInputColumn::TYPE_HIDDEN_INPUT,
                                                'defaultValue' => EscalaTrabalho::TURNO_NOITE,
                                            ],
                                        ],
                                    ])->label(false);
                                    ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
                                        <?= Html::a('Voltar', '.', ['class' => 'btn btn-default']) ?>
                                    </div>
                                </div>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
