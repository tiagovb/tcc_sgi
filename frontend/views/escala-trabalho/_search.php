<?php

use common\components\widgets\DateRangePicker;
use common\helpers\ActiveForm;
use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\search\EscalaTrabalhoSearch */
/* @var $form ActiveForm */
?>

<div class="row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'enableClientValidation' => true,
    ]); ?>

    <div class="col-sm-3">
        <?= $form->field($model, 'data')->widget(DateRangePicker::className(), [
            'attribute' => 'data',
            'model' => $model,
        ]) ?>
    </div>

    <div class="col-sm-3">
        <?= $this->render('../layouts/_buttonsSearch') ?>
    </div>

    <div class="col-md-6 text-right">
        <label class="control-label" for="add-button-container">&nbsp;</label>
        <div id="add-button-container">
            <?= Html::a(Html::icon('plus') . ' Cadastrar Escala', ['create'], ['class' => 'btn btn-success']); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
