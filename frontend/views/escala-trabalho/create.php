<?php

/* @var $this yii\web\View */
/* @var $model common\models\EscalaTrabalho */
/* @var $membros common\models\Membro[] */
/* @var $funcoes common\models\Funcao[] */

$this->title = 'Cadastrar Escala de Trabalho';
$this->params['breadcrumbs'][] = ['label' => 'Escalas de Trabalho', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="escala-trabalho-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
