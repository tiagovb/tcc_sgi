<?php

use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

/* @var $this yii\web\View */
/* @var $model \frontend\models\EscalaTrabalhoForm */

$data = Yii::$app->formatter->asDate($model->data);
?>
<div class="row">
    <div class="col-md-12 text-center">
        <h2>Escala de Trabalho para Cultos</h2>
        <h3>Data: <?= $data ?></h3>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= GridView::widget([
            'dataProvider' => new ArrayDataProvider(['allModels' => $model->escalasTarde]),
            'layout' => '{items}',
            'columns' => [
                [
                    'label' => 'Função',
                    'attribute' => 'funcao_id',
                    'value' => function ($model, $key, $index, $widget) {
                        return $model->funcao->nome;
                    },
                    'headerOptions' => ['class' => 'col-xs-6'],
                    'contentOptions' => ['class' => 'col-xs-6'],
                    'group' => true,
                ],
                [
                    'label' => 'Membro',
                    'attribute' => 'membro_id',
                    'value' => function ($model, $key, $index, $widget) {
                        return $model->membro->nome;
                    },
                    'headerOptions' => ['class' => 'col-xs-6'],
                    'contentOptions' => ['class' => 'col-xs-6'],
                ],
            ],
        ]); ?>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-12">
        <h5><strong>Turno Manhã</strong></h5>
        <?= GridView::widget([
            'dataProvider' => new ArrayDataProvider(['allModels' => $model->escalasManha]),
            'layout' => '{items}',
            'columns' => [
                [
                    'label' => 'Função',
                    'attribute' => 'funcao_id',
                    'value' => function ($model, $key, $index, $widget) {
                        return $model->funcao->nome;
                    },
                    'headerOptions' => ['class' => 'col-xs-6'],
                    'contentOptions' => ['class' => 'col-xs-6'],
                    'group' => true,
                ],
                [
                    'label' => 'Membro',
                    'attribute' => 'membro_id',
                    'value' => function ($model, $key, $index, $widget) {
                        return $model->membro->nome;
                    },
                    'headerOptions' => ['class' => 'col-xs-6'],
                    'contentOptions' => ['class' => 'col-xs-6'],
                ],
            ],
        ]); ?>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-12">
        <h5><strong>Turno Noite</strong></h5>

        <?= GridView::widget([
            'dataProvider' => new ArrayDataProvider(['allModels' => $model->escalasNoite]),
            'layout' => '{items}',
            'columns' => [
                [
                    'label' => 'Função',
                    'attribute' => 'funcao_id',
                    'value' => function ($model, $key, $index, $widget) {
                        return $model->funcao->nome;
                    },
                    'headerOptions' => ['class' => 'col-xs-6'],
                    'contentOptions' => ['class' => 'col-xs-6'],
                    'group' => true,
                ],
                [
                    'label' => 'Membro',
                    'attribute' => 'membro_id',
                    'value' => function ($model, $key, $index, $widget) {
                        return $model->membro->nome;
                    },
                    'headerOptions' => ['class' => 'col-xs-6'],
                    'contentOptions' => ['class' => 'col-xs-6'],
                ],
            ],
        ]); ?>
    </div>
</div>

