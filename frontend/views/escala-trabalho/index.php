<?php

use kartik\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\EscalaTrabalhoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consultar Escala de Trabalho';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="escala-trabalho-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'columns' => [
                                    'data:date',
                                    [
                                        'buttons' => [
                                            'view' => function ($url, $model) {
                                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'data' => $model->data]);
                                            },
                                            'update' => function ($url, $model) {
                                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'data' => $model->data]);
                                            },
                                            'delete' => function ($url, $model) {
                                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'data' => $model->data]);
                                            },
                                        ],
                                        'class' => 'yii\grid\ActionColumn'
                                    ],
                                ],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>