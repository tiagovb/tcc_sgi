<?php

use frontend\models\EscalaTrabalhoForm;

/* @var $this yii\web\View */
/* @var $model EscalaTrabalhoForm */

$data = Yii::$app->formatter->asDate($model->data);
$this->title = 'Editar Escala de Trabalho: ' . $data;
$this->params['breadcrumbs'][] = ['label' => 'Escalas de Trabalho', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $data, 'url' => ['view', 'data' => $data]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="escala-trabalho-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
