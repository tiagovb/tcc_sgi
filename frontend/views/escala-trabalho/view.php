<?php

use kartik\helpers\Html;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model \frontend\models\EscalaTrabalhoForm */

$data = Yii::$app->formatter->asDate($model->data);
$this->title = 'Escala de Trabalho: ' . $data;
$this->params['breadcrumbs'][] = ['label' => 'Escala de Trabalho', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$css = <<<CSS
hr{
margin-top: 1px;
margin-bottom: 1px;
}
.table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #979494;
}
.table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > th, .table > caption + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > td, .table > thead:first-child > tr:first-child > td {
    border: 1px solid #979494;
}
CSS;

$this->registerCss($css);
?>
<div class="escala-trabalho-view">
    <div class="row" style="margin-bottom: 5px;">
        <div class="col-md-4">
            <?= Html::a('Alterar', ['update', 'data' => $model->data], ['class' => 'btn btn-primary']) ?>&nbsp;&nbsp;
            <?= Html::a('Imprimir', ['imprimir-relatorio', 'data' => $model->data], ['target' => '_blank', 'class' => 'btn bg-navy']) ?>&nbsp;&nbsp;
            <?= Html::a('Excluir', ['delete', 'id' => $model->data], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Tem certeza que deseja excluir este item?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('Voltar', Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
            &nbsp;&nbsp;
            <?= Html::a(Html::icon('plus') . ' Cadastrar Nova Escala de Trabalho', ['create'], ['class' => 'btn btn-success']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= GridView::widget([
                                'dataProvider' => new ArrayDataProvider(['allModels' => $model->escalasTarde]),
                                'layout' => '{items}',
                                'columns' => [
                                    [
                                        'label' => 'Função',
                                        'attribute' => 'funcao_id',
                                        'value' => function ($model, $key, $index, $widget) {
                                            return $model->funcao->nome;
                                        },
                                        'headerOptions' => ['class' => 'col-xs-6'],
                                        'contentOptions' => ['class' => 'col-xs-6'],
                                    ],
                                    [
                                        'label' => 'Membro',
                                        'attribute' => 'membro_id',
                                        'value' => function ($model, $key, $index, $widget) {
                                            return $model->membro->nome;
                                        },
                                        'headerOptions' => ['class' => 'col-xs-6'],
                                        'contentOptions' => ['class' => 'col-xs-6'],
                                    ],
                                ],
                            ]); ?>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <h5><strong>Turno Manhã</strong></h5>
                            <?= GridView::widget([
                                'dataProvider' => new ArrayDataProvider(['allModels' => $model->escalasManha]),
                                'layout' => '{items}',
                                'columns' => [
                                    [
                                        'label' => 'Função',
                                        'attribute' => 'funcao_id',
                                        'value' => function ($model, $key, $index, $widget) {
                                            return $model->funcao->nome;
                                        },
                                        'headerOptions' => ['class' => 'col-xs-6'],
                                        'contentOptions' => ['class' => 'col-xs-6'],
                                    ],
                                    [
                                        'label' => 'Membro',
                                        'attribute' => 'membro_id',
                                        'value' => function ($model, $key, $index, $widget) {
                                            return $model->membro->nome;
                                        },
                                        'headerOptions' => ['class' => 'col-xs-6'],
                                        'contentOptions' => ['class' => 'col-xs-6'],
                                    ],
                                ],
                            ]); ?>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <h5><strong>Turno Noite</strong></h5>

                            <?= GridView::widget([
                                'dataProvider' => new ArrayDataProvider(['allModels' => $model->escalasNoite]),
                                'layout' => '{items}',
                                'columns' => [
                                    [
                                        'label' => 'Função',
                                        'attribute' => 'funcao_id',
                                        'value' => function ($model, $key, $index, $widget) {
                                            return $model->funcao->nome;
                                        },
                                        'headerOptions' => ['class' => 'col-xs-6'],
                                        'contentOptions' => ['class' => 'col-xs-6'],
                                    ],
                                    [
                                        'label' => 'Membro',
                                        'attribute' => 'membro_id',
                                        'value' => function ($model, $key, $index, $widget) {
                                            return $model->membro->nome;
                                        },
                                        'headerOptions' => ['class' => 'col-xs-6'],
                                        'contentOptions' => ['class' => 'col-xs-6'],
                                    ],
                                ],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

