<?php

use common\components\enum\EnumSexo;
use common\components\enum\EnumUf;
use common\helpers\ActiveForm;
use common\models\FamiliaBeneficiada;
use common\models\Membro;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model common\models\FamiliaBeneficiada */
/* @var $dataProvider \yii\data\ActiveDataProvider */

?>
<div class="box box-<?= $model->colorTypeBox() ?>">
    <div class="box-body">
        <div class="familia-beneficiada-form">
            <div class="row">
                <div class="col-sm-12">
                    <?php $form = ActiveForm::begin(); ?>

                    <div class="row">
                        <div class="col-sm-6">
                            <?= $form->field($model, 'nome')->textInput() ?>
                        </div>


                        <div class="col-sm-2">
                            <?= $form->field($model, 'sexo')->radioList([EnumSexo::SEXO_MASCULINO => 'Masc.', EnumSexo::SEXO_FEMININO => 'Fem.'], ['inline' => true]) ?>
                        </div>

                        <div class="col-sm-2">
                            <?= $form->field($model, 'cpf')->cpf() ?>
                        </div>

                        <div class="col-sm-2">
                            <?= $form->field($model, 'estadoCivil')->drop(Membro::getEnumListLabels('estadoCivil')) ?>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <?= $form->field($model, 'dtNascimento')->widget(DateControl::className(), [
                                'type' => DateControl::FORMAT_DATE,
                                'widgetOptions' => [
                                    'removeButton' => false,
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                    ],
                                ],
                            ]); ?>
                        </div>

                        <div class="col-sm-3">
                            <?= $form->field($model, 'telefone')->telefoneFixo() ?>
                        </div>

                        <div class="col-sm-3">
                            <?= $form->field($model, 'celular')->celular() ?>
                        </div>

                        <div class="col-sm-3">
                            <?= $form->field($model, 'necessidadeAuxilio')->drop(FamiliaBeneficiada::getEnumListLabels('necessidadeAuxilio')) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'observacoesAuxilio')->textArea() ?>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-3">
                            <?= $form->field($model, 'logradouro')->textInput() ?>
                        </div>

                        <div class="col-sm-1">
                            <?= $form->field($model, 'numero')->textInput() ?>
                        </div>

                        <div class="col-sm-2">
                            <?= $form->field($model, 'complemento')->textInput() ?>
                        </div>

                        <div class="col-sm-2">
                            <?= $form->field($model, 'cep')->cep() ?>
                        </div>

                        <div class="col-sm-2">

                            <?= $form->field($model, 'cidade')->textInput() ?>
                        </div>

                        <div class="col-sm-2">

                            <?= $form->field($model, 'estado')->dropDownList(EnumUf::$listEnumLabels) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <?= $this->render('../layouts/_buttonsForm', ['model' => $model]) ?>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
