<?php

use common\helpers\ActiveForm;
use common\models\FamiliaBeneficiada;
use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\search\FamiliaBeneficiadaSearch */
/* @var $form ActiveForm */
$css = <<<CSS
.padding-5{
padding-left: 5px;
padding-right: 5px;
}
CSS;

$this->registerCss($css);?>

<div class="row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-sm-3" style="padding-right: 5px;">
        <?= $form->field($model, 'nome') ?>
    </div>

    <div class="col-sm-2 padding-5">
        <?= $form->field($model, 'cpf')->cpf() ?>
    </div>

    <div class="col-sm-2 padding-5">
        <?= $form->field($model, 'necessidadeAuxilio')->drop(FamiliaBeneficiada::getEnumListLabels('necessidadeAuxilio')) ?>
    </div>

    <div class="col-xs-6 col-md-3 padding-5">
        <?= $this->render('../layouts/_buttonsSearch') ?>
    </div>

    <div class="col-md-2 col-xs-6 text-right" style="padding-left: 0;">
        <label class="control-label" for="add-button-container">&nbsp;</label>
        <div id="add-button-container">
            <?= Html::a(Html::icon('plus') . ' Cadastrar Família', ['create'], ['class' => 'btn btn-success']); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
