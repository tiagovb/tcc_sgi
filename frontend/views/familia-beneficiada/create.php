<?php

/* @var $this yii\web\View */
/* @var $model common\models\FamiliaBeneficiada */

$this->title = 'Cadastrar Familia Beneficiada';
$this->params['breadcrumbs'][] = ['label' => 'Familia Beneficiadas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="familia-beneficiada-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
