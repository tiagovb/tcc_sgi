<?php

use common\models\FamiliaBeneficiada;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\FamiliaBeneficiadaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Relatório de Famílias Beneficiadas';
$this->params['breadcrumbs'][] = $this->title;
$dataProvider->setPagination(false);
?>

<?= GridView::widget([
    'layout' => '{items}',
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'nome',
            'headerOptions' => ['class' => 'col-xs-4'],
            'contentOptions' => ['class' => 'col-xs-4'],
        ],
        'telefone',
        'celular',
        [
            'attribute' => 'necessidadeAuxilio',
            'value' => function ($model) {
                return FamiliaBeneficiada::getEnumLabel('necessidadeAuxilio', $model->necessidadeAuxilio);
            }
        ],
    ],
]); ?>
