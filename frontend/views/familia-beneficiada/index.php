<?php

use common\models\FamiliaBeneficiada;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\FamiliaBeneficiadaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consultar Família Beneficiada';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="familia-beneficiada-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                </div>


                    <div class="box-body">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                'id',
                                [
                                    'attribute' => 'nome',
                                    'headerOptions' => ['class' => 'col-xs-4'],
                                    'contentOptions' => ['class' => 'col-xs-4'],
                                ],
                                'cpf:cpf',
                                'celular',
                                [
                                    'attribute' => 'necessidadeAuxilio',
                                    'value' => function ($model) {
                                        return FamiliaBeneficiada::getEnumLabel('necessidadeAuxilio', $model->necessidadeAuxilio);
                                    }
                                ],
                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </div>

            </div>
        </div>
    </div>
</div>