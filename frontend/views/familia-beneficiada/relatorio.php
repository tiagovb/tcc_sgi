<?php

use common\helpers\ActiveForm;
use common\models\FamiliaBeneficiada;
use kartik\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\FamiliaBeneficiadaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $queryParams array */

$this->title = 'Relatório de Famílias Beneficiadas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="FamiliaBeneficiada-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="row">

                        <?php $form = ActiveForm::begin([
                            'method' => 'get',
                        ]); ?>
                        <div class="col-sm-4">
                            <?= $form->field($searchModel, 'nome') ?>
                        </div>

                        <div class="col-sm-2">
                            <?= $form->field($searchModel, 'necessidadeAuxilio')->drop(FamiliaBeneficiada::getEnumListLabels('necessidadeAuxilio')) ?>
                        </div>

                        <div class="col-sm-3">
                            <?= $this->render('../layouts/_buttonsSearch') ?>
                        </div>

                        <div class="col-sm-3 text-right">
                            <?php if (!empty($dataProvider->models)) : ?>

                                <label class="control-label" for="buttons-container">&nbsp;</label>
                                <div id="buttons-container">
                                    <?= Html::a('Imprimir', ['imprimir-relatorio', $queryParams], ['target' => '_blank', 'class' => 'btn bg-navy btn-md']) ?>
                                </div>

                            <?php endif; ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>


                    <div class="box-body">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [

                                'id',
                                [
                                    'attribute' => 'nome',
                                    'headerOptions' => ['class' => 'col-xs-4'],
                                    'contentOptions' => ['class' => 'col-xs-4'],
                                ],
                                'telefone',
                                'celular',
                                [
                                    'attribute' => 'necessidadeAuxilio',
                                    'value' => function ($model) {
                                        return FamiliaBeneficiada::getEnumLabel('necessidadeAuxilio', $model->necessidadeAuxilio);
                                    }
                                ],

                                ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
                            ],
                        ]); ?>
                    </div>

            </div>
        </div>
    </div>

</div>
