<?php


/* @var $this yii\web\View */
/* @var $model common\models\FamiliaBeneficiada */

$this->title = 'Edtiar Família Beneficiada - Responsável: ' . $model->nome;
$this->params['breadcrumbs'][] = ['label' => 'Familia Beneficiadas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="familia-beneficiada-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
