<?php

use common\assets\DependenteAsset;
use common\models\Dependente;
use common\models\FamiliaBeneficiada;
use kartik\helpers\Html;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FamiliaBeneficiada */
/* @var $depentendesProvider */

$this->title = 'Família Beneficiada - Responsável: ' . $model->nome;
$this->params['breadcrumbs'][] = ['label' => 'Famílias Beneficiadas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
DependenteAsset::register($this);
$this->registerJs('Dependente.init(' . $model->id . ');');
?>
<div class="familia-beneficiada-view">
    <div class="row" style="margin-bottom: 5px;">
        <div class="col-md-6">
            <?= Html::button(Html::icon('plus') . ' Cadastrar Dependente', ['class' => 'btn btn-info', 'id' => 'btn-create-dependente']); ?>&nbsp;&nbsp;
            <?= Html::a('Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>&nbsp;&nbsp;
            <?= Html::a('Excluir', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Tem certeza que deseja excluir este item?',
                    'method' => 'post',
                ],
            ]) ?>
            &nbsp;&nbsp;
            <?= Html::a(Html::icon('plus') . ' Cadastrar Nova Família Beneficiada', ['create'], ['class' => 'btn btn-success']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h4>Dados Cadastrais</h4>
                </div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'nome',
                            'sexo:sexo',
                            'cpf:cpf',
                            'estadoCivil:estadoCivil',
                            [
                                'attribute' => 'telefone',
                                'visible' => !empty($model->telefone),
                            ],
                            'telefone:telefone',
                            'celular:telefone',
                            [
                                'attribute' => 'necessidadeAuxilio',
                                'value' => FamiliaBeneficiada::getEnumLabel('necessidadeAuxilio', $model->necessidadeAuxilio),
                            ],
                            'observacoesAuxilio:ntext',
                            'logradouro',
                            'numero',
                            'complemento',
                            'cep',
                            'cidade',
                            'estado:uf',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h4>Dependentes</h4>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <?= /** @noinspection PhpUnusedParameterInspection */
                                    GridView::widget([
                                        'dataProvider' => $depentendesProvider,
                                        'columns' => [
                                            'nome',
                                            'dtNascimento:date',
                                            [
                                                'attribute' => 'religiao',
                                                'value' => function ($model) {
                                                    return Dependente::getEnumLabel('religiao', $model->religiao);
                                                }
                                            ],
                                            [
                                                'class' => ActionColumn::class,
                                                'headerOptions' => ['class' => 'col-xs-1'],
                                                'buttons' => [
                                                    'update' => function ($url, $model) {
                                                        return Html::a(Html::icon('pencil'), 'javascript:void(0);', [
                                                            'class' => 'btn-update-dependente',
                                                            'data-dependente-id' => $model->id,
                                                        ]);
                                                    },
                                                    'delete' => function ($url, $model) {
                                                        /** @var \common\models\Dependente $model */
                                                        return Html::a(Html::icon('trash'), ['/dependente/delete', 'dependenteId' => $model->id], [
                                                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                                            'data-method' => 'post',
                                                        ]);
                                                    },
                                                ],
                                                'template' => '{update} {delete}',
                                            ],
                                        ],
                                    ]); ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
