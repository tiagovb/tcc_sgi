<?php

use common\helpers\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Funcao */
/* @var $form ActiveForm */
?>
<div class="box box-<?= $model->colorTypeBox() ?>">
    <div class="box-body">
        <div class="funcao-form">
            <div class="row">
                <div class="col-sm-6">

                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'descricao')->textarea(['rows' => 2]) ?>

                    <?= $this->render('../layouts/_buttonsForm', ['model' => $model]) ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>

