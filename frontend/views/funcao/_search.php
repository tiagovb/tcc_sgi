<?php

use common\helpers\ActiveForm;
use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\search\FuncaoSearch */
/* @var $form ActiveForm */
?>
<div class="row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-sm-3">
        <?= $form->field($model, 'nome') ?>
    </div>

    <div class="col-sm-3">
        <?= $form->field($model, 'descricao') ?>
    </div>

    <div class="col-sm-3">
        <?= $this->render('../layouts/_buttonsSearch') ?>
    </div>

    <div class="col-md-3 text-right">
        <label class="control-label" for="add-button-container">&nbsp;</label>
        <div id="add-button-container">
            <?= Html::a(Html::icon('plus') . ' Cadastrar Função', ['create'], ['class' => 'btn btn-success']); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>