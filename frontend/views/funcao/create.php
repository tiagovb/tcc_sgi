<?php

/* @var $this yii\web\View */
/* @var $model common\models\Funcao */

$this->title = 'Cadastrar Função';
$this->params['breadcrumbs'][] = ['label' => 'Funções', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="funcao-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
