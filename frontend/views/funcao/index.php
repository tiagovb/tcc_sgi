<?php

use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\FuncaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consultar Função de Trabalho';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="funcao-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                </div>

                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            'id',
                            'nome',
                            'descricao:text',
                            [
                                'class' => ActionColumn::class,
                                'visibleButtons' => [
                                    'delete' => function ($model) {
                                        /** @var \common\models\Funcao $model */
                                        return $model->canDelete();
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>

            </div>
        </div>
    </div>
</div>
