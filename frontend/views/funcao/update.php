<?php

/* @var $this yii\web\View */
/* @var $model common\models\Funcao */

$this->title = 'Editar Função: ' . $model->nome;
$this->params['breadcrumbs'][] = ['label' => 'Funções de Trabalho', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="funcao-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
