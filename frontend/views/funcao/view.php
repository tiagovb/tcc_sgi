<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Funcao */

$this->title = 'Função: ' . $model->nome;
$this->params['breadcrumbs'][] = ['label' => 'Funções de Trabalho', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$avisoExclusao = '';
$disableClass = '';

if (!$model->canDelete()) {
    $avisoExclusao = 'Esta função não pode ser excluida pois possui escala de trabalho vinculada.';
    $disableClass = 'disabled';
}
$this->registerJs('$(document).ready(function(){
    $(\'[data-toggle="tooltip"]\').tooltip(); 
});')
?>
<div class="funcao-view">
    <div class="row" style="margin-bottom: 5px;">
        <div class="col-md-6">
            <?= Html::a('Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>&nbsp;&nbsp;
            <span data-toggle="tooltip" data-title="<?= $avisoExclusao ?>">
                <?= Html::a('Excluir', ['delete', 'id' => $model->id], [
                    'class' => "btn btn-danger {$disableClass}",
                    'data' => [
                        'confirm' => 'Tem certeza que deseja excluir este item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </span>
            &nbsp;&nbsp;
            <?= Html::a(Html::icon('plus') . ' Cadastrar Nova Função', ['create'], ['class' => 'btn btn-success']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">

                    <div class="row">
                        <div class="col-md-6">
                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'id',
                                    'nome',
                                    'descricao:ntext',
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
