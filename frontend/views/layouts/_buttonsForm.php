<?php

use kartik\helpers\Html;

/* @var $model common\models\MainModel */
?>

<div class="form-group">
    <?= Html::submitButton('Salvar', ['class' => "btn btn-{$model->colorTypeBox()}"]) ?>
    <?= Html::a('Voltar', Yii::$app->request->referrer, ['class' => 'btn btn-default']) ?>
</div>

