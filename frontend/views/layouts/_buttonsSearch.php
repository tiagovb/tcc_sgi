<?php

use kartik\helpers\Html;

?>

<label class="control-label" for="buttons-container">&nbsp;</label>
<div id="buttons-container">
    <?= Html::submitButton('Consultar', ['class' => 'btn btn-primary btn-md']) ?>
    <?= Html::a('Limpar', [Yii::$app->request->getPathInfo()], ['class' => 'btn btn-default btn-md']) ?>
</div>