<aside class="main-sidebar">

    <section class="sidebar">

        <?= \common\components\menu\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => \frontend\models\MenuItems::create()
            ]
        ) ?>

    </section>

</aside>
