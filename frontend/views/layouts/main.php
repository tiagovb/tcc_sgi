<?php

use kartik\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */


if (Yii::$app->controller->action->id === 'login') {
    /**
     * Do not use this code in your template. Remove it.
     * Instead, use the code  $this->layout = '//main-login'; in your controller.
     */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    frontend\assets\AppAsset::register($this);

    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="mobile-web-app-capable" content="yes">
        <link rel="manifest" href="/manifest.json">
		<link rel="stylesheet" type="text/css" href="/css/addtohomescreen.css">
		<script src="/js/addtohomescreen.js"></script>
        <!-- icon in the highest resolution we need it for -->
        <link rel="icon" sizes="192x192" href="img/Logo-ICBMP-192x192.png">
        <!-- multiple icons for IE -->
        <meta name="msapplication-square192x192logo" content="img/Logo-ICBMP-192x192.png">
		<script>
            if ('serviceWorker' in navigator) {
                navigator.serviceWorker.register('/js/sw.js', { scope: '/js/' }).then(function(reg) {

                    if(reg.installing) {
                        console.log('Service worker installing');
                    } else if(reg.waiting) {
                        console.log('Service worker installed');
                    } else if(reg.active) {
                        console.log('Service worker active');
                    }

                }).catch(function(error) {
                    // registration failed
                    console.log('Registration failed with ' + error);
                });
            }
        </script>
        <meta name="theme-color" content="#367fa9">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
    <?php $this->beginBody() ?>
    <div class="wrapper">

        <?= $this->render(
            'header.php',
            ['directoryAsset' => $directoryAsset]
        ) ?>

        <?= $this->render(
            'left.php',
            ['directoryAsset' => $directoryAsset]
        )
        ?>

        <?= $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>

    </div>

    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
