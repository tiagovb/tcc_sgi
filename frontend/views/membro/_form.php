<?php

use common\components\enum\EnumSexo;
use common\components\enum\EnumUf;
use common\helpers\ActiveForm;
use common\models\Membro;
use kartik\datecontrol\DateControl;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Membro */
/* @var $dataProvider \yii\data\ActiveDataProvider */

$membros = Membro::find()->select(['id', 'nome'])->asArray()->all();
?>

<div class="box box-<?= $model->colorTypeBox() ?>">
    <div class="box-body">
        <div class="membro-form">
            <div class="row">
                <div class="col-sm-12">

                    <?php $form = ActiveForm::begin(); ?>

                    <div class="row">
                        <div class="col-sm-6">
                            <?= $form->field($model, 'nome')->textInput(['class' => 'input-sm']) ?>
                        </div>

                        <div class="col-sm-2">
                            <?= $form->field($model, 'sexo')->radioList([EnumSexo::SEXO_MASCULINO => 'Masc.', EnumSexo::SEXO_FEMININO => 'Fem.'], ['inline' => true]) ?>
                        </div>

                        <div class="col-sm-2">
                            <?= $form->field($model, 'dtNascimento')->widget(DateControl::className(), [
                                'type' => DateControl::FORMAT_DATE,
                                'widgetOptions' => [
                                    'size' => 'sm',
                                    'removeButton' => false,
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'minuteStep' => 10,
                                        'todayHighlight' => true,
                                    ],
                                ],
                            ]); ?>
                        </div>

                        <div class="col-sm-2">
                            <?= $form->field($model, 'estadoCivil', ['addClass' => 'form-control input-sm'])->drop(Membro::getEnumListLabels('estadoCivil')) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <?= $form->field($model, 'nomeMae')->textInput(['class' => 'input-sm']) ?>
                        </div>

                        <div class="col-sm-3">
                            <?= $form->field($model, 'nomePai')->textInput(['class' => 'input-sm']) ?>
                        </div>

                        <div class="col-sm-3">
                            <?= $form->field($model, 'conjuge_id')->widget(Select2::className(), [
                                'data' => ArrayHelper::map($membros, 'id', 'nome'),
                                'options' => ['placeholder' => 'Selecione um membro...'],
                                'size' => Select2::SMALL,
                            ]) ?>
                        </div>


                        <div class="col-sm-3">
                            <?= $form->field($model, 'dtCasamento')->widget(DateControl::className(), [
                                'type' => DateControl::FORMAT_DATE,
                                'widgetOptions' => [
                                    'size' => 'sm',
                                    'removeButton' => false,
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'minuteStep' => 10,
                                        'todayHighlight' => true,
                                    ],
                                ],
                            ]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-2">
                            <?= $form->field($model, 'rg')->textInput(['class' => 'input-sm']) ?>
                        </div>

                        <div class="col-sm-2">
                            <?= $form->field($model, 'cpf')->cpf(['class' => 'input-sm form-control']) ?>
                        </div>
                        <div class="col-sm-2">
                            <?= $form->field($model, 'telefone')->telefoneFixo(['class' => 'input-sm form-control']) ?>
                        </div>

                        <div class="col-sm-2">
                            <?= $form->field($model, 'celular')->celular(['class' => 'input-sm form-control']) ?>
                        </div>

                        <div class="col-sm-4">
                            <?= $form->field($model, 'email')->textInput(['class' => 'input-sm']) ?>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-2">
                            <?= $form->field($model, 'naturalidade')->textInput(['class' => 'input-sm']) ?>
                        </div>

                        <div class="col-sm-2">
                            <?= $form->field($model, 'nivelEscolar', ['addClass' => 'form-control input-sm'])->drop(Membro::getEnumListLabels('nivelEscolar')) ?>
                        </div>

                        <div class="col-sm-2">
                            <?= $form->field($model, 'status', ['addClass' => 'form-control input-sm'])->dropDownList(Membro::getEnumListLabels('status')) ?>
                        </div>

                        <div class="col-sm-6">
                            <?= $form->field($model, 'observacoes')->textInput(['class' => 'input-sm']) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <?= $form->field($model, 'logradouro')->textInput(['class' => 'input-sm']) ?>
                        </div>

                        <div class="col-sm-1">
                            <?= $form->field($model, 'numero')->textInput(['class' => 'input-sm']) ?>
                        </div>

                        <div class="col-sm-2">
                            <?= $form->field($model, 'complemento')->textInput(['class' => 'input-sm']) ?>
                        </div>

                        <div class="col-sm-2">

                            <?= $form->field($model, 'cep')->cep(['class' => 'form-control input-sm']) ?>
                        </div>

                        <div class="col-sm-2">

                            <?= $form->field($model, 'cidade')->textInput(['class' => 'input-sm']) ?>
                        </div>

                        <div class="col-sm-2">

                            <?= $form->field($model, 'estado', ['addClass' => 'form-control input-sm'])->dropDownList(EnumUf::$listEnumLabels) ?>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <?= $this->render('../layouts/_buttonsForm', ['model' => $model]) ?>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

