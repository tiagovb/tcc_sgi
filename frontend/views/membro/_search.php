<?php

use common\helpers\ActiveForm;
use common\models\Membro;
use kartik\helpers\Html;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\search\MembroSearch */
/* @var $form ActiveForm */

$css = <<<CSS
.padding-5{
padding-left: 5px;
padding-right: 5px;
}
CSS;

$this->registerCss($css);
?>

<div class="row">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-sm-3" style="padding-right: 5px;">
        <?= $form->field($model, 'nome') ?>
    </div>

    <div class="col-sm-2">
        <?= $form->field($model, 'mesNascimento')->widget(DatePicker::className(), [
//                                'layout' => '{input}{picker}',
            'pluginOptions' => [
                'autoclose' => true,
                'startView' => 'months',
                'minViewMode' => 'months',
                'format' => 'mm'
            ],
        ])->label('Mês de Aniversário') ?>
    </div>

    <div class="col-sm-2 padding-5">
        <?= $form->field($model, 'status')->drop(Membro::getEnumListLabels('status')) ?>
    </div>

    <div class="col-xs-4 col-md-3 padding-5">
        <?= $this->render('../layouts/_buttonsSearch') ?>
    </div>

    <div class="col-md-2 col-xs-6 text-right" style="padding-left: 0;">
        <label class="control-label" for="add-button-container">&nbsp;</label>
        <div id="add-button-container">
            <?= Html::a(Html::icon('plus') . ' Cadastrar Membro', ['create'], ['class' => 'btn btn-success']); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>