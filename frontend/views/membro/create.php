<?php

/* @var $this yii\web\View */
/* @var $model common\models\Membro */
/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = 'Cadastrar Membro';
$this->params['breadcrumbs'][] = ['label' => 'Membros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="membro-create">

    <?= $this->render('_form', [
        'model' => $model,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
