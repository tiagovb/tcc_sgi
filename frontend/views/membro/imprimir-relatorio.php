<?php

use common\models\Membro;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\MembroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Relatório de Membro';
$this->params['breadcrumbs'][] = $this->title;
$dataProvider->setPagination(false);
?>

<?= GridView::widget([
    'layout' => '{items}',
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'nome',
            'headerOptions' => ['class' => 'col-xs-4'],
            'contentOptions' => ['class' => 'col-xs-4'],
        ],
        'telefone',
        'celular',
        'nivelEscolar:nivelEscolar',
        'dtNascimento:date',
        'estadoCivil:estadoCivil',
        [
            'attribute' => 'status',
            'value' => function ($model) {
                return Membro::getEnumLabel('statusAbrev', $model->status);
            },
        ],
    ],
]); ?>
