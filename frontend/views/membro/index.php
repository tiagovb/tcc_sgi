<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\MembroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consultar Membro';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="membro-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                </div>


                    <div class="box-body">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                'id',
                                'nome',
                                'cpf:cpf',
                                'dtCadastro:date',
                                'status:status',
                                ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {update}'],
                            ],
                        ]); ?>
                    </div>

            </div>
        </div>
    </div>
</div>