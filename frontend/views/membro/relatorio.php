<?php

use common\components\widgets\DateRangePicker;
use common\helpers\ActiveForm;
use common\models\Membro;
use kartik\helpers\Html;
use kartik\widgets\DatePicker;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\MembroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $queryParams array */

$this->title = 'Relatório de Membros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="membro-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="row">

                        <?php $form = ActiveForm::begin([
                            'method' => 'get',
                        ]); ?>

                        <div class="col-sm-2">
                            <?= $form->field($searchModel, 'mesNascimento')->widget(DatePicker::className(), [
//                                'layout' => '{input}{picker}',
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'startView' => 'months',
                                    'minViewMode' => 'months',
                                    'format' => 'mm'
                                ],
                            ])->label('Mês de Aniversário') ?>
                        </div>

                        <div class="col-sm-2">
                            <?= $form->field($searchModel, 'dtCadastro')->widget(DateRangePicker::className(), [
                                'attribute' => 'dtCadastro',
                                'model' => $searchModel,
                            ]) ?>
                        </div>

                        <div class="col-sm-3">
                            <?= $form->field($searchModel, 'status')->drop(Membro::getEnumListLabels('status')) ?>
                        </div>

                        <div class="col-sm-3">
                            <?= $this->render('../layouts/_buttonsSearch') ?>
                        </div>

                        <div class="col-sm-2 text-right">
                            <?php if (!empty($dataProvider->models)) : ?>

                                <label class="control-label" for="buttons-container">&nbsp;</label>
                                <div id="buttons-container">
                                    <?= Html::a('Imprimir', ['imprimir-relatorio', $queryParams], ['target' => '_blank', 'class' => 'btn bg-navy btn-md']) ?>
                                </div>

                            <?php endif; ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>

                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [

                            'id',
                            'nome',
                            'cpf:cpf',
                            'dtCadastro:date',
                            'status:status',

                            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
                        ],
                    ]); ?>
                </div>

            </div>
        </div>
    </div>

</div>
