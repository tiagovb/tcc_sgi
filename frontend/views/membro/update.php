<?php

/* @var $this yii\web\View */
/* @var $model common\models\Membro */

$this->title = 'Editar Membro: ' . $model->nome;
$this->params['breadcrumbs'][] = ['label' => 'Membros', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="membro-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
