<?php

use common\helpers\ActiveForm;
use common\models\Patrimonio;
use kartik\datecontrol\DateControl;
use kartik\money\MaskMoney;

/* @var $this yii\web\View */
/* @var $model common\models\Patrimonio */
/* @var $form ActiveForm */
?>

<div class="box box-<?= $model->colorTypeBox() ?>">
    <div class="box-body">
        <div class="patrimonio-form">
            <div class="row">
                <div class="col-sm-6">
                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'descricao')->textarea(['rows' => 3, 'maxlenght' => true]) ?>

                    <?= $form->field($model, 'dtAquisicao')->widget(DateControl::className(), [
                        'type' => DateControl::FORMAT_DATE,
                        'widgetOptions' => [
                            'options' => ['placeholder' => 'Informe a data de aquisição...'],
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'todayHighlight' => true,
                            ],
                        ],
                    ]); ?>

                    <?= $form->field($model, 'tipoAquisicao')->drop(Patrimonio::getEnumListLabels('aquisicao')) ?>

                    <?= $form->field($model, 'valor')->widget(MaskMoney::className(), [
                        'pluginOptions' => [
                            'prefix' => 'R$',
                            'allowNegative' => false,
                            'thousands' => '.',
                            'decimal' => ',',
                            'precision' => 2,
                        ],
                    ]) ?>

                    <?= $form->field($model, 'observacoes')->textarea(['rows' => 3, 'maxlenght' => true]) ?>

                    <?= $form->field($model, 'notaFiscal')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'status')->dropDownList(Patrimonio::getEnumListLabels('status')) ?>

                    <?= $this->render('../layouts/_buttonsForm', ['model' => $model]) ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
