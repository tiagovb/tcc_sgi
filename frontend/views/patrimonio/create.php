<?php



/* @var $this yii\web\View */
/* @var $model common\models\Patrimonio */

$this->title = 'Cadastrar Patrimonio';
$this->params['breadcrumbs'][] = ['label' => 'Patrimonios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patrimonio-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
