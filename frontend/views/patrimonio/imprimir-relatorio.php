<?php

use common\models\Patrimonio;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PatrimonioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Relatório de Patrimônio';
$this->params['breadcrumbs'][] = $this->title;
$dataProvider->setPagination(false);
?>
<?= GridView::widget([
    'layout' => '{items}',
    'dataProvider' => $dataProvider,
    'columns' => [
        'descricao:ntext',
        [
            'attribute' => 'dtAquisicao',
            'label' => 'Dt. Aquisição',
            'format' => 'date',
        ],
        'valor:currency',
        [
            'attribute' => 'tipoAquisicao',
            'label' => 'Tipo',
            'value' => function ($model) {
                return Patrimonio::getEnumLabel('aquisicao', $model->tipoAquisicao);
            }
        ],
        [
            'attribute' => 'status',
            'value' => function ($model) {
                return Patrimonio::getEnumLabel('status', $model->status);
            }
        ],
        'notaFiscal',
    ],
]); ?>
