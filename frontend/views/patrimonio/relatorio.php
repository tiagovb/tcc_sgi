<?php

use common\components\widgets\DateRangePicker;
use common\helpers\ActiveForm;
use common\models\Patrimonio;
use kartik\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PatrimonioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $queryParams array */

$this->title = 'Relatório de Patrimônio';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patrimonio-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="row">

                        <?php $form = ActiveForm::begin([
                            'method' => 'get',
                        ]); ?>


                        <div class="col-sm-2">
                            <?= $form->field($searchModel, 'dtAquisicao')->widget(DateRangePicker::className(), [
                                'attribute' => 'dtRecebimento',
                                'model' => $searchModel,
                                'pluginEvents' => false,
                            ]) ?>
                        </div>


                        <div class="col-sm-2">
                            <?= $form->field($searchModel, 'tipoAquisicao')->drop(Patrimonio::getEnumListLabels('aquisicao')) ?>
                        </div>

                        <div class="col-sm-2">
                            <?= $form->field($searchModel, 'status')->drop(Patrimonio::getEnumListLabels('status')) ?>
                        </div>

                        <?php // echo $form->field($model, 'observacoes') ?>

                        <?php // echo $form->field($model, 'status') ?>

                        <?php // echo $form->field($model, 'notaFiscal') ?>

                        <div class="col-sm-3">
                            <?= $this->render('../layouts/_buttonsSearch') ?>
                        </div>

                        <div class="col-sm-3 text-right">
                            <?php if (!empty($dataProvider->models)) : ?>

                                <label class="control-label" for="buttons-container">&nbsp;</label>
                                <div id="buttons-container">
                                    <?= Html::a('Imprimir', ['imprimir-relatorio', $queryParams], ['target' => '_blank', 'class' => 'btn bg-navy btn-md']) ?>
                                </div>

                            <?php endif; ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>


                    <div class="box-body">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [

                                'id',
                                'descricao:ntext',
                                'dtAquisicao:date',
                                'valor:currency',
                                [
                                    'attribute' => 'tipoAquisicao',
                                    'value' => function ($model) {
                                        return Patrimonio::getEnumLabel('aquisicao', $model->tipoAquisicao);
                                    }
                                ],
                                [
                                    'attribute' => 'status',
                                    'value' => function ($model) {
                                        return Patrimonio::getEnumLabel('status', $model->status);
                                    }
                                ],
                                'notaFiscal',
                                'observacoes:ntext',

                                ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
                            ],
                        ]); ?>
                    </div>

            </div>
        </div>
    </div>

</div>
