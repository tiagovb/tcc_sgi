<?php

/* @var $this yii\web\View */
/* @var $model common\models\Patrimonio */

$this->title = 'Editar Patrimonio: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Patrimonios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="patrimonio-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
