<?php

use kartik\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Patrimonio */

$this->title = 'Patrimônio: ' . $model->descricao;
$this->params['breadcrumbs'][] = ['label' => 'Patrimônios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patrimonio-view">

    <div class="row" style="margin-bottom: 5px;">
        <div class="col-md-6">
            <?= Html::a('Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>&nbsp;&nbsp;
            <?= Html::a('Excluir', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Tem certeza que deseja excluir este item?',
                    'method' => 'post',
                ],
            ]) ?>
            &nbsp;&nbsp;
            <?= Html::a(Html::icon('plus') . ' Cadastrar Novo Patrimônio', ['create'], ['class' => 'btn btn-success']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'id',
                                    'descricao:text',
                                    'dtAquisicao:date',
                                    'valor:currency',
                                    [
                                        'attribute' => 'tipoAquisicao',
                                        'value' => \common\models\Patrimonio::getEnumLabel('aquisicao', $model->tipoAquisicao),
                                    ],
                                    [
                                        'attribute' => 'status',
                                        'value' => \common\models\Patrimonio::getEnumLabel('status', $model->status),
                                    ],
                                    'notaFiscal',
                                    'observacoes',
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
