<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $exception Exception */

$this->title = "Ocorreu um erro ='(";
?>
<section class="content">

    <div class="error-page">
        <h2 class="headline text-info"><i class="fa fa-warning text-yellow"></i></h2>

        <div class="error-content">
            <h3><?= $this->title ?></h3>

            <p>
                <a href='<?= Yii::$app->homeUrl ?>'>Página Principal</a>
            </p>

        </div>
    </div>

</section>
