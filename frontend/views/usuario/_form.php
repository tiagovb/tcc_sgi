<?php

use common\helpers\ActiveForm;
use common\models\Usuario;

/* @var $this yii\web\View */
/* @var $model common\models\Usuario */
/* @var $form ActiveForm */

?>

<div class="box box-<?= $model->colorTypeBox() ?>">
    <div class="box-body">
        <div class="user-form">
            <div class="row">
                <div class="col-sm-4 form-usuario-padding">
                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'login')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'tipoUsuario')->radioList(Usuario::$listEnumLabels['tipoUsuario']) ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>

                    <?= $form->field($model, 'passwordConfirm')->passwordInput()->label('Digite a senha novamente') ?>

                    <?= $form->field($model, 'tempoOciosoMax')->textInput(['maxlenght' => true]) ?>

                    <?= $this->render('../layouts/_buttonsForm', ['model' => $model]) ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
