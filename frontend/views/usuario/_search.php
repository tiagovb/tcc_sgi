<?php

use common\helpers\ActiveForm;
use common\models\Usuario;
use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\search\UsuarioSearch */
/* @var $form ActiveForm */
?>

<div class="usuario-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-sm-3">
        <?= $form->field($model, 'login') ?>
    </div>

    <div class="col-sm-3">
        <?= $form->field($model, 'tipoUsuario')->drop(Usuario::getEnumListLabels('tipoUsuario')) ?>
    </div>

    <div class="col-sm-3">
        <?= $this->render('../layouts/_buttonsSearch') ?>
    </div>

    <div class="col-md-3 text-right">
        <label class="control-label" for="add-button-container">&nbsp;</label>
        <div id="add-button-container">
            <?= Html::a(Html::icon('plus') . ' Cadastrar Usuário', ['create'], ['class' => 'btn btn-success']); ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
