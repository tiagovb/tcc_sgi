<?php

use common\models\Usuario;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\UsuarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consultar Usuário';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                </div>

                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,

                        'columns' => [
                            [
                                'attribute' => 'id',
                                'headerOptions' => ['class' => 'col-xs-1'],
                            ],
                            [
                                'attribute' => 'login',
                            ],
                            [
                                'attribute' => 'tipoUsuario',
                                'value' => function ($model) {
                                    /** @var Usuario $model */
                                    return Usuario::getEnumLabel('tipoUsuario', $model->tipoUsuario);
                                },
                            ],
                            [
                                'attribute' => 'dtUltimoAcesso',
                                'format' => 'dateTime',
                            ],
                            [
                                'class' => ActionColumn::class,
                                'visibleButtons' => [
                                    'delete' => function ($model) {
                                        /** @var \common\models\Usuario $model */
                                        return $model->id != Yii::$app->user->identity->getId();
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>