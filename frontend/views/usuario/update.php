<?php

use common\helpers\ActiveForm;
use common\models\Usuario;
use kartik\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\models\Usuario */
/* @var $modelAlterarSenha \common\models\Usuario */

$this->title = 'Alterar Usuário: ' . $model->login;
$this->params['breadcrumbs'][] = ['label' => 'Usuários', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->login, 'url' => ['view', 'id' => $model->login]];
$this->params['breadcrumbs'][] = 'Alterar';
?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h4>Dados Cadastrais</h4>
            </div>
            <div class="box-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'form-alterar-usuario'
                ]); ?>

                <?= $form->field($model, 'login')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'tipoUsuario')->radioList(Usuario::$listEnumLabels['tipoUsuario'], ['inline' => true]) ?>

                <?= $form->field($model, 'tempoOciosoMax')->textInput(['maxlenght' => true]) ?>

                <?= $this->render('../layouts/_buttonsForm', ['model' => $model]) ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h4>Alterar Senha</h4>
                    </div>
                    <div class="box-body">
                        <?php $form = ActiveForm::begin([
                            'enableClientValidation' => true,
                            'id' => 'form-alterar-senha'
                        ]); ?>

                        <?= $form->field($modelAlterarSenha, 'password')->passwordInput() ?>

                        <?= $form->field($modelAlterarSenha, 'passwordConfirm')->passwordInput()->label('Digite a senha novamente') ?>

                        <div class="form-group text-right">
                            <?= Html::submitButton('Salvar nova senha', ['class' => 'btn btn-success']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>