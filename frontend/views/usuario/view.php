<?php

use common\models\Usuario;
use kartik\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Usuario */

$this->title = 'Usuário: ' . $model->login;
$this->params['breadcrumbs'][] = ['label' => 'Usuários', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->login, 'url' => ['view', 'id' => $model->login]];
$avisoExclusao = '';
$disableClass = '';

if ($model->id == Yii::$app->user->identity->getId()) {
    $avisoExclusao = 'Não é possível excluir seu próprio usuário.';
    $disableClass = 'disabled';
}
?>
<div class="usuario-view">
    <div class="row" style="margin-bottom: 5px;">
        <div class="col-md-6">
            <?= Html::a('Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>&nbsp;&nbsp;
            <span data-toggle="tooltip" data-title="<?= $avisoExclusao ?>">
                <?= Html::a('Excluir', ['delete', 'id' => $model->id], [
                    'class' => "btn btn-danger {$disableClass}",
                    'data' => [
                        'confirm' => 'Tem certeza que deseja excluir este item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </span>
            &nbsp;&nbsp;
            <?= Html::a(Html::icon('plus') . ' Cadastrar Novo Usuário', ['create'], ['class' => 'btn btn-success']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    'id',
                                    'login',
                                    'tipoUsuario:tipoUsuario',
                                    'dtUltimoAcesso:dateTime',
                                    [
                                        'attribute' => 'tempoOciosoMax',
                                        'format' => 'appendMinutos',
                                        'value' => $model->tempoOciosoMax ?: Usuario::TEMPO_OCIOSO_MAX_PADRAO,
                                    ]
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
