#!/usr/bin/env bash

#== Bash helpers ==

function info {
  echo " "
  echo "--> $1"
  echo " "
}

info "Config XDEBUG"

rm -rf /etc/php/7.0/mods-available/xdebug.ini
cp /app/vagrant/php/xdebug.ini /etc/php/7.0/mods-available/xdebug.ini

service php7.0-fpm restart