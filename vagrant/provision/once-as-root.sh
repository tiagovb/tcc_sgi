#!/usr/bin/env bash

#== Import script args ==

timezone=$(echo "$1")

#== Bash helpers ==

function info {
  echo " "
  echo "--> $1"
  echo " "
}

#== Provision script ==

info "Provision-script user: `whoami`"

info "Allocate swap for MySQL 5.6"
fallocate -l 2048M /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo '/swapfile none swap defaults 0 0' >> /etc/fstab
export DEBIAN_FRONTEND=noninteractive
info "Configure locales"
update-locale LC_ALL="C"
dpkg-reconfigure locales

info "Configure timezone"
timedatectl set-timezone ${timezone} --no-ask-password

info "Prepare root password for MySQL"
debconf-set-selections <<< "mysql-server-5.6 mysql-server/root_password password \"''\""
debconf-set-selections <<< "mysql-server-5.6 mysql-server/root_password_again password \"''\""
echo "Done!"

info "PHP7 Repo for trusty"
sudo add-apt-repository ppa:ondrej/php

info "Update OS software"
apt-get update
apt-get upgrade -y

info "Install additional software"
apt-get install -y php7.0-curl php7.0-cli php7.0-intl php7.0-mysqlnd php7.0-gd php7.0-fpm php7.0-mbstring php7.0-xml php-xdebug unzip nginx mysql-server-5.6


info "Configure MySQL"
sed -i "s/.*bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/my.cnf
mysql -uroot <<< "CREATE USER 'sgi_db'@'localhost' IDENTIFIED BY 'sgi_pwd';"
mysql -uroot <<< "GRANT ALL PRIVILEGES ON * . * TO 'sgi_db'@'localhost';"
mysql -uroot <<< "FLUSH PRIVILEGES;"
echo "Done!"

info "Configure PHP-FPM"
sed -i 's/user = www-data/user = vagrant/g' /etc/php/7.0/fpm/pool.d/www.conf
sed -i 's/group = www-data/group = vagrant/g' /etc/php/7.0/fpm/pool.d/www.conf
sed -i 's/owner = www-data/owner = vagrant/g' /etc/php/7.0/fpm/pool.d/www.conf
echo "Done!"

info "Configure NGINX"
sed -i 's/user www-data/user vagrant/g' /etc/nginx/nginx.conf
echo "Done!"

info "Enabling site configuration"
ln -s /app/vagrant/nginx/app.conf /etc/nginx/sites-enabled/app.conf
sudo rm /etc/nginx/sites-enabled/default
sudo apt-get autoremove
sudo apt-get remove --purge apache2
sudo apt-get remove apache2*
sudo apt-get remove apache2
sudo apt-get remove --auto-remove apache2
sudo apt-get purge --auto-remove apache2
sudo apt-get purge apache2
sudo service nginx restart
echo "Done!"

info "Initailize databases for MySQL"
mysql -uroot <<< "DROP DATABASE IF EXISTS sgi; CREATE DATABASE sgi;"
echo "Done!"

info "Setando Timezone para America/Sao_Paulo"
timedatectl set-timezone America/Sao_Paulo
echo "Done!"

info "Install composer"
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer